package lk.ncisl.obs.data.domain;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Cacheable;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author vimukthi
 */
@Entity
@Table(name = "order_request")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "OrderRequest.findAll", query = "SELECT o FROM OrderRequest o"),
    @NamedQuery(name = "OrderRequest.findLatestOrders", query = "SELECT o FROM OrderRequest o ORDER BY o.dateCreated DESC"),
    @NamedQuery(name = "OrderRequest.findById", query = "SELECT o FROM OrderRequest o WHERE o.id = :id"),
    @NamedQuery(name = "OrderRequest.findByCode", query = "SELECT o FROM OrderRequest o WHERE o.code = :code"),
    @NamedQuery(name = "OrderRequest.findByQuantity", query = "SELECT o FROM OrderRequest o WHERE o.quantity = :quantity"),
    @NamedQuery(name = "OrderRequest.findByState", query = "SELECT o FROM OrderRequest o WHERE o.state = :state"),
    @NamedQuery(name = "OrderRequest.findByExpirationDate", query = "SELECT o FROM OrderRequest o WHERE o.expirationDate = :expirationDate")
    })
@Cacheable(false)
public class OrderRequest implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @JoinColumn(name = "state_id")
    @ManyToOne(optional = false)
    private OrderState state;
    @Basic(optional = false)
    @Column(name = "code", length = 50, unique = true, nullable = false)
    private String code = "";
    @Basic(optional = false)
    @Column(name = "summary", length = 200, nullable = false)
    private String summary = "";
    @Basic(optional = false)
    @Column(name = "description", length = 5000, nullable = false)
    private String description = "";
    @Basic(optional = false)
    @Column(name = "quantity", nullable = false)
    private int quantity = 0;
    @Basic(optional = false)
    @Column(name = "expiration_date", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date expirationDate;
    @Basic(optional = false)
    @Column(name = "timezone", length = 500, nullable = false)
    private String timeZone = "";
    @Column(name = "image", length = 200)
    private String image = "";
    @Basic(optional = false)
    @Column(name = "date_created")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateCreated;
    @Basic(optional = false)
    @Column(name = "date_modified")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateModified;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "orderRequest")
    private List<Bid> bidList;

    public OrderRequest() {
        this.dateCreated = new Date();
    }

    public OrderRequest(String code, int quantity, Date expirationDate) {
        this.code = code;
        this.quantity = quantity;
        this.expirationDate = expirationDate;
        this.dateCreated = new Date();
    }

    /**
     * check if the given next state is a valid transition from current state
     *
     * @param nextState
     * @return
     */
    public boolean isValidStateTransition(OrderState nextState) {
        for (ValidOrderTransition validOrderTransition : getState().getValidOrderTransitions()) {
            // if the state 2 for the current state can be the next state then return true
            if (validOrderTransition.getState2().equals(nextState)) return true;
        }
        return false;
    }
    
    public boolean isInState(String state){
        return getState().getName().equals(state);
    }
    
    public boolean hasPhase1AcceptedBids(){
        for (Bid bid : bidList) {
            if (bid.isInState(BidState.ValidStates.PHASE1_ACCEPTED)) return true;
        }
        return false;
    }
    
    public boolean hasPhase2AcceptedBids(){
        for (Bid bid : bidList) {
            if (bid.isInState(BidState.ValidStates.ACCEPTED)) return true;
        }
        return false;
    }

    public Integer getId() {
        return id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public Date getExpirationDate() {
        return expirationDate;
    }

    public void setExpirationDate(Date expirationDate) {
        this.expirationDate = expirationDate;
    }

    public String getTimeZone() {
        return timeZone;
    }

    public void setTimeZone(String timeZone) {
        this.timeZone = timeZone;
    }    

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public Date getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(Date dateCreated) {
        this.dateCreated = dateCreated;
    }

    public Date getDateModified() {
        return dateModified;
    }

    public void setDateModified(Date dateModified) {
        this.dateModified = dateModified;
    }

    @XmlTransient
    public List<Bid> getBidList() {
        return bidList;
    }

    public void setBidList(List<Bid> bidList) {
        this.bidList = bidList;
    }

    public OrderState getState() {
        return state;
    }

    public void setState(OrderState state) {
        this.state = state;
    }

    public String getSummary() {
        return summary;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }
    
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof OrderRequest)) {
            return false;
        }
        OrderRequest other = (OrderRequest) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "lk.ncisl.obs.data.domain.OrderRequest[ id=" + id + " ]";
    }
}
