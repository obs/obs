package lk.ncisl.obs.data.services;

import lk.ncisl.obs.data.domain.OrderRequest;
import lk.ncisl.obs.data.domain.OrderState;

/**
 * @author vimukthi
 */
public interface OrderStateObserver {
    public void stateChanged(OrderRequest order);
}
