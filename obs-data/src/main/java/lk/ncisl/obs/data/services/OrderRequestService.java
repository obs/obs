
package lk.ncisl.obs.data.services;

import java.util.List;
import lk.ncisl.obs.data.domain.OrderRequest;
import lk.ncisl.obs.data.services.exceptions.InvalidOrderStateTransitionException;

/**
 * Facade for all order related actions
 * @author vimukthi
 */
public interface OrderRequestService {

    /**
     *
     * @param order
     */
    public void save(OrderRequest order);

    /**
     *
     * @param order
     * @throws InvalidOrderStateTransitionException
     */
    public void cancel(OrderRequest order) throws InvalidOrderStateTransitionException;

    /**
     *
     * @param order
     */
    public void startOrderRequest(OrderRequest order);

    public void expirePhase1(OrderRequest order) throws InvalidOrderStateTransitionException;

    public void startPhase2(OrderRequest order) throws InvalidOrderStateTransitionException;

    public void expirePhase2(OrderRequest order) throws InvalidOrderStateTransitionException;

    public void fullfilled(OrderRequest order) throws InvalidOrderStateTransitionException;

    public void unsatisfied(OrderRequest order) throws InvalidOrderStateTransitionException;
    
    public void expireOrderRequests();

    public void setOrderStateObserver(OrderStateObserver observer);

    public void setBidStateObserver(BidStateObserver observer);
    
}
