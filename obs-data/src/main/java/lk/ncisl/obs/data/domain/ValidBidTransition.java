

package lk.ncisl.obs.data.domain;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

/**
 *
 * @author vimukthi
 */
@Entity
@Table(name = "valid_bid_transition")
@NamedQueries({
    @NamedQuery(name = "ValidBidTransition.findAll", query = "SELECT v FROM ValidBidTransition v"),
    @NamedQuery(name = "ValidBidTransition.findById", query = "SELECT v FROM ValidBidTransition v WHERE v.id = :id"),
    @NamedQuery(name = "ValidBidTransition.findByDateModified", query = "SELECT v FROM ValidBidTransition v WHERE v.dateModified = :dateModified")})
public class ValidBidTransition implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Column(name = "date_modified")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateModified;
    @JoinColumn(name = "state1_id", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private BidState state1;
    @JoinColumn(name = "state2_id", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private BidState state2;

    public ValidBidTransition() {
    }

    public Integer getId() {
        return id;
    }

    public Date getDateModified() {
        return dateModified;
    }

    public BidState getState1() {
        return state1;
    }

    public BidState getState2() {
        return state2;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ValidBidTransition)) {
            return false;
        }
        ValidBidTransition other = (ValidBidTransition) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "lk.ncisl.obs.data.domain.ValidBidTransition[ id=" + id + " ]";
    }

}
