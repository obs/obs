

package lk.ncisl.obs.data.domain;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author vimukthi
 */
@Entity
@Table(name = "order_state")
@NamedQueries({
    @NamedQuery(name = "OrderState.findAll", query = "SELECT o FROM OrderState o"),
    @NamedQuery(name = "OrderState.findById", query = "SELECT o FROM OrderState o WHERE o.id = :id"),
    @NamedQuery(name = "OrderState.findByName", query = "SELECT o FROM OrderState o WHERE o.name = :name"),
    @NamedQuery(name = "OrderState.findByDescription", query = "SELECT o FROM OrderState o WHERE o.description = :description"),
    @NamedQuery(name = "OrderState.findByDateModified", query = "SELECT o FROM OrderState o WHERE o.dateModified = :dateModified")})
public class OrderState implements Serializable {
    
    public interface ValidStates {
        public static final String PHASE1 = "phase1";
        public static final String PHASE1_EXPIRED = "phase1_expired";
        public static final String PHASE2 = "phase2";
        public static final String PHASE2_EXPIRED = "phase2_expired";
        public static final String CANCELLED = "cancelled";
        public static final String FULLFILLED = "fullfilled";
        public static final String UNSATISFIED = "unsatisfied";
    }
    
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "name")
    private String name;
    
    @Size(max = 50)
    @Column(name = "display_name")
    private String displayName;
    
    @Size(max = 100)
    @Column(name = "description")
    private String description;
    
    @Column(name = "date_modified")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateModified;
    
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "state1")
    private List<ValidOrderTransition> validOrderTransitions;

    public OrderState() {
    }

    public Integer getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getDisplayName() {
        return displayName;
    }
    
    public String getDescription() {
        return description;
    }

    public Date getDateModified() {
        return dateModified;
    }

    public List<ValidOrderTransition> getValidOrderTransitions() {
        return validOrderTransitions;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof OrderState)) {
            return false;
        }
        OrderState other = (OrderState) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "lk.ncisl.obs.data.domain.OrderState[ id=" + id + " ]";
    }

}
