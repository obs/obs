

package lk.ncisl.obs.data.services.exceptions;

/**
 *
 * @author vimukthi
 */
public class InvalidOrderStateForTransactionException extends Exception {

    public InvalidOrderStateForTransactionException() {
        super("The order state is not valid for the attempted transaction");
    }

}
