package lk.ncisl.obs.data.services;

import lk.ncisl.obs.data.domain.Bid;
import lk.ncisl.obs.data.domain.BidState;
import lk.ncisl.obs.data.domain.OrderRequest;
import lk.ncisl.obs.data.domain.OrderState;

/**
 * @author vimukthi
 */
public interface BidStateObserver {
    public void stateChanged(Bid bid);
}
