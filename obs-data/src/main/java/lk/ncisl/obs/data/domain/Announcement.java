package lk.ncisl.obs.data.domain;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author vimukthi
 */
@Entity
@Table(name = "announcement")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Announcement.findAll", query = "SELECT a FROM Announcement a"),
    @NamedQuery(name = "Announcement.findById", query = "SELECT a FROM Announcement a WHERE a.id = :id"),
    @NamedQuery(name = "Announcement.findByTitle", query = "SELECT a FROM Announcement a WHERE a.title = :title"),
    @NamedQuery(name = "Announcement.findByDescription", query = "SELECT a FROM Announcement a WHERE a.description = :description"),
    @NamedQuery(name = "Announcement.findByGlobal", query = "SELECT a FROM Announcement a WHERE a.global = :global"),
    @NamedQuery(name = "Announcement.findByDateCreated", query = "SELECT a FROM Announcement a WHERE a.dateCreated = :dateCreated"),
    @NamedQuery(name = "Announcement.findByDateModified", query = "SELECT a FROM Announcement a WHERE a.dateModified = :dateModified")})
public class Announcement implements Serializable {
    
    private static final long serialVersionUID = 1L;
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    
    @Basic(optional = false)
    @Column(name = "title", length = 50)
    private String title;
    
    @Basic(optional = false)
    @Column(name = "description", length = 1000)
    private String description;
    
    @Basic(optional = false)
    @Column(name = "global")
    private short global;
    
    @Basic(optional = false)
    @Column(name = "date_created")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateCreated;
    
    @Basic(optional = false)
    @Column(name = "date_modified")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateModified;
    
    @JoinColumn(name = "to_user_id", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private User toUser;

    public Announcement() {
    }

    public Announcement(Integer id) {
        this.id = id;
    }

    public Announcement(Integer id, String title, String description, short global, Date dateCreated, Date dateModified) {
        this.id = id;
        this.title = title;
        this.description = description;
        this.global = global;
        this.dateCreated = dateCreated;
        this.dateModified = dateModified;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public short getGlobal() {
        return global;
    }

    public void setGlobal(short global) {
        this.global = global;
    }

    public Date getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(Date dateCreated) {
        this.dateCreated = dateCreated;
    }

    public Date getDateModified() {
        return dateModified;
    }

    public void setDateModified(Date dateModified) {
        this.dateModified = dateModified;
    }

    public User getToUser() {
        return toUser;
    }

    public void setToUser(User toUser) {
        this.toUser = toUser;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Announcement)) {
            return false;
        }
        Announcement other = (Announcement) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "lk.ncisl.obs.data.domain.Announcement[ id=" + id + " ]";
    }
    
}
