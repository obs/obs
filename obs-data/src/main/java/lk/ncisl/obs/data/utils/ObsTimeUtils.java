package lk.ncisl.obs.data.utils;

import java.util.Date;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;

/**
 * Add time utility functions that user JodaTime functionalities here.
 * http://joda-time.sourceforge.net/timezones.html
 * @author vimukthi
 */
public class ObsTimeUtils {

    public static Date getCurrentDateTimeForZone(String userTimeZone) {
        DateTimeZone zone = DateTimeZone.forID(userTimeZone);
        DateTime dt = new DateTime();
        return dt.withZone(zone).toDate();
    }

    public static Long getTimeLeft(Date expiryDate, String timeZoneForDate) {
        DateTimeZone fromZone = DateTimeZone.forID(timeZoneForDate);
        // expiry instant
        DateTime expiryDt = new DateTime(expiryDate, fromZone);
        // now
        DateTime dt = new DateTime();
        return expiryDt.getMillis() - dt.getMillis();       
    }
}
