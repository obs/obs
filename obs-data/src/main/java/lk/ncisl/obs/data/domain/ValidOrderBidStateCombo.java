

package lk.ncisl.obs.data.domain;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

/**
 *
 * @author vimukthi
 */
@Entity
@Table(name = "valid_order_bid_state_combo")
@NamedQueries({
    @NamedQuery(name = "ValidOrderBidStateCombo.findAll", query = "SELECT v FROM ValidOrderBidStateCombo v"),
    @NamedQuery(name = "ValidOrderBidStateCombo.findById", query = "SELECT v FROM ValidOrderBidStateCombo v WHERE v.id = :id"),
    @NamedQuery(name = "ValidOrderBidStateCombo.findByDateModified", query = "SELECT v FROM ValidOrderBidStateCombo v WHERE v.dateModified = :dateModified")})
public class ValidOrderBidStateCombo implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Column(name = "date_modified")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateModified;
    @JoinColumn(name = "bid_state_id", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private BidState bidState;
    @JoinColumn(name = "order_state_id", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private OrderState orderState;

    public ValidOrderBidStateCombo() {
    }

    public ValidOrderBidStateCombo(Integer id) {
        this.id = id;
    }

    public ValidOrderBidStateCombo(Integer id, Date dateModified) {
        this.id = id;
        this.dateModified = dateModified;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getDateModified() {
        return dateModified;
    }

    public void setDateModified(Date dateModified) {
        this.dateModified = dateModified;
    }
    
    public BidState getBidState() {
        return bidState;
    }

    public void setBidState(BidState bidState) {
        this.bidState = bidState;
    }

    public OrderState getOrderState() {
        return orderState;
    }

    public void setOrderState1Id(OrderState orderState) {
        this.orderState = orderState;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ValidOrderBidStateCombo)) {
            return false;
        }
        ValidOrderBidStateCombo other = (ValidOrderBidStateCombo) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return orderState.getName() + " : " + bidState.getName();
    }

}
