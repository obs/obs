package lk.ncisl.obs.data.domain;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author vimukthi
 */
@Entity
@Table(name = "bid")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Bid.findAll", query = "SELECT b FROM Bid b"),
    @NamedQuery(name = "Bid.findById", query = "SELECT b FROM Bid b WHERE b.id = :id"),
    @NamedQuery(name = "Bid.findByPricePerUnit", query = "SELECT b FROM Bid b WHERE b.pricePerUnit = :pricePerUnit"),
    @NamedQuery(name = "Bid.findByQuantity", query = "SELECT b FROM Bid b WHERE b.quantity = :quantity"),
    @NamedQuery(name = "Bid.findByDescription", query = "SELECT b FROM Bid b WHERE b.description = :description"),
    @NamedQuery(name = "Bid.findByDateCreated", query = "SELECT b FROM Bid b WHERE b.dateCreated = :dateCreated"),
    @NamedQuery(name = "Bid.findByDateModified", query = "SELECT b FROM Bid b WHERE b.dateModified = :dateModified")})
@Cacheable(false)
public class Bid implements Serializable {
    
    private static final long serialVersionUID = 1L;
    private final static Logger logger = LoggerFactory.getLogger(Bid.class);
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    
    @Basic(optional = false)
    @Column(name = "price_per_unit", nullable = false)
    private Float pricePerUnit = 0.1f;
    
    @Basic(optional = false)
    @Column(name = "quantity", nullable = false)
    private Integer quantity = 0;
    
    @Column(name = "description", length = 5000, nullable = false)
    private String description = "";
    
    @Basic(optional = false)
    @Column(name = "date_created")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateCreated;
    
    @Basic(optional = false)
    @Column(name = "date_modified")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateModified;
    
    @Basic(optional = false)
    @Column(name = "amends_bid_id")
    private Integer amendsBid;
    
    @JoinColumn(name = "order_id", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private OrderRequest orderRequest;
    
    @JoinColumn(name = "bidder_id", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private User bidder;
    
    @JoinColumn(name = "state_id")
    @ManyToOne(optional = false)
    private BidState state;

    public Bid() {
    }

    public Bid(float pricePerUnit, Integer quantity, String description) {
        this.pricePerUnit = pricePerUnit;
        this.quantity = quantity;
        this.description = description;
        this.dateCreated = new Date();
    }
    
    public boolean isValidStateTransition(BidState nextState) {
        Boolean validTransition = false;
        Boolean validOrderState = false;
        for (ValidBidTransition vbt : getState().getValidBidTransitions()) {
            if(vbt.getState2().equals(nextState)) validTransition = true;
        }
        
//        for (ValidOrderBidStateCombo combination : getState().getValidOrderBidStateCombinations()) {
//            logger.info(combination.toString());
//            if(combination.getBidState().equals(nextState) && 
//                    combination.getOrderState().equals(getOrderRequest().getState())) {
//                validOrderState = true;
//            }
//        }
        return /*validOrderState &&*/ validTransition;
    }
    
    public boolean isInState(String state){
        return getState().getName().equals(state);
    }

    public Integer getId() {
        return id;
    }

    public Float getPricePerUnit() {
        return pricePerUnit;
    }

    public void setPricePerUnit(Float pricePerUnit) {
        this.pricePerUnit = pricePerUnit;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setDateCreated(Date dateCreated) {
        this.dateCreated = dateCreated;
    }

    public Date getDateCreated() {
        return dateCreated;
    }

    public Date getDateModified() {
        return dateModified;
    }

    public OrderRequest getOrderRequest() {
        return orderRequest;
    }

    public void setOrderRequest(OrderRequest orderRequest) {
        this.orderRequest = orderRequest;
    }

    public User getBidder() {
        return bidder;
    }

    public void setBidder(User bidder) {
        this.bidder = bidder;
    }

    public BidState getState() {
        return state;
    }

    public void setState(BidState state) {
        this.state = state;
    }    

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Bid)) {
            return false;
        }
        Bid other = (Bid) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "lk.ncisl.obs.data.domain.Bid[ id=" + id + " ]";
    }
    
}
