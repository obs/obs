package lk.ncisl.obs.data.services.impl;

import lk.ncisl.obs.data.domain.BidState;
import lk.ncisl.obs.data.domain.Message;
import lk.ncisl.obs.data.domain.User;
import lk.ncisl.obs.data.services.MessagingService;
import lk.ncisl.obs.data.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import java.util.List;

/**
 *
 * @author prabhath
 */
public class UserServiceImpl implements UserService {

    @Autowired
    private LocalContainerEntityManagerFactoryBean emf;

    public void setEmf(LocalContainerEntityManagerFactoryBean emf) {
        this.emf = emf;
    }

    @Override
    public User getSystemUser() {
        EntityManager em = emf.getNativeEntityManagerFactory().createEntityManager();
        User systemUser = (User) em.createNamedQuery("User.findSystemUser").getSingleResult();
        return systemUser;
    }

    @Override
    public List<User> getAllUsers() {
        EntityManager em = emf.getNativeEntityManagerFactory().createEntityManager();
        List<User> users = (List<User>) em.createNamedQuery("User.findAllUsers").getResultList();
        return users;
    }
}
