package lk.ncisl.obs.data.services.impl;

import java.util.Date;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import lk.ncisl.obs.data.domain.Bid;
import lk.ncisl.obs.data.domain.BidState;
import lk.ncisl.obs.data.domain.OrderRequest;
import lk.ncisl.obs.data.domain.OrderState;
import lk.ncisl.obs.data.services.BidStateObserver;
import lk.ncisl.obs.data.services.OrderRequestService;
import lk.ncisl.obs.data.services.OrderStateObserver;
import lk.ncisl.obs.data.services.exceptions.InvalidOrderStateTransitionException;
import lk.ncisl.obs.data.utils.ObsTimeUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;

/**
 *
 * @author vimukthi
 */
public class OrderRequestServiceImpl implements OrderRequestService {

    private final static Logger logger = LoggerFactory.getLogger(OrderRequestServiceImpl.class);
    @Autowired
    private LocalContainerEntityManagerFactoryBean emf;

    @Autowired
    private OrderStateObserver orderStateObserver;

    @Autowired
    private BidStateObserver bidStateObserver;

    @Override
    public void save(OrderRequest order) {
        /**
         * Additional checking is required to make sure that an external party
         * can not circumvent state transition order using this method
         */
        EntityManager em = emf.getNativeEntityManagerFactory().createEntityManager();
        em.merge(order);
        // start a transaction manually to update record
        EntityTransaction tx = em.getTransaction();
        tx.begin();
        em.flush();
        tx.commit();
        // fire the state change events after committing transaction
        orderStateObserver.stateChanged(order);
    }

    @Override
    public void cancel(OrderRequest order) throws InvalidOrderStateTransitionException {
        // remember before state
        OrderState orderStateBefore = order.getState();
        /**
         * order = cancelled bid = admin_cancelled
         */
        EntityManager em = emf.getNativeEntityManagerFactory().createEntityManager();
        OrderState cancelledState = (OrderState) em.createNamedQuery("OrderState.findByName").
                setParameter("name", OrderState.ValidStates.CANCELLED).getSingleResult();
        BidState adminCancelState = (BidState) em.createNamedQuery("BidState.findByName").
                setParameter("name", BidState.ValidStates.ADMIN_CANCELLED).getSingleResult();

        boolean commit = true;
        if (order.isValidStateTransition(cancelledState)) {
            EntityTransaction tx = em.getTransaction();
            tx.begin();
            // Updating without checking the validity of the order and bid state combination
            order.setState(cancelledState);
            em.merge(order);
            em.flush();
            for (Bid bid : order.getBidList()) {
                if (bid.isValidStateTransition(adminCancelState)) {
                    bid.setState(adminCancelState);
                    em.merge(bid);
                    em.flush();
                } else {
                    tx.rollback();
                    commit = false;
                    break;
                }
            }
            if (commit) {
                tx.commit();
                // fire the state change events after committing transaction
                orderStateObserver.stateChanged(order);
                for (Bid bid : order.getBidList()) {
                    bidStateObserver.stateChanged(bid);
                }
            }
        } else {
            throw new InvalidOrderStateTransitionException("Order state transition for order "
                    + order.getCode() + " from " + order.getState().getName() + " to " + cancelledState.getName() + " is invalid");
        }

    }

    @Override
    public void startOrderRequest(OrderRequest order) {
        /**
         * order = phase1
         */
        EntityManager em = emf.getNativeEntityManagerFactory().createEntityManager();
        OrderState phase1State = (OrderState) em.createNamedQuery("OrderState.findByName").
                setParameter("name", OrderState.ValidStates.PHASE1).getSingleResult();
        order.setState(phase1State);
        em.persist(order);
        // start a transaction manually to update record
        EntityTransaction tx = em.getTransaction();
        tx.begin();
        em.flush();
        tx.commit();
        // fire the state change events after committing transaction
        orderStateObserver.stateChanged(order);
    }

    @Override
    public void expirePhase1(OrderRequest order) throws InvalidOrderStateTransitionException {
        /**
         * order = phase1_expired bid = phase1_onhold -> admin_cancelled
         */
        if (order.isInState(OrderState.ValidStates.PHASE1)) {
            EntityManager em = emf.getNativeEntityManagerFactory().createEntityManager();
            OrderState phase1ExpiredState = (OrderState) em.createNamedQuery("OrderState.findByName").
                    setParameter("name", OrderState.ValidStates.PHASE1_EXPIRED).getSingleResult();
            BidState adminCancelState = (BidState) em.createNamedQuery("BidState.findByName").
                    setParameter("name", BidState.ValidStates.ADMIN_CANCELLED).getSingleResult();

            if (order.isValidStateTransition(phase1ExpiredState)) {
                EntityTransaction tx = em.getTransaction();
                tx.begin();
                // Updating without checking the validity of the order and bid state combination
                order.setState(phase1ExpiredState);
                em.merge(order);
                em.flush();
                for (Bid bid : order.getBidList()) {
                    if (bid.isInState(BidState.ValidStates.PHASE1_ONHOLD)) {
                        bid.setState(adminCancelState);
                        em.merge(bid);
                        em.flush();
                    }
                }
                tx.commit();
                // fire the state change events after committing transaction
                orderStateObserver.stateChanged(order);
                for (Bid bid : order.getBidList()) {
                    bidStateObserver.stateChanged(bid);
                }
            } else {
                throw new InvalidOrderStateTransitionException("Order state transition for order "
                        + order.getCode() + " from " + order.getState().getName() + " to " + phase1ExpiredState.getName() + " is invalid");
            }
        }
    }

    @Override
    public void startPhase2(OrderRequest order) throws InvalidOrderStateTransitionException {
        /**
         * order = phase2 bid = phase1_accepted -> phase2_active phase1_active
         * -> phase1_rejected
         */
        if (order.isInState(OrderState.ValidStates.PHASE1_EXPIRED)) {
            EntityManager em = emf.getNativeEntityManagerFactory().createEntityManager();
            OrderState phase2State = (OrderState) em.createNamedQuery("OrderState.findByName").
                    setParameter("name", OrderState.ValidStates.PHASE2).getSingleResult();
            BidState phase2ActiveState = (BidState) em.createNamedQuery("BidState.findByName").
                    setParameter("name", BidState.ValidStates.PHASE2_ACTIVE).getSingleResult();
            BidState phase1RejectedState = (BidState) em.createNamedQuery("BidState.findByName").
                    setParameter("name", BidState.ValidStates.PHASE1_REJECTED).getSingleResult();

            if (order.isValidStateTransition(phase2State)) {
                EntityTransaction tx = em.getTransaction();
                tx.begin();
                // Updating without checking the validity of the order and bid state combination
                order.setState(phase2State);
                em.merge(order);
                em.flush();
                for (Bid bid : order.getBidList()) {
                    if (bid.isInState(BidState.ValidStates.PHASE1_ACCEPTED)) {
                        bid.setState(phase2ActiveState);
                        em.merge(bid);
                        em.flush();
                    } else if (bid.isInState(BidState.ValidStates.PHASE1_ACTIVE)) {
                        bid.setState(phase1RejectedState);
                        em.merge(bid);
                        em.flush();
                    }
                }
                tx.commit();
                // fire the state change events after committing transaction
                orderStateObserver.stateChanged(order);
                for (Bid bid : order.getBidList()) {
                    bidStateObserver.stateChanged(bid);
                }
            } else {
                throw new InvalidOrderStateTransitionException("Order state transition for order "
                        + order.getCode() + " from " + order.getState().getName() + " to " + phase2State.getName() + " is invalid");
            }
        }
    }

    @Override
    public void expirePhase2(OrderRequest order) throws InvalidOrderStateTransitionException {
        /**
         * order = phase2_expired bid = phase2_onhold -> admin_cancelled
         */
        if (order.isInState(OrderState.ValidStates.PHASE2)) {
            EntityManager em = emf.getNativeEntityManagerFactory().createEntityManager();
            OrderState phase2ExpiredState = (OrderState) em.createNamedQuery("OrderState.findByName").
                    setParameter("name", OrderState.ValidStates.PHASE2_EXPIRED).getSingleResult();
            BidState adminCancelState = (BidState) em.createNamedQuery("BidState.findByName").
                    setParameter("name", BidState.ValidStates.ADMIN_CANCELLED).getSingleResult();

            if (order.isValidStateTransition(phase2ExpiredState)) {
                EntityTransaction tx = em.getTransaction();
                tx.begin();
                // Updating without checking the validity of the order and bid state combination
                order.setState(phase2ExpiredState);
                em.merge(order);
                em.flush();
                for (Bid bid : order.getBidList()) {
                    if (bid.isInState(BidState.ValidStates.PHASE2_ONHOLD)) {
                        bid.setState(adminCancelState);
                        em.merge(bid);
                        em.flush();
                    }
                }
                tx.commit();
                // fire the state change events after committing transaction
                orderStateObserver.stateChanged(order);
                for (Bid bid : order.getBidList()) {
                    bidStateObserver.stateChanged(bid);
                }
            } else {
                throw new InvalidOrderStateTransitionException("Order state transition for order "
                        + order.getCode() + " from " + order.getState().getName() + " to " + phase2ExpiredState.getName() + " is invalid");
            }
        }
    }

    @Override
    public void fullfilled(OrderRequest order) throws InvalidOrderStateTransitionException {
        /**
         * order = fullfilled bid = phase2_active -> rejected
         */
        if (order.isInState(OrderState.ValidStates.PHASE2_EXPIRED)) {
            EntityManager em = emf.getNativeEntityManagerFactory().createEntityManager();
            OrderState fullFilledState = (OrderState) em.createNamedQuery("OrderState.findByName").
                    setParameter("name", OrderState.ValidStates.FULLFILLED).getSingleResult();
            BidState rejectedState = (BidState) em.createNamedQuery("BidState.findByName").
                    setParameter("name", BidState.ValidStates.REJECTED).getSingleResult();

            if (order.isValidStateTransition(fullFilledState)) {
                EntityTransaction tx = em.getTransaction();
                tx.begin();
                // Updating without checking the validity of the order and bid state combination
                order.setState(fullFilledState);
                em.merge(order);
                em.flush();
                for (Bid bid : order.getBidList()) {
                    if (bid.isInState(BidState.ValidStates.PHASE2_ACTIVE)) {
                        bid.setState(rejectedState);
                        em.merge(bid);
                        em.flush();
                    }
                }
                tx.commit();
                // fire the state change events after committing transaction
                orderStateObserver.stateChanged(order);
                for (Bid bid : order.getBidList()) {
                    bidStateObserver.stateChanged(bid);
                }
            } else {
                throw new InvalidOrderStateTransitionException("Order state transition for order "
                        + order.getCode() + " from " + order.getState().getName() + " to " + fullFilledState.getName() + " is invalid");
            }
        }
    }

    @Override
    public void unsatisfied(OrderRequest order) throws InvalidOrderStateTransitionException {
        /**
         * order = unsatisfied bid = phase2_active -> rejected
         */
        if (order.isInState(OrderState.ValidStates.PHASE2_EXPIRED)) {
            EntityManager em = emf.getNativeEntityManagerFactory().createEntityManager();
            OrderState unsatisfiedState = (OrderState) em.createNamedQuery("OrderState.findByName").
                    setParameter("name", OrderState.ValidStates.UNSATISFIED).getSingleResult();
            BidState rejectedState = (BidState) em.createNamedQuery("BidState.findByName").
                    setParameter("name", BidState.ValidStates.REJECTED).getSingleResult();

            if (order.isValidStateTransition(unsatisfiedState)) {
                EntityTransaction tx = em.getTransaction();
                tx.begin();
                // Updating without checking the validity of the order and bid state combination
                order.setState(unsatisfiedState);
                em.merge(order);
                em.flush();
                for (Bid bid : order.getBidList()) {
                    if (bid.isInState(BidState.ValidStates.PHASE2_ACTIVE)) {
                        bid.setState(rejectedState);
                        em.merge(bid);
                        em.flush();
                    }
                }
                tx.commit();
                // fire the state change events after committing transaction
                orderStateObserver.stateChanged(order);
                for (Bid bid : order.getBidList()) {
                    bidStateObserver.stateChanged(bid);
                }
            } else {
                throw new InvalidOrderStateTransitionException("Order state transition for order "
                        + order.getCode() + " from " + order.getState().getName() + " to " + unsatisfiedState.getName() + " is invalid");
            }
        }
    }

    @Override
    public void expireOrderRequests() {
        EntityManager em = emf.getNativeEntityManagerFactory().createEntityManager();
        // expire phase 1
        OrderState phase1State = (OrderState) em.createNamedQuery("OrderState.findByName").
                setParameter("name", OrderState.ValidStates.PHASE1).getSingleResult();
        List<OrderRequest> phase1Orders = em.createNamedQuery("OrderRequest.findByState").
                setParameter("state", phase1State).getResultList();
        for (OrderRequest orderRequest : phase1Orders) {
            if (ObsTimeUtils.getTimeLeft(orderRequest.getExpirationDate(), orderRequest.getTimeZone()) <= 0) {
                try {
                    this.expirePhase1(orderRequest);
                } catch (InvalidOrderStateTransitionException ex) {
                    logger.error("Could not expire phase 1 for order " + orderRequest.getCode(), ex);
                }
            }
        }

        // expire phase 2
        OrderState phase2State = (OrderState) em.createNamedQuery("OrderState.findByName").
                setParameter("name", OrderState.ValidStates.PHASE2).getSingleResult();
        List<OrderRequest> phase2Orders = em.createNamedQuery("OrderRequest.findByState").
                setParameter("state", phase2State).getResultList();
        for (OrderRequest orderRequest : phase2Orders) {
            if (ObsTimeUtils.getTimeLeft(orderRequest.getExpirationDate(), orderRequest.getTimeZone()) <= 0) {
                try {
                    this.expirePhase2(orderRequest);
                } catch (InvalidOrderStateTransitionException ex) {
                    logger.error("Could not expire phase 2 for order " + orderRequest.getCode(), ex);
                }
            }
        }

    }

    @Override
    public void setOrderStateObserver(OrderStateObserver observer) {
        this.orderStateObserver = observer;
    }

    @Override
    public void setBidStateObserver(BidStateObserver observer) {
        this.bidStateObserver = observer;
    }

    public void setEmf(LocalContainerEntityManagerFactoryBean emf) {
        this.emf = emf;
    }
}
