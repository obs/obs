

package lk.ncisl.obs.data.services.exceptions;

/**
 *
 * @author vimukthi
 */
public class InvalidBidStateTransitionException extends Exception {

    public InvalidBidStateTransitionException() {
        super("The bid state transition is not valid");
    }

}
