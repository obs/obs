

package lk.ncisl.obs.data.services.exceptions;

/**
 *
 * @author vimukthi
 */
public class NonActiveOrderException extends Exception {

    public NonActiveOrderException() {
        super("The order can not be bid on");
    }

}
