

package lk.ncisl.obs.data.domain;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author vimukthi
 */
@Entity
@Table(name = "valid_order_transition")
@NamedQueries({
    @NamedQuery(name = "ValidOrderTransition.findAll", query = "SELECT v FROM ValidOrderTransition v"),
    @NamedQuery(name = "ValidOrderTransition.findById", query = "SELECT v FROM ValidOrderTransition v WHERE v.id = :id"),
    @NamedQuery(name = "ValidOrderTransition.findByDateModified", query = "SELECT v FROM ValidOrderTransition v WHERE v.dateModified = :dateModified")})
public class ValidOrderTransition implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    
    @Column(name = "date_modified")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateModified;
    
    @JoinColumn(name = "state1_id")
    @ManyToOne(optional = false)
    private OrderState state1;
    
    @JoinColumn(name = "state2_id")
    @ManyToOne(optional = false)
    private OrderState state2;

    public ValidOrderTransition() {
    }
    
    public Integer getId() {
        return id;
    }

    public Date getDateModified() {
        return dateModified;
    }

    public OrderState getState1() {
        return state1;
    }

    public OrderState getState2() {
        return state2;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ValidOrderTransition)) {
            return false;
        }
        ValidOrderTransition other = (ValidOrderTransition) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "lk.ncisl.obs.data.domain.ValidOrderTransition[ id=" + id + " ]";
    }

}
