package lk.ncisl.obs.data.services.impl;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import lk.ncisl.obs.data.domain.Bid;
import lk.ncisl.obs.data.domain.BidState;
import lk.ncisl.obs.data.domain.OrderState;
import lk.ncisl.obs.data.services.BidStateObserver;
import lk.ncisl.obs.data.services.BiddingService;
import lk.ncisl.obs.data.services.exceptions.InvalidBidStateTransitionException;
import lk.ncisl.obs.data.services.exceptions.InvalidOrderStateForTransactionException;
import lk.ncisl.obs.data.services.exceptions.NonActiveOrderException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;

/**
 *
 * @author vimukthi
 */
public class BiddingServiceImpl implements BiddingService {

    @Autowired
    private LocalContainerEntityManagerFactoryBean emf;
    @Autowired
    private BidStateObserver bidStateObserver;

    @Override
    public void add(Bid bid) throws NonActiveOrderException {
        if (bid.getOrderRequest().isInState(OrderState.ValidStates.PHASE1)) {
            EntityManager em = emf.getNativeEntityManagerFactory().createEntityManager();
            BidState phase1ActiveState = (BidState) em.createNamedQuery("BidState.findByName").
                    setParameter("name", BidState.ValidStates.PHASE1_ACTIVE).getSingleResult();
            bid.setState(phase1ActiveState);
            em.persist(bid);
            // start a transaction manually to update record
            EntityTransaction tx = em.getTransaction();
            tx.begin();
            em.flush();
            tx.commit();
            bidStateObserver.stateChanged(bid);
        } else if (bid.getOrderRequest().isInState(OrderState.ValidStates.PHASE2)) {
            EntityManager em = emf.getNativeEntityManagerFactory().createEntityManager();
            BidState phase2ActiveState = (BidState) em.createNamedQuery("BidState.findByName").
                    setParameter("name", BidState.ValidStates.PHASE2_ACTIVE).getSingleResult();
            bid.setState(phase2ActiveState);
            em.persist(bid);
            // start a transaction manually to update record
            EntityTransaction tx = em.getTransaction();
            tx.begin();
            em.flush();
            tx.commit();
            bidStateObserver.stateChanged(bid);
        } else {
            throw new NonActiveOrderException();
        }
    }

    @Override
    public void adminCancel(Bid bid) throws InvalidBidStateTransitionException, InvalidOrderStateForTransactionException {
        EntityManager em = emf.getNativeEntityManagerFactory().createEntityManager();
        BidState adminCancelledState = (BidState) em.createNamedQuery("BidState.findByName").
                setParameter("name", BidState.ValidStates.ADMIN_CANCELLED).getSingleResult();
        if (!bid.getOrderRequest().isInState(OrderState.ValidStates.FULLFILLED) || 
                !bid.getOrderRequest().isInState(OrderState.ValidStates.UNSATISFIED)) {
            if (bid.isValidStateTransition(adminCancelledState)) {
                bid.setState(adminCancelledState);
                em.merge(bid);
                // start a transaction manually to update record
                EntityTransaction tx = em.getTransaction();
                tx.begin();
                em.flush();
                tx.commit();
                bidStateObserver.stateChanged(bid);
            } else {
                throw new InvalidBidStateTransitionException();
            }
        } else {
            throw new InvalidOrderStateForTransactionException();
        }
    }

    @Override
    public void cancel(Bid bid) throws InvalidBidStateTransitionException, InvalidOrderStateForTransactionException {
        EntityManager em = emf.getNativeEntityManagerFactory().createEntityManager();
        BidState cancelledState = (BidState) em.createNamedQuery("BidState.findByName").
                setParameter("name", BidState.ValidStates.CANCELLED).getSingleResult();
        if (!bid.getOrderRequest().isInState(OrderState.ValidStates.FULLFILLED) || 
                !bid.getOrderRequest().isInState(OrderState.ValidStates.UNSATISFIED)) {
            if (bid.isValidStateTransition(cancelledState)) {
                bid.setState(cancelledState);
                em.merge(bid);
                // start a transaction manually to update record
                EntityTransaction tx = em.getTransaction();
                tx.begin();
                em.flush();
                tx.commit();
                bidStateObserver.stateChanged(bid);
            } else {
                throw new InvalidBidStateTransitionException();
            }
        } else {
            throw new InvalidOrderStateForTransactionException();
        }
    }

    @Override
    public void accept(Bid bid) throws InvalidBidStateTransitionException, InvalidOrderStateForTransactionException {
        if (bid.getOrderRequest().isInState(OrderState.ValidStates.PHASE1_EXPIRED)) {
            EntityManager em = emf.getNativeEntityManagerFactory().createEntityManager();
            BidState phase1AcceptedState = (BidState) em.createNamedQuery("BidState.findByName").
                    setParameter("name", BidState.ValidStates.PHASE1_ACCEPTED).getSingleResult();
            if (bid.isValidStateTransition(phase1AcceptedState)) {
                bid.setState(phase1AcceptedState);
                em.persist(bid);
                // start a transaction manually to update record
                EntityTransaction tx = em.getTransaction();
                tx.begin();
                em.flush();
                tx.commit();
                bidStateObserver.stateChanged(bid);
            } else {
                throw new InvalidBidStateTransitionException();
            }
        } else if (bid.getOrderRequest().isInState(OrderState.ValidStates.PHASE2_EXPIRED)) {
            EntityManager em = emf.getNativeEntityManagerFactory().createEntityManager();
            BidState AcceptedState = (BidState) em.createNamedQuery("BidState.findByName").
                    setParameter("name", BidState.ValidStates.ACCEPTED).getSingleResult();
            if (bid.isValidStateTransition(AcceptedState)) {
                bid.setState(AcceptedState);
                em.persist(bid);
                // start a transaction manually to update record
                EntityTransaction tx = em.getTransaction();
                tx.begin();
                em.flush();
                tx.commit();
                bidStateObserver.stateChanged(bid);
            } else {
                throw new InvalidBidStateTransitionException();
            }
        } else {
            throw new InvalidOrderStateForTransactionException();
        }
    }

    @Override
    public void reject(Bid bid) throws InvalidBidStateTransitionException, InvalidOrderStateForTransactionException {
        if (bid.getOrderRequest().isInState(OrderState.ValidStates.PHASE1_EXPIRED)) {
            EntityManager em = emf.getNativeEntityManagerFactory().createEntityManager();
            BidState phase1RejectedState = (BidState) em.createNamedQuery("BidState.findByName").
                    setParameter("name", BidState.ValidStates.PHASE1_REJECTED).getSingleResult();
            if (bid.isValidStateTransition(phase1RejectedState)) {
                bid.setState(phase1RejectedState);
                em.persist(bid);
                // start a transaction manually to update record
                EntityTransaction tx = em.getTransaction();
                tx.begin();
                em.flush();
                tx.commit();
                bidStateObserver.stateChanged(bid);
            } else {
                throw new InvalidBidStateTransitionException();
            }
        } else if (bid.getOrderRequest().isInState(OrderState.ValidStates.PHASE2_EXPIRED)) {
            EntityManager em = emf.getNativeEntityManagerFactory().createEntityManager();
            BidState rejectedState = (BidState) em.createNamedQuery("BidState.findByName").
                    setParameter("name", BidState.ValidStates.REJECTED).getSingleResult();
            if (bid.isValidStateTransition(rejectedState)) {
                bid.setState(rejectedState);
                em.persist(bid);
                // start a transaction manually to update record
                EntityTransaction tx = em.getTransaction();
                tx.begin();
                em.flush();
                tx.commit();
                bidStateObserver.stateChanged(bid);
            } else {
                throw new InvalidBidStateTransitionException();
            }
        } else {
            throw new InvalidOrderStateForTransactionException();
        }
    }

    @Override
    public void hold(Bid bid) throws InvalidBidStateTransitionException, InvalidOrderStateForTransactionException {
        if (bid.getOrderRequest().isInState(OrderState.ValidStates.PHASE1)) {
            EntityManager em = emf.getNativeEntityManagerFactory().createEntityManager();
            BidState phase1OnholdState = (BidState) em.createNamedQuery("BidState.findByName").
                    setParameter("name", BidState.ValidStates.PHASE1_ONHOLD).getSingleResult();
            if (bid.isValidStateTransition(phase1OnholdState)) {
                bid.setState(phase1OnholdState);
                em.persist(bid);
                // start a transaction manually to update record
                EntityTransaction tx = em.getTransaction();
                tx.begin();
                em.flush();
                tx.commit();
                bidStateObserver.stateChanged(bid);
            } else {
                throw new InvalidBidStateTransitionException();
            }
        } else if (bid.getOrderRequest().isInState(OrderState.ValidStates.PHASE2)) {
            EntityManager em = emf.getNativeEntityManagerFactory().createEntityManager();
            BidState phase2OnholdState = (BidState) em.createNamedQuery("BidState.findByName").
                    setParameter("name", BidState.ValidStates.PHASE2_ONHOLD).getSingleResult();
            if (bid.isValidStateTransition(phase2OnholdState)) {
                bid.setState(phase2OnholdState);
                em.persist(bid);
                // start a transaction manually to update record
                EntityTransaction tx = em.getTransaction();
                tx.begin();
                em.flush();
                tx.commit();
                bidStateObserver.stateChanged(bid);
            } else {
                throw new InvalidBidStateTransitionException();
            }
        } else {
            throw new InvalidOrderStateForTransactionException();
        }
    }

    @Override
    public void unhold(Bid bid) throws InvalidBidStateTransitionException, InvalidOrderStateForTransactionException {
        if (bid.getOrderRequest().isInState(OrderState.ValidStates.PHASE1)) {
            EntityManager em = emf.getNativeEntityManagerFactory().createEntityManager();
            BidState phase1ActiveState = (BidState) em.createNamedQuery("BidState.findByName").
                    setParameter("name", BidState.ValidStates.PHASE1_ACTIVE).getSingleResult();
            if (bid.isValidStateTransition(phase1ActiveState)) {
                bid.setState(phase1ActiveState);
                em.persist(bid);
                // start a transaction manually to update record
                EntityTransaction tx = em.getTransaction();
                tx.begin();
                em.flush();
                tx.commit();
                bidStateObserver.stateChanged(bid);
            } else {
                throw new InvalidBidStateTransitionException();
            }
        } else if (bid.getOrderRequest().isInState(OrderState.ValidStates.PHASE2)) {
            EntityManager em = emf.getNativeEntityManagerFactory().createEntityManager();
            BidState phase2ActiveState = (BidState) em.createNamedQuery("BidState.findByName").
                    setParameter("name", BidState.ValidStates.PHASE2_ACTIVE).getSingleResult();
            if (bid.isValidStateTransition(phase2ActiveState)) {
                bid.setState(phase2ActiveState);
                em.persist(bid);
                // start a transaction manually to update record
                EntityTransaction tx = em.getTransaction();
                tx.begin();
                em.flush();
                tx.commit();
                bidStateObserver.stateChanged(bid);
            } else {
                throw new InvalidBidStateTransitionException();
            }
        } else {
            throw new InvalidOrderStateForTransactionException();
        }
    }

    @Override
    public void setBidStateObserver(BidStateObserver observer) {
        this.bidStateObserver = observer;
    }

    public void setEmf(LocalContainerEntityManagerFactoryBean emf) {
        this.emf = emf;
    }
}
