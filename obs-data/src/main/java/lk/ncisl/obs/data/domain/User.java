package lk.ncisl.obs.data.domain;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Cacheable;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.xml.bind.annotation.XmlTransient;
import org.hibernate.validator.constraints.Email;

/**
 *
 * @author vimukthi
 */
@Entity
@Table(name = "user")
@NamedQueries({
    @NamedQuery(name = "User.findByEmail", query = "SELECT u FROM User u WHERE u.email = :email"),
    @NamedQuery(name = "User.findSystemUser", query = "SELECT u FROM User u WHERE u.system = 1"),
    @NamedQuery(name = "User.findAllUsers", query = "SELECT u FROM User u")
})
@Cacheable(false)
public class User implements Serializable {
    
    private static final long serialVersionUID = 1L;
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    
    @Basic(optional = false)
    @Column(name = "first_name", length = 50)
    //@Pattern(regexp = "[a-z-A-Z]+", message="Enter valid charactors")
    private String firstName = "";
    
    @Basic(optional = false)
    @Column(name = "last_name", length = 50)
    //@Pattern(regexp = "[a-z-A-Z]+", message="Enter valid charactors")
    private String lastName = "";
    
    @Basic(optional = true)
    @Column(name = "about_me", length = 500)
    private String aboutMe = "";
    
    @Basic(optional = true)
    @Column(name = "admin_comment", length = 500)
    private String adminComment = "";
    
    @Basic(optional = false)
    @Column(name = "phone", length = 20, unique = true)
    //@Pattern(regexp = "[0-9]+", message="numeric charactors only")
    private String phone = "";
    
    @Basic(optional = true)
    @Column(name = "mobile", length = 20)
    //@Pattern(regexp = "[0-9]*", message="numeric charactors only")
    private String mobile = "";
    
    @Basic(optional = false)
    @Column(name = "email", length = 200, unique = true)
    //@Email
    //@NotNull
    private String email = "";
    
    @Basic(optional = true)
    @Column(name = "secondary_email", length = 200)
    //@Email
    private String secondaryEmail = "";
    
    @Basic(optional = true)
    @Column(name = "photo", length = 500)
    private String photo = "";
    
    @Basic(optional = false)
    @Column(name = "password", length = 20)
    //@Pattern(regexp = "[a-z-A-Z-0-9]+", message = "Alphanumerical charactors only")
    private String password = "";
    
    @Basic(optional = false)
    @Column(name = "security_question", length = 500)
    //@NotNull
    private String securityQuestion = "What is your birth place?";
    
    @Basic(optional = false)
    @Column(name = "security_question_answer", length = 500)
    //@NotNull
    private String securityQuestionAnswer = "Colombo ???";
    
    @Basic(optional = false)
    @Column(name = "organization", length = 50)
    @NotNull
    private String organization = "";
    
    @Basic(optional = false)
    @Column(name = "active")
    private Integer active = 1;
    
    @Basic(optional = false)
    @Column(name = "system")
    private Integer system = 0;
    
    @Basic(optional = false)
    @Column(name = "date_created")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateCreated;
    
    @Basic(optional = false)
    @Column(name = "date_modified")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateModified;
    
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "toUser")
    private List<Announcement> announcementList;
    
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "user")
    private List<UserPreference> userPreferenceList;
    
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "bidder")
    private List<Bid> bidList;
    
    @ManyToMany
    @JoinTable(name="user_role",
                 joinColumns=
                      @JoinColumn(name="user_id"),
                 inverseJoinColumns=
                      @JoinColumn(name="role_id")
     )
    private List<Role> roleList;

    public User() {
    }

    public User(String name, String email, String password, String organization) {
        this.firstName = name;
        this.email = email;
        this.password = password;
        this.organization = organization;
        this.dateCreated = new Date();
    }
    
     public Boolean isAdmin(){
        for (Role role : roleList) {
            if(role.getName().equals("admin")){
                return true;                
            }
        }
        return false;
    }
    
    public Boolean isInRole(String roleName){
        for (Role role : roleList) {
            if(role.getName().equals(roleName)){
                return true;                
            }
        }
        return false;
    }
    
    public Boolean hasPermission(String permissionName){
        for (Permission permission : getPermissions()) {
            if (permission.getName().equals(permissionName)) {
                return true;
            }
        }
        return false;
    }
    
    public List<Permission> getPermissions(){
        List<Permission> perms = new ArrayList<Permission>();
        for (Role role : roleList) {
            perms.addAll(role.getPermissionList());
        }
        return perms;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String name) {
        this.firstName = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getOrganization() {
        return organization;
    }

    public void setOrganization(String organization) {
        this.organization = organization;
    }

    public Date getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(Date dateCreated) {
        this.dateCreated = dateCreated;
    }

    public Date getDateModified() {
        return dateModified;
    }

    public void setDateModified(Date dateModified) {
        this.dateModified = dateModified;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getAboutMe() {
        return aboutMe;
    }

    public void setAboutMe(String aboutMe) {
        this.aboutMe = aboutMe;
    }

    public String getAdminComment() {
        return adminComment;
    }

    public void setAdminComment(String adminComment) {
        this.adminComment = adminComment;
    }   

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getSecondaryEmail() {
        return secondaryEmail;
    }

    public void setSecondaryEmail(String secondaryEmail) {
        this.secondaryEmail = secondaryEmail;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getSecurityQuestion() {
        return securityQuestion;
    }

    public void setSecurityQuestion(String securityQuestion) {
        this.securityQuestion = securityQuestion;
    }

    public String getSecurityQuestionAnswer() {
        return securityQuestionAnswer;
    }

    public void setSecurityQuestionAnswer(String securityQuestionAnswer) {
        this.securityQuestionAnswer = securityQuestionAnswer;
    }
    
    public void setActive(Boolean active){
        this.active = active ? 1 : 0;
    }
    
    public Boolean isActive(){
        return active == 1;
    }
    
    public Integer getActive(){
        return active;
    }

    public Boolean isSystem() {
        return system == 1;
    }    
    
    public Integer getSystem() {
        return system;
    }  
    
    public String getTimeZone(){
        String tz = null;
        for (UserPreference pref : userPreferenceList) {
            if(pref.getPreference().getName().equals("timezone")){
                tz = pref.getValue().trim();
            }
        }
        return tz;
    }            

    @XmlTransient
    public List<Announcement> getAnnouncementList() {
        return announcementList;
    }

    public void setAnnouncementList(List<Announcement> announcementList) {
        this.announcementList = announcementList;
    }

    @XmlTransient
    public List<UserPreference> getUserPreferenceList() {
        return userPreferenceList;
    }

    public void setUserPreferenceList(List<UserPreference> userPreferenceList) {
        this.userPreferenceList = userPreferenceList;
    }

    @XmlTransient
    public List<Bid> getBidList() {
        return bidList;
    }

    public void setBidList(List<Bid> bidList) {
        this.bidList = bidList;
    }

    public List<Role> getRoleList() {
        return roleList;
    }

    public void setRoleList(List<Role> roleList) {
        this.roleList = roleList;
    }    

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof User)) {
            return false;
        }
        User other = (User) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "lk.ncisl.obs.data.domain.User[ id=" + id + " ]";
    }
    
}
