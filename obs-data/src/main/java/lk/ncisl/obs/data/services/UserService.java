/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package lk.ncisl.obs.data.services;

import lk.ncisl.obs.data.domain.User;

import java.util.List;

/**
 *
 * @author vimukthi
 */
public interface UserService {

    public User getSystemUser();

    public List<User> getAllUsers();
    
}
