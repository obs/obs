package lk.ncisl.obs.data.domain;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author vimukthi
 */
@Entity
@Table(name = "bid_state")
@NamedQueries({
    @NamedQuery(name = "BidState.findAll", query = "SELECT b FROM BidState b"),
    @NamedQuery(name = "BidState.findById", query = "SELECT b FROM BidState b WHERE b.id = :id"),
    @NamedQuery(name = "BidState.findByName", query = "SELECT b FROM BidState b WHERE b.name = :name"),
    @NamedQuery(name = "BidState.findByDisplayName", query = "SELECT b FROM BidState b WHERE b.displayName = :displayName"),
    @NamedQuery(name = "BidState.findByDescription", query = "SELECT b FROM BidState b WHERE b.description = :description"),
    @NamedQuery(name = "BidState.findByDateModified", query = "SELECT b FROM BidState b WHERE b.dateModified = :dateModified")})
public class BidState implements Serializable {

    public interface ValidStates {
        public static final String PHASE1_REJECTED = "phase1_rejected";
        public static final String PHASE1_ACTIVE = "phase1_active";
        public static final String PHASE1_ACCEPTED = "phase1_accepted";
        public static final String PHASE2_ACTIVE = "phase2_active";
        public static final String REJECTED = "rejected";
        public static final String ACCEPTED = "accepted";
        public static final String CANCELLED = "cancelled";
        public static final String ADMIN_CANCELLED = "admin_cancelled";
        public static final String PHASE1_ONHOLD = "phase1_onhold";
        public static final String PHASE2_ONHOLD = "phase2_onhold";
    }
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "name")
    private String name;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "display_name")
    private String displayName;
    @Size(max = 100)
    @Column(name = "description")
    private String description;
    @Basic(optional = false)
    @NotNull
    @Column(name = "date_modified")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateModified;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "state1")
    private List<ValidBidTransition> validBidTransitions;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "bidState")
    private List<ValidOrderBidStateCombo> validOrderBidStateCombinations;

    public BidState() {
    }

    public Integer getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getDisplayName() {
        return displayName;
    }

    public String getDescription() {
        return description;
    }

    public Date getDateModified() {
        return dateModified;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof BidState)) {
            return false;
        }
        BidState other = (BidState) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "lk.ncisl.obs.data.domain.BidState[ id=" + id + " ]";
    }

    public List<ValidBidTransition> getValidBidTransitions() {
        return validBidTransitions;
    }

    public List<ValidOrderBidStateCombo> getValidOrderBidStateCombinations() {
        return validOrderBidStateCombinations;
    }
}
