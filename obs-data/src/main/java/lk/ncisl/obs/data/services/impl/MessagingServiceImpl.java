package lk.ncisl.obs.data.services.impl;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;

import lk.ncisl.obs.data.domain.Message;
import lk.ncisl.obs.data.domain.User;
import lk.ncisl.obs.data.services.MessagingService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;

/**
 *
 * @author prabhath
 */
public class MessagingServiceImpl implements MessagingService {

    @Autowired
    private LocalContainerEntityManagerFactoryBean emf;

    @Override
    public void add(Message message) {
        if (message.getTitle()!=null) {
            EntityManager em = emf.getNativeEntityManagerFactory().createEntityManager();
            
            if(message.getState()==null){
                message.setState(0);
            }

            em.persist(message);
            // start a transaction manually to update record
            EntityTransaction tx = em.getTransaction();
            tx.begin();
            em.flush();
            tx.commit();
            
            //if has parent make parent unread as well
            if(message.getParent()!=null){
                Message parentMsg = (Message) em.createNamedQuery("Message.findById").
                    setParameter("id", message.getId()).getSingleResult();
                markAsRead(parentMsg);
            }
            
        }
    }

    @Override
    public void sendMessage(String title, String body, User sender, User receiver) {
        this.add(new Message(title, body, sender, receiver));
    }
    
    @Override
    public void markAsRead(Message message) {
            EntityManager em = emf.getNativeEntityManagerFactory().createEntityManager();
                        
            message.setState(1);
            
            em.merge(message);


            try {
                // start a transaction manually to update record
                EntityTransaction tx = em.getTransaction();
                tx.begin();
                em.flush();
                tx.commit();
            } catch (ConstraintViolationException e) {
                for (ConstraintViolation cv : e.getConstraintViolations()) {
                    System.out.println(cv);
                }

            }


            
    }


    public void setEmf(LocalContainerEntityManagerFactoryBean emf) {
        this.emf = emf;
    }
}
