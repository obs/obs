/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package lk.ncisl.obs.data.services;

import lk.ncisl.obs.data.domain.Message;
import lk.ncisl.obs.data.domain.User;

/**
 *
 * @author prabhath
 */
public interface MessagingService {

    public void add(Message message);

    public void sendMessage(String title, String body, User sender, User receiver);
    
    public void markAsRead(Message message);
    
}
