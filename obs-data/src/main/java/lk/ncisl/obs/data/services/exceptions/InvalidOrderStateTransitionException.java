

package lk.ncisl.obs.data.services.exceptions;

/**
 *
 * @author vimukthi
 */
public class InvalidOrderStateTransitionException extends Exception {

    public InvalidOrderStateTransitionException(String message) {
        super(message);
    }

}
