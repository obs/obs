
package lk.ncisl.obs.data.services;

import lk.ncisl.obs.data.services.exceptions.NonActiveOrderException;
import lk.ncisl.obs.data.domain.Bid;
import lk.ncisl.obs.data.services.exceptions.InvalidBidStateTransitionException;
import lk.ncisl.obs.data.services.exceptions.InvalidOrderStateForTransactionException;

/**
 *
 * @author vimukthi
 */
public interface BiddingService {
    
    /**
     * add new bid
     * @param bid 
     */
    public void add(Bid bid) throws NonActiveOrderException;

    /**
     * move the bid to admin_cancel state
     * @param bid 
     */
    public void adminCancel(Bid bid) throws InvalidBidStateTransitionException, InvalidOrderStateForTransactionException;
    
    /**
     * 
     * @param bid 
     */
    public void cancel(Bid bid) throws InvalidBidStateTransitionException, InvalidOrderStateForTransactionException;
    
    /**
     * 
     * @param bid 
     */
    public void accept(Bid bid) throws InvalidBidStateTransitionException, InvalidOrderStateForTransactionException;
    
    /**
     * 
     * @param bid 
     */
    public void reject(Bid bid) throws InvalidBidStateTransitionException, InvalidOrderStateForTransactionException;
    
    /**
     * 
     * @param bid 
     */
    public void hold(Bid bid) throws InvalidBidStateTransitionException, InvalidOrderStateForTransactionException;
    
    /**
     * 
     * @param bid 
     */
    public void unhold(Bid bid) throws InvalidBidStateTransitionException, InvalidOrderStateForTransactionException;

    /**
     *
     * @param observer
     */
    public void setBidStateObserver(BidStateObserver observer);
    
}
