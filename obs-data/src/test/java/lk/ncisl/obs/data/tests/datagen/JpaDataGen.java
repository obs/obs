package lk.ncisl.obs.data.tests.datagen;

import java.util.Date;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import lk.ncisl.obs.data.domain.Role;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;

@ContextConfiguration(locations = {"classpath:test-beans.xml"})
@Transactional
@TransactionConfiguration(transactionManager = "transactionManager")
@RunWith(SpringJUnit4ClassRunner.class)
public class JpaDataGen {

    @PersistenceContext
    private EntityManager em;    

    @Before
    public void setUp() {
        Role test = new Role();
        test.setName("test");
        test.setDateCreated(new Date());
        em.persist(test);
        em.flush();
    }

    @After
    public void tearDown() {
    }
    
    @Test
    public void testGenerate() throws Exception {
        createRoles();
    }
    
    private void createRoles(){
//        Role admin = new Role();
//        admin.setName("admin");
//        admin.setDateCreated(new Date());
//        em.persist(admin);
//        
//        Role bidder = new Role();
//        bidder.setName("bidder");
//        bidder.setDateCreated(new Date());
//        em.persist(bidder);
        
//        Role test = new Role();
//        test.setName("test");
//        test.setDateCreated(new Date());
//        em.persist(test);
//        em.flush();
    }
    
    private void createPermissions(){
        
    }
    
    private void createPreferences(){
        
    }
    
    private void createUsers(){
        
    }    

    public EntityManager getEm() {
        return em;
    }

    public void setEm(EntityManager em) {
        this.em = em;
    }
    
}
