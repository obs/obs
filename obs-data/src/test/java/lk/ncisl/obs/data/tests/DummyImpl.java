package lk.ncisl.obs.data.tests;

import lk.ncisl.obs.data.domain.Bid;
import lk.ncisl.obs.data.domain.OrderRequest;
import lk.ncisl.obs.data.services.BidStateObserver;
import lk.ncisl.obs.data.services.OrderStateObserver;

/**
 * Created by vimukthi on 5/1/14.
 */
public class DummyImpl implements OrderStateObserver, BidStateObserver {
    @Override
    public void stateChanged(Bid bid) {

    }

    @Override
    public void stateChanged(OrderRequest order) {

    }
}
