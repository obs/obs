package com.vimukthi.util.standalonewar;

import java.io.FileInputStream;
import java.security.ProtectionDomain;
import java.net.URL;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.naming.NamingException;
import org.apache.tomcat.jdbc.pool.DataSource;
import org.apache.tomcat.jdbc.pool.PoolProperties;
import org.eclipse.jetty.plus.jndi.Resource;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.webapp.WebAppContext;

/**
 *
 * @author vimukthi
 */
public class WarRunner {

    public static final String[] DEFAULT_CONFIG_CLASSES = {
        "org.eclipse.jetty.webapp.WebInfConfiguration", //init webapp structure
        "org.eclipse.jetty.plus.webapp.PlusConfiguration", //add for jndi
        "org.eclipse.jetty.plus.webapp.EnvConfiguration", //process jetty-env
        "org.eclipse.jetty.webapp.WebXmlConfiguration", //process web.xml
        //"org.eclipse.jetty.annotations.AnnotationConfiguration", // annotations
        "org.eclipse.jetty.webapp.JettyWebXmlConfiguration",//process jetty-web.xml
    };
    private PoolProperties poolProps;
    private DataSource ds;
    private Integer port = 8080;
    private String contextPath = "/";
    private String loggingConfig;
    private Boolean runModeProduction = false;

    public static void main(String[] args) {
        WarRunner runner = new WarRunner();
        try {
            runner.loadConfigurationFromFile(args[0].trim());
            runner.startWebApp();
        } catch (ArrayIndexOutOfBoundsException e) {
            System.out.println("Error : Please provide the configuration file");
        }
    }

    /**
     * starts the webapp in war with the provided configurations
     */
    private void startWebApp() {
        Server server = new Server(port);
        WebAppContext context = new WebAppContext();
        context.setConfigurationClasses(DEFAULT_CONFIG_CLASSES);
        context.setServer(server);
        context.setContextPath(contextPath);        
        try {
            Resource dsResource = new Resource("jdbc/obsdb", ds);
            server.setAttribute("obsdb", dsResource);
            Resource loggingConfigResource = new Resource("application/loggingconfig", loggingConfig);
            server.setAttribute("loggingConfig", loggingConfigResource);
            Resource runModeResource = new Resource("application/runmode", runModeProduction);
            server.setAttribute("runMode", runModeResource);
            ProtectionDomain protectionDomain = WarRunner.class.getProtectionDomain();
            URL location = protectionDomain.getCodeSource().getLocation();
            context.setWar(location.toExternalForm());

            server.setHandler(context);
            try {
                server.start();
                System.in.read();
                server.stop();
                server.join();
            } catch (Exception e) {
                e.printStackTrace();
                System.exit(100);
            }
        } catch (NamingException ex) {
            Logger.getLogger(WarRunner.class.getName()).log(Level.SEVERE, "-------------------------------", ex);
        }

    }

    private void loadConfigurationFromFile(String filename) {
        Properties properties = new Properties();
        try {
            FileInputStream fis = new FileInputStream(filename);
            properties.load(fis);
            poolProps = new PoolProperties();
            poolProps.setUrl(properties.getProperty("db.jdbc.connectionpool.url"));
            poolProps.setDriverClassName(properties.getProperty("db.jdbc.connectionpool.driverclassname"));
            poolProps.setUsername(properties.getProperty("db.jdbc.connectionpool.username"));
            poolProps.setPassword(properties.getProperty("db.jdbc.connectionpool.password"));
            poolProps.setJmxEnabled(properties.getProperty("db.jdbc.connectionpool.jmxenabled").equals("true"));
            poolProps.setTestWhileIdle(properties.getProperty("db.jdbc.connectionpool.testwhileidle").equals("true"));
            poolProps.setTestOnBorrow(properties.getProperty("db.jdbc.connectionpool.testonborrow").equals("true"));
            poolProps.setValidationQuery(properties.getProperty("db.jdbc.connectionpool.validationquery"));
            poolProps.setTestOnReturn(properties.getProperty("db.jdbc.connectionpool.testonreturn").equals("true"));
            poolProps.setValidationInterval(Long.valueOf(properties.getProperty("db.jdbc.connectionpool.validationinterval")));
            poolProps.setTimeBetweenEvictionRunsMillis(Integer.valueOf(properties.getProperty("db.jdbc.connectionpool.timebetweenevictionrunsmillis")));
            poolProps.setMaxActive(Integer.valueOf(properties.getProperty("db.jdbc.connectionpool.maxactive")));
            poolProps.setInitialSize(Integer.valueOf(properties.getProperty("db.jdbc.connectionpool.initialsize")));
            poolProps.setMaxWait(Integer.valueOf(properties.getProperty("db.jdbc.connectionpool.maxwait")));
            poolProps.setRemoveAbandonedTimeout(Integer.valueOf(properties.getProperty("db.jdbc.connectionpool.removeabandonedtimeout")));
            poolProps.setMinEvictableIdleTimeMillis(Integer.valueOf(properties.getProperty("db.jdbc.connectionpool.minevictableidletimemillis")));
            poolProps.setMinIdle(Integer.valueOf(properties.getProperty("db.jdbc.connectionpool.minidle")));
            poolProps.setLogAbandoned(properties.getProperty("db.jdbc.connectionpool.logabandoned").equals("true"));
            poolProps.setRemoveAbandoned(properties.getProperty("db.jdbc.connectionpool.removeabandoned").equals("true"));
            poolProps.setJdbcInterceptors(properties.getProperty("db.jdbc.connectionpool.jdbcinterceptors"));
            ds = new DataSource();
            ds.setPoolProperties(poolProps);
            port = Integer.valueOf(properties.getProperty("application.http.port"));
            contextPath = properties.getProperty("application.http.contextpath");
            loggingConfig = properties.getProperty("application.logging.config");
            runModeProduction = properties.getProperty("application.runmode.production").equals("true");
        } catch (Exception e) {
            System.out.println("Error in properties file specified in command parameter");
            e.printStackTrace();
            System.exit(100);
        }
    }
}
