package lk.ncisl.obs.fe.user;

import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import java.util.Map;
import lk.ncisl.obs.data.domain.User;
import lk.ncisl.obs.fe.ManageableComponent;

/**
 *
 * @author vimukthi
 */
public class UserAdminToolbar extends HorizontalLayout implements ManageableComponent {
    
    private Button addUser;
    
    private Button editUser;
    
    private Button suspendUser;

    @Override
    public void onShow(Map<String, Object> params) {
        setWidth("100%");
        setSpacing(true);
        setMargin(true);
        addStyleName("toolbar");

        // title
        Label title = new Label("Manage Users");
        title.addStyleName("h1");
        title.setSizeUndefined();
        addComponent(title);
        setComponentAlignment(title, Alignment.MIDDLE_LEFT);

        // buttons
        addUser = new Button("Add User");
        addUser.addStyleName("small");
        addComponent(addUser);
        setComponentAlignment(addUser, Alignment.MIDDLE_RIGHT);

        editUser = new Button("Edit User");
        editUser.setEnabled(false);
        editUser.addStyleName("small");
        addComponent(editUser);
        setComponentAlignment(editUser, Alignment.MIDDLE_LEFT);

        suspendUser = new Button("Suspend User");
        suspendUser.setEnabled(false);
        suspendUser.addStyleName("small");
        addComponent(suspendUser);
        setComponentAlignment(suspendUser, Alignment.MIDDLE_LEFT);
    }
    
    public void enableRowSpecificButtons(User selectedUser) {
        editUser.setEnabled(true);
        changeSuspendButtonCaption(selectedUser.isActive());
        suspendUser.setEnabled(true);        
    }
    
    public void changeSuspendButtonCaption(Boolean userActive){
        if (userActive){
            suspendUser.setCaption("Suspend User");            
        } else {
            suspendUser.setCaption("Activate User");
        }
    }

    @Override
    public void onHide() {
        removeAllComponents();
    }

    public Button getAddUserButton() {
        return addUser;
    }

    public Button getEditUserButton() {
        return editUser;
    }

    public Button getSuspendUserButton() {
        return suspendUser;
    }   
}
