package lk.ncisl.obs.fe.user;

import com.vaadin.addon.jpacontainer.EntityItem;
import com.vaadin.addon.jpacontainer.JPAContainer;
import com.vaadin.addon.jpacontainer.provider.CachingMutableLocalEntityProvider;
import com.vaadin.server.VaadinSession;
import com.vaadin.shared.communication.PushMode;
import com.vaadin.ui.UI;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import lk.ncisl.obs.data.domain.Role;
import lk.ncisl.obs.data.domain.User;
import lk.ncisl.obs.fe.ManageableComponent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;

/**
 *
 * @author vimukthi
 */
public class UserContainer extends JPAContainer<User> implements ManageableComponent {

    private EntityManager em;
    @Autowired
    private LocalContainerEntityManagerFactoryBean emf;

    public UserContainer() {
        super(User.class);
    }

    @Override
    public void onShow(Map<String, Object> params) {
        this.em = emf.getNativeEntityManagerFactory().createEntityManager();
        setEntityProvider(
                new CachingMutableLocalEntityProvider<User>(
                        User.class, em));
        refresh();
    }

    @Override
    public void onHide() {
        //setEntityProvider(null);
    }

    /**
     * Get user for email address
     *
     * @param email
     * @return
     */
    public User getUserByEmail(String email) {
        User user = (User) em.createNamedQuery("User.findByEmail").setParameter("email", email).getSingleResult();
        return user;
    }

    public User getDetachedUser(User persistentUser) {
        em.detach(persistentUser);
        return persistentUser;
    }

    public User getCurrentUser() {
        // get persistent object for the user in session
        UI.getCurrent().getPushConfiguration().setPushMode(PushMode.DISABLED);
        User currentUser = (User) VaadinSession.getCurrent().getSession().getAttribute("user");
        UI.getCurrent().getPushConfiguration().setPushMode(PushMode.AUTOMATIC);
        //em.persist(currentUser);
        return currentUser;
    }

    public void addBiddingUser(User user) {
        Role bidder = (Role) em.createNamedQuery("Role.findByName").setParameter("name", "bidder").getSingleResult();
        List<Role> roleList = new ArrayList<Role>();
        roleList.add(bidder);
        user.setRoleList(roleList);
        user.setDateCreated(new Date());
        addEntity(user);
    }

    public List<User> getBiddingUsers() {
        List<User> bidders = new ArrayList<User>();
        for (Iterator i = getItemIds().iterator(); i.hasNext();) {
            User user = getItem((Integer) i.next()).getEntity();
            if (user.isInRole("bidder")) {
                bidders.add(user);
            }
        }
        return bidders;
    }

    public void saveUser(User user) {
        getEm().merge(user);
        // start a transaction manually to update record
        EntityTransaction tx = getEm().getTransaction();
        tx.begin();
        getEm().flush();
        tx.commit();
    }

    public List<User> getContactableUsers(User sender) {
        List<User> users = new ArrayList<User>();
        
        for (Iterator i = getItemIds().iterator(); i.hasNext();) {
            User user = getItem((Integer) i.next()).getEntity();
            boolean canContact = true;
            
            //only admin can send contact bidders
            if (user.isInRole("bidder") && !sender.isAdmin()) {
                canContact = false;
            }
            
            //avoid self messages
            if(user.equals(sender)){
                canContact = false;
            }
            
            if(canContact){
                users.add(user);
            }
            
        }
        return users;
    }

    public EntityManager getEm() {
        return em;
    }

    public void setEm(EntityManager em) {
        this.em = em;
    }
}
