package lk.ncisl.obs.fe.layout;

import com.vaadin.navigator.View;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.UI;
import java.util.Map;
import lk.ncisl.obs.fe.ExtendedNavigator;
import lk.ncisl.obs.fe.ManageableComponent;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author vimukthi
 */
public class MainLayout extends HorizontalLayout implements ManageableComponent {

    private ExtendedNavigator nav;
    
    @Autowired
    private SidebarLayout sideBar;
    
    @Autowired
    private ContentLayout content;
    
    private Map<String, View> routes;

    @Override
    public void onShow(Map<String, Object> params) {
        setSizeFull();
        addStyleName("main-view");
        
        // init navigator
        nav = new ExtendedNavigator(UI.getCurrent(), content);
        params.put("navigator", nav);
        nav.setRoutes(routes);
        nav.onShow(null);
        
        // add main components
        addComponent(sideBar);
        sideBar.onShow(params);
        addComponent(content);
        content.onShow(params);
        content.setSizeFull();
        setExpandRatio(content, 1);
    }

    @Override
    public void onHide() {
        sideBar.onHide();
        content.onHide();
        removeAllComponents();
    }

    public ExtendedNavigator getNav() {
        return nav;
    }

    public void setNav(ExtendedNavigator nav) {
        this.nav = nav;
    }
    
    public SidebarLayout getSideBar() {
        return sideBar;
    }

    public void setSideBar(SidebarLayout sideBar) {
        this.sideBar = sideBar;
    }

    public ContentLayout getContent() {
        return content;
    }

    public void setContent(ContentLayout content) {
        this.content = content;
    }

    public Map<String, View> getRoutes() {
        return routes;
    }

    public void setRoutes(Map<String, View> routes) {
        this.routes = routes;
    }  
    
}
