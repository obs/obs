package lk.ncisl.obs.fe.layout;

import com.vaadin.shared.ui.label.ContentMode;
import com.vaadin.ui.Label;
import com.vaadin.ui.VerticalLayout;
import java.util.Map;
import lk.ncisl.obs.fe.ManageableComponent;
import lk.ncisl.obs.fe.utils.StaticUtils;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author vimukthi
 */
public class SidebarLayout extends VerticalLayout implements ManageableComponent {
    
    @Autowired
    private BrandingLayout branding;
    
    @Autowired
    private MenuLayout mainMenu;
    
    @Autowired
    private UserMenuLayout userMenu;

    @Override
    public void onShow(Map<String, Object> params) {
        addStyleName("sidebar");
        setWidth("220px");
        setHeight("100%");
        branding.onShow(params);
        addComponent(branding);
        mainMenu.onShow(params);
        addComponent(mainMenu);
        userMenu.onShow(params);
        addComponent(userMenu);
        addComponent(new Label(StaticUtils.createdByGeoclipseHtml(), ContentMode.HTML));
        setExpandRatio(mainMenu, 1);
    }

    @Override
    public void onHide() {
        branding.onHide();
        mainMenu.onHide();
        userMenu.onHide();
        removeAllComponents();
    }

    public BrandingLayout getBranding() {
        return branding;
    }

    public void setBranding(BrandingLayout branding) {
        this.branding = branding;
    }

    public MenuLayout getMainMenu() {
        return mainMenu;
    }

    public void setMainMenu(MenuLayout mainMenu) {
        this.mainMenu = mainMenu;
    }

    public UserMenuLayout getUserMenu() {
        return userMenu;
    }

    public void setUserMenu(UserMenuLayout userMenu) {
        this.userMenu = userMenu;
    }   
}
