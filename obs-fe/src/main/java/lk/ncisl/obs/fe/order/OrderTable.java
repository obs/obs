package lk.ncisl.obs.fe.order;

import com.vaadin.ui.Table;
import java.util.Map;
import lk.ncisl.obs.fe.ManageableComponent;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author vimukthi
 */
public class OrderTable extends Table implements ManageableComponent {
    
    @Autowired
    private OrderContainer container;

    public OrderContainer getContainer() {
        return container;
    }

    public void setContainer(OrderContainer container) {
        this.container = container;
    }

    @Override
    public void onShow(Map<String, Object> params) {
        container.onShow(params);
        setContainerDataSource(container);
        setSizeFull();
    }

    @Override
    public void onHide() {
    }
    
    
    
}
