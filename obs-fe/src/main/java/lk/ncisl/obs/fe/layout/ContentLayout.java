package lk.ncisl.obs.fe.layout;

import com.vaadin.ui.CssLayout;
import java.util.Map;
import lk.ncisl.obs.fe.ManageableComponent;

/**
 *
 * @author vimukthi
 */
public class ContentLayout extends CssLayout implements ManageableComponent {

    @Override
    public void onShow(Map<String, Object> params) {
        setSizeFull();
        addStyleName("view-content");
    }

    @Override
    public void onHide() {
        removeAllComponents();
    }
}
