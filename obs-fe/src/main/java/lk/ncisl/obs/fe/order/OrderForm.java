package lk.ncisl.obs.fe.order;

import com.vaadin.data.fieldgroup.BeanFieldGroup;
import com.vaadin.data.fieldgroup.FieldGroup;
import com.vaadin.data.fieldgroup.FieldGroup.CommitException;
import com.vaadin.data.validator.IntegerRangeValidator;
import com.vaadin.data.validator.StringLengthValidator;
import com.vaadin.server.FileResource;
import com.vaadin.shared.ui.datefield.Resolution;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.DateField;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.Image;
import com.vaadin.ui.Label;
import com.vaadin.ui.Notification;
import com.vaadin.ui.RichTextArea;
import com.vaadin.ui.TextArea;
import com.vaadin.ui.TextField;
import com.vaadin.ui.Upload;
import com.vaadin.ui.Upload.Receiver;
import com.vaadin.ui.Upload.SucceededListener;
import com.vaadin.ui.Window;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.util.Map;
import lk.ncisl.obs.data.domain.OrderRequest;
import lk.ncisl.obs.data.services.OrderRequestService;
import lk.ncisl.obs.data.services.exceptions.InvalidOrderStateTransitionException;
import lk.ncisl.obs.fe.InvariantConfigs;
import lk.ncisl.obs.fe.ManageableComponent;
import lk.ncisl.obs.fe.user.UserContainer;
import lk.ncisl.obs.data.utils.ObsTimeUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author vimukthi
 */
public class OrderForm extends FormLayout implements ManageableComponent, Receiver, SucceededListener {

    private final static Logger logger = LoggerFactory.getLogger(OrderForm.class);
    private OrderRequest order;
    private Window parent;
    private String uploadLocation;
    private File orderImage;
    private String orderImageFileName;
    private Image orderImageDisplay = new Image();
    private BeanFieldGroup<OrderRequest> binder;
    @Autowired
    private OrderRequestService orderService;
    @Autowired
    private UserContainer users;
    // fields
    private TextField code;
    private TextArea summary;
    private RichTextArea description;
    private TextField quantity;
    private DateField expirationDate;
    private Upload upload;
    private Label errorMessage = new Label();
    private Boolean startPhase2 = false;
    private Boolean newOrder = false;

    @Override
    public void onShow(Map<String, Object> params) {
        // Form for editing the bean
        binder = new BeanFieldGroup<OrderRequest>(OrderRequest.class);
        parent = (Window) params.get("window");
        startPhase2 = params.containsKey("phase2");

        // this is a new user addition
        if (params.containsKey("new")) {
            newOrder = true;
            order = new OrderRequest();
            binder.setItemDataSource(order);
            bindFields(binder, true);

            // Buffer the form content
            binder.setBuffered(true);
            addComponent(new Button("OK", new ClickListener() {
                @Override
                public void buttonClick(ClickEvent event) {
                    try {
                        binder.commit();
                        order.setImage(orderImageFileName);
                        order.setDateCreated(ObsTimeUtils.getCurrentDateTimeForZone(users.getCurrentUser().getTimeZone()));
                        order.setTimeZone(users.getCurrentUser().getTimeZone());
                        // container handles transaction here
                        orderService.startOrderRequest(order);
                        if (parent != null) {
                            parent.close();
                        }
                    } catch (CommitException e) {
                        logger.error("validation", e);
                        // add validation errors on each field here
                        errorMessage.setValue("check inputs");
                        errorMessage.setVisible(true);
                    }
                }
            }));
        } else {
            // this is an existing user edition
            order = (OrderRequest) params.get("order");

            binder.setItemDataSource(order);
            bindFields(binder, false);

            // Buffer the form content
            binder.setBuffered(true);
            addComponent(new Button("OK", new ClickListener() {
                @Override
                public void buttonClick(ClickEvent event) {
                    try {
                        binder.commit();
                        if (startPhase2) {
                            orderService.startPhase2(order);
                        } else {
                            orderService.save(order);
                        }
                        if (parent != null) {
                            parent.close();
                        }
                    } catch (CommitException e) {
                        logger.error("validation", e);
                        // add validation errors on each field here
                        errorMessage.setValue("check inputs");
                        errorMessage.setVisible(true);
                    } catch (InvalidOrderStateTransitionException e) {
                        logger.error("validation", e);
                    }
                }
            }));
        }
    }

    /**
     *
     * @param binder
     * @param neworder
     * @throws com.vaadin.data.fieldgroup.FieldGroup.BindException
     */
    private void bindFields(BeanFieldGroup<OrderRequest> binder, boolean neworder) throws FieldGroup.BindException {
        errorMessage.setVisible(false);
        addComponent(errorMessage);
        if (neworder) {
            // code if new
            code = new TextField("Code");
            code.addValidator(new StringLengthValidator("Can not be empty", 1, 50, false));
            addComponent(code);
            binder.bind(code, "code");
        }
        // summary
        summary = new TextArea("Summary");
        summary.addValidator(new StringLengthValidator("Can not be empty", 1, 200, false));
        binder.bind(summary, "summary");

        // description
        description = new RichTextArea("Description");
        description.addValidator(new StringLengthValidator("Can not be empty", 1, 5000, false));
        binder.bind(description, "description");

        // quantity
        quantity = new TextField("Quantity");
        quantity.addValidator(new IntegerRangeValidator("Non zero", 1, Integer.MAX_VALUE));
        binder.bind(quantity, "quantity");

        // expiration date
        expirationDate = new DateField("Expires on");
        expirationDate.setRangeStart(ObsTimeUtils.getCurrentDateTimeForZone(users.getCurrentUser().getTimeZone()));
        expirationDate.setResolution(Resolution.SECOND);
        expirationDate.setDateFormat("yyyy-MM-dd HH:mm:ss");
        binder.bind(expirationDate, "expirationDate");

        // image upload
        upload = new Upload("Upload Image", this);
        upload.addSucceededListener(this);
        prepareOrderImageDisplay();

        if (!startPhase2) {
            addComponent(summary);
            addComponent(description);
            addComponent(quantity);
        }
        addComponent(expirationDate);
        addComponent(upload);
        addComponent(orderImageDisplay);
    }

    private void prepareOrderImageDisplay() {
        if (newOrder) {
            orderImageDisplay.setVisible(false);
        } else {
            orderImageDisplay.setVisible(true);
            orderImageFileName = order.getImage();
            // Open the file for writing.
            orderImage = new File(uploadLocation + InvariantConfigs.ORDERS_IMAGES_PATH + orderImageFileName);
            orderImageDisplay.setSource(new FileResource(orderImage));
            orderImageDisplay.setWidth(InvariantConfigs.ORDERLIST_IMAGE_DISPLAY_WIDTH);
            orderImageDisplay.setHeight(InvariantConfigs.ORDERLIST_IMAGE_DISPLAY_HEIGHT);
            order.setImage(orderImageFileName);
        }
    }

    @Override
    public OutputStream receiveUpload(String filename, String mimeType) {
        // Create upload stream
        FileOutputStream fos = null; // Stream to write to
        try {
            orderImageFileName = filename;
            // Open the file for writing.
            orderImage = new File(uploadLocation + InvariantConfigs.ORDERS_IMAGES_PATH + filename);
            fos = new FileOutputStream(orderImage);
        } catch (FileNotFoundException e) {
            Notification.show("Error occured while uploading", Notification.Type.ERROR_MESSAGE);
            logger.error("Image Upload error", e);
            return null;
        }
        return fos; // Return the output stream to write to
    }

    @Override
    public void uploadSucceeded(Upload.SucceededEvent event) {
        // Show the uploaded file in the image viewer
        orderImageDisplay.setVisible(true);
        orderImageDisplay.setSource(new FileResource(orderImage));
        orderImageDisplay.setWidth(InvariantConfigs.ORDERLIST_IMAGE_DISPLAY_WIDTH);
        orderImageDisplay.setHeight(InvariantConfigs.ORDERLIST_IMAGE_DISPLAY_HEIGHT);
        order.setImage(orderImageFileName);
    }

    @Override
    public void onHide() {
        binder.discard();
        removeAllComponents();
    }

    public String getUploadLocation() {
        return uploadLocation;
    }

    public void setUploadLocation(String uploadLocation) {
        this.uploadLocation = uploadLocation;
    }
}
