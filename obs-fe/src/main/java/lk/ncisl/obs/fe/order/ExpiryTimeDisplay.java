package lk.ncisl.obs.fe.order;

import lk.ncisl.obs.fe.system.ExpiryTimeCounter;
import lk.ncisl.obs.fe.utils.StaticUtils;

import com.vaadin.shared.ui.label.ContentMode;
import com.vaadin.ui.Label;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author vimukthi
 */
public class ExpiryTimeDisplay extends Label implements ExpiryTimeCounter.Updatable {

    private final static Logger logger = LoggerFactory.getLogger(ExpiryTimeDisplay.class);
    private OrderContainer refreshable;

    public ExpiryTimeDisplay() {
        setContentMode(ContentMode.HTML);
    }

    public ExpiryTimeDisplay(String content, OrderContainer refreshable) {
        super(content);
        setContentMode(ContentMode.HTML);
        this.refreshable = refreshable;
    }

    @Override
    public void update(final long diffDays, final long diffHours, final long diffMinutes, final long diffSeconds) {
        //logger.debug(this.getUI().toString());
        this.getUI().access(new Runnable() {
            @Override
            public void run() {
                if (isVisible()) {
                    setValue(StaticUtils.lableCaptionWrap("Expiring in: ") + diffDays + " d: " + diffHours + " H: " + diffMinutes + " m: " + diffSeconds + " s");
                }
            }
        });
    }

    @Override
    public void refreshParent() {
        this.getUI().access(new Runnable() {
            @Override
            public void run() {
                refreshable.refresh();
            }
        });
    }
}
