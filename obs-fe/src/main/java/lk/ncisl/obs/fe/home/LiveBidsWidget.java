/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package lk.ncisl.obs.fe.home;

import com.vaadin.ui.VerticalLayout;
import java.util.List;
import java.util.Map;
import lk.ncisl.obs.data.domain.Bid;
import lk.ncisl.obs.fe.ManageableComponent;
import lk.ncisl.obs.fe.bid.BidContainer;
import lk.ncisl.obs.fe.bid.BidTable;
import lk.ncisl.obs.fe.user.UserContainer;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author prabhathd
 */
public class LiveBidsWidget extends VerticalLayout implements ManageableComponent {
    @Autowired
    private BidContainer bids;    
    @Autowired
    private UserContainer users;
    @Autowired
    private BidTable table;    
   
    VerticalLayout bidArea;
    private List<Bid> visibleBids;    
    private Integer itemIndex = 0;
   
    @Override
    public void onShow(Map<String, Object> params) {
        setSizeFull();
        setStyleName("livebidding-area");
        bids.onShow(params);
        
        bidArea= new VerticalLayout();
        bidArea.setSizeFull();        
        addComponent(bidArea);
        
        addNewBidToUI(params);
    }

    @Override
    public void onHide() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    public void addNewBidToUI(Map<String, Object> params){     
//        bids.sort(new String[] { "pricePerUnit" }, new boolean[] { true });
//        Bid newBid = (Bid) bids.getItem(  bids.getIdByIndex(itemIndex) ).getEntity();
//        BidWidget bidWidget = new BidWidget(newBid, users.getCurrentUser());
//        bidWidget.hideOrderCode();
//        bidArea.addComponent(bidWidget); 
//        
//        itemIndex++;
        
        params.put("allBidList", "allBidList");
        bidArea.addComponent(table);
        table.setSizeFull();
        table.onShow(params);
        
    }
    
        
}
