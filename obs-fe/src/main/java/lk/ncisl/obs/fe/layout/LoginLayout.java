package lk.ncisl.obs.fe.layout;

import com.vaadin.annotations.JavaScript;
import com.vaadin.data.validator.CompositeValidator;
import com.vaadin.data.validator.EmailValidator;
import com.vaadin.event.ShortcutAction;
import com.vaadin.event.ShortcutListener;
import com.vaadin.external.org.slf4j.Logger;
import com.vaadin.external.org.slf4j.LoggerFactory;
import com.vaadin.server.VaadinSession;
import com.vaadin.shared.communication.PushMode;
import com.vaadin.shared.ui.label.ContentMode;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.CssLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Notification;
import com.vaadin.ui.PasswordField;
import com.vaadin.ui.TextField;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import com.wcs.wcslib.vaadin.widget.recaptcha.ReCaptcha;
import com.wcs.wcslib.vaadin.widget.recaptcha.shared.ReCaptchaOptions;
import java.util.Map;
import lk.ncisl.obs.data.domain.User;
import lk.ncisl.obs.fe.MainUI;
import lk.ncisl.obs.fe.ManageableComponent;
import lk.ncisl.obs.fe.user.UserContainer;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author vimukthi
 */
@JavaScript("http://www.google.com/recaptcha/api/js/recaptcha_ajax.js")
public class LoginLayout extends VerticalLayout implements ManageableComponent, Button.ClickListener {

    private MainUI ui;
    private CssLayout loginPanel;
    private TextField username;
    private PasswordField password;
    private Button signin;
    private ShortcutListener enter;
    private static Logger logger = LoggerFactory.getLogger(LoginLayout.class);
    @Autowired
    private UserContainer container;

    ReCaptcha captcha;

    @Override
    public void onShow(Map<String, Object> params) {
        this.ui = (MainUI) UI.getCurrent();
        container.onShow(params);
        setSizeFull();
        addStyleName("login-layout");

        loginPanel = new CssLayout();
        loginPanel.addStyleName("login-panel");

        HorizontalLayout labels = new HorizontalLayout();
        labels.setWidth("100%");
        labels.setMargin(true);
        labels.addStyleName("labels");
        loginPanel.addComponent(labels);

        Label welcome = new Label("Welcome");
        welcome.setSizeUndefined();
        welcome.addStyleName("h4");
        labels.addComponent(welcome);
        labels.setComponentAlignment(welcome, Alignment.MIDDLE_LEFT);

        Label title = new Label("NCI OBS");
        title.setSizeUndefined();
        title.addStyleName("h2");
        title.addStyleName("light");
        labels.addComponent(title);
        labels.setComponentAlignment(title, Alignment.MIDDLE_RIGHT);

        HorizontalLayout fields = new HorizontalLayout();
        fields.setSpacing(true);
        fields.setMargin(true);
        fields.addStyleName("fields");

        username = new TextField("Email");
        username.focus();
        username.setRequired(true);
        CompositeValidator usernameValidator = new CompositeValidator(
                CompositeValidator.CombinationMode.AND, "Invalid email address");
        usernameValidator.addValidator(new EmailValidator("Invalid email address"));
        username.addValidator(new EmailValidator("Invalid email address"));
        fields.addComponent(username);

        password = new PasswordField("Password");
//        CompositeValidator passwordValidator = new CompositeValidator(
//                CompositeValidator.CombinationMode.AND, "Invalid password");
        password.setRequired(true);
        fields.addComponent(password);

        signin = new Button("Sign In");
        signin.addStyleName("default");

        enter = new ShortcutListener("Sign In",
                ShortcutAction.KeyCode.ENTER, null) {
                    @Override
                    public void handleAction(Object sender, Object target) {
                        signin.click();
                    }
                };
        signin.addClickListener(this);
        signin.addShortcutListener(enter);

        loginPanel.addComponent(fields);

        HorizontalLayout fields_level2 = new HorizontalLayout();
        fields_level2.setSpacing(true);
        fields_level2.setMargin(true);
        fields_level2.addStyleName("fields");

        //captcha
        captcha = new ReCaptcha(
                "6LcN-vISAAAAABvQPXXeISvn4NgcB5AXO808hNM9",
                "6LcN-vISAAAAALK1K7qInOSW1DI049NFMBA39WsN",
                new ReCaptchaOptions() {
                    {
                        theme = "white";
                    }
                }
        );
        fields_level2.addComponent(captcha);

        fields_level2.addComponent(signin);
        fields_level2.setComponentAlignment(signin, Alignment.BOTTOM_LEFT);
        loginPanel.addComponent(fields_level2);

        addComponent(loginPanel);
        setComponentAlignment(loginPanel, Alignment.MIDDLE_CENTER);


        /* Hard-coded credentials */
        username.setValue("vimukthi@geoclipse.com");
        password.setValue("test123");

    }

    @Override
    public void onHide() {
        removeAllComponents();
    }

    /**
     * Handle signin
     *
     * @param event
     */
    @Override
    public void buttonClick(ClickEvent event) {
        try {
            username.validate();
            password.validate();
            String email = username.getValue();
            String passwd = password.getValue();
            User user = container.getUserByEmail(email);

            //captcha validation
            if (!captcha.validate()) {
                Notification.show("Invalid Captcha code entered !", Notification.Type.ERROR_MESSAGE);
                captcha.reload();
                logger.error("Invalid Captcha code entered !");
                //invalidLogin();
            } else {

                //continue with login
                if (user.getPassword().equals(passwd) && user.isActive()) {
                    try {
                        UI.getCurrent().getPushConfiguration().setPushMode(PushMode.DISABLED);
                        VaadinSession.getCurrent().getLockInstance().lock();
                        VaadinSession.getCurrent().getSession().setAttribute("user", user);
                        signin.removeShortcutListener(enter);
                        logger.debug("User logged in");
                        ui.buildMainView();
                    } catch (Exception e) {
                        logger.error("error", e);
                    } finally {
                        VaadinSession.getCurrent().getLockInstance().unlock();
                        UI.getCurrent().getPushConfiguration().setPushMode(PushMode.AUTOMATIC);
                    }
                } else {
                    invalidLogin();
                }
//            MessageDigest md = MessageDigest.getInstance("MD5");
//            byte[] passwdmd5 = md.digest(passwd.getBytes());
//            System.out.println("********************************** " + passwdmd5);

            }
        } catch (Exception e) {
            logger.error("Exception during login", e);
            invalidLogin();
        }
    }

    private void invalidLogin() {
        if (loginPanel.getComponentCount() > 2) {
            // Remove the previous error message
            loginPanel.removeComponent(loginPanel.getComponent(2));
        }
        // Add new error message
        Label error = new Label(
                "Wrong username or password",
                ContentMode.HTML);
        error.addStyleName("error");
        error.setSizeUndefined();
        error.addStyleName("light");
        // Add animation
        error.addStyleName("v-animate-reveal");
        loginPanel.addComponent(error);
        username.focus();
    }

    public UserContainer getContainer() {
        return container;
    }

    public void setContainer(UserContainer container) {
        this.container = container;
    }
}
