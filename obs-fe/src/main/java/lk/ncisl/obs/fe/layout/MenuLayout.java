package lk.ncisl.obs.fe.layout;

import com.vaadin.server.Page;
import com.vaadin.server.ThemeResource;
import com.vaadin.ui.Button;
import com.vaadin.ui.Component;
import com.vaadin.ui.CssLayout;
import com.vaadin.ui.NativeButton;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.TreeMap;
import lk.ncisl.obs.data.domain.User;
import lk.ncisl.obs.fe.ExtendedNavigator;
import lk.ncisl.obs.fe.ManageableComponent;
import lk.ncisl.obs.fe.user.UserContainer;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author vimukthi
 */
public class MenuLayout extends CssLayout implements ManageableComponent, Page.UriFragmentChangedListener {

    @Autowired
    private UserContainer container;
    private ExtendedNavigator nav;
    private static final String NAME_KEY = "name";
    private static final String URL_KEY = "url";
    private static final String DISPLAY_NAME_KEY = "display_name";
    private static final String ICON_KEY = "icon";
    private static final String MENU_BUTTON_KEY = "button";
    private static final Integer DEFAULT_VIEW_KEY = 1;
	private static final String ICON_IMAGE = "default.png";
    // keep menu items in order
    private Map<Integer, Map<String, Object>> menuItems = new TreeMap<Integer, Map<String, Object>>();

    @Override
    public void onShow(Map<String, Object> params) {
        container.onShow(params);
        setNav((ExtendedNavigator) params.get("navigator"));
        addStyleName("menu");
        setHeight("100%");
        initMenuItems();
        Page.getCurrent().addUriFragmentChangedListener(this);

        // add menu items
        for (Map.Entry<Integer, Map<String, Object>> entry : menuItems.entrySet()) {
            Button menuButton = new NativeButton((String) entry.getValue().get(DISPLAY_NAME_KEY));
            menuButton.setIcon( new ThemeResource("img/icons/" + (String) entry.getValue().get(ICON_IMAGE) ) );
            //menuButton.addStyleName((String) entry.getValue().get(ICON_KEY));
            final String url = (String) entry.getValue().get(URL_KEY);
            menuButton.addClickListener(new Button.ClickListener() {
                @Override
                public void buttonClick(Button.ClickEvent event) {
                    clearMenuSelection();
                    event.getButton().addStyleName("selected");
                    if (!nav.getState().equals(url)) {
                        nav.navigateTo(url);
                    }
                }
            });
            addComponent(menuButton);
            entry.getValue().put(MENU_BUTTON_KEY, menuButton);
        }

        // init default view
        String url = getUrlFromFragment(Page.getCurrent().getUriFragment());
        if (url == null || url.equals("") || url.equals("/")) {
            nav.navigateTo((String) menuItems.get(DEFAULT_VIEW_KEY).get(URL_KEY));
            Button menuButton = (Button) menuItems.get(DEFAULT_VIEW_KEY).get(MENU_BUTTON_KEY);
            menuButton.addStyleName("selected");
        } else {
            Button menuButton = getMenuItemForFragment(Page.getCurrent().getUriFragment());
            // the url can be anything so we have to check if the button return is null
            if (menuButton != null) {
                menuButton.addStyleName("selected");
                nav.navigateTo(url);
            } 
        }
    }

    @Override
    public void onHide() {
        removeAllComponents();
    }

    @Override
    public void uriFragmentChanged(Page.UriFragmentChangedEvent event) {
        String url = getUrlFromFragment(event.getUriFragment());
        for (Map.Entry<Integer, Map<String, Object>> entry : menuItems.entrySet()) {
            // get the url of the menu entry
            String itemUrl = (String) entry.getValue().get(URL_KEY);
            // if the menu entry url equals the new fragment set it as selected
            if (itemUrl.equals(url)) {
                clearMenuSelection();
                Button menuButton = getMenuItemForFragment(event.getUriFragment());
                // the url can be anything so we have to check if the button return is null
                if (menuButton != null) {
                    menuButton.addStyleName("selected");
                    nav.navigateTo(url);
                } 
            }
        }
    }

    private String getUrlFromFragment(String fragment) {
        String url = "";
        if (fragment != null && fragment.startsWith("!")) {
            url = fragment.substring(1);
        }
        return url;
    }

    private Button getMenuItemForFragment(String fragment) {
        String url = getUrlFromFragment(fragment);
        Button menuButton = null;
        for (Map.Entry<Integer, Map<String, Object>> entry : menuItems.entrySet()) {
            // get the url of the menu entry
            String itemUrl = (String) entry.getValue().get(URL_KEY);
            // if the menu entry url equals the new fragment set it as selected
            if (itemUrl.equals(url)) {
                menuButton = (Button) entry.getValue().get(MENU_BUTTON_KEY);
            }
        }
        return menuButton;
    }

    /**
     * Initiate menu items taking into account the user permissions as well
     */
    private void initMenuItems() {
        // get persistent object for the user in session
        User currentUser = container.getCurrentUser();
        // home
        Map<String, Object> home = new HashMap<String, Object>();
        home.put(NAME_KEY, "home");
        home.put(URL_KEY, "/home");
        home.put(DISPLAY_NAME_KEY, "HOME");
        home.put(ICON_KEY, "icon-dashboard");
        home.put(ICON_IMAGE, "home.png");        
        menuItems.put(1, home);

        // orders
        Map<String, Object> orders = new HashMap<String, Object>();
        orders.put(NAME_KEY, "orders");
        orders.put(URL_KEY, "/orders");
        orders.put(DISPLAY_NAME_KEY, "ORDERS");
        orders.put(ICON_KEY, "icon-sales");
        orders.put(ICON_IMAGE, "orders.png"); 
        menuItems.put(2, orders);

        // bids
        Map<String, Object> bids = new HashMap<String, Object>();
        bids.put(NAME_KEY, "bids");
        bids.put(URL_KEY, "/bids");
        if(currentUser.isAdmin()) {
            bids.put(DISPLAY_NAME_KEY, "BIDS");
        } else {
            bids.put(DISPLAY_NAME_KEY, "MY BIDS");
        }        
        bids.put(ICON_KEY, "icon-transactions");
        bids.put(ICON_IMAGE, "bids.png");
        menuItems.put(4, bids);

        // messages
        Map<String, Object> messages = new HashMap<String, Object>();
        messages.put(NAME_KEY, "messages");
        messages.put(URL_KEY, "/messages");
        messages.put(DISPLAY_NAME_KEY, "MESSAGES");
        messages.put(ICON_KEY, "icon-sales");
		messages.put(ICON_IMAGE, "messages.png");
        menuItems.put(5, messages);
        
        // live bidding
        Map<String, Object> livebidding = new HashMap<String, Object>();
        livebidding.put(NAME_KEY, "livebidding");
        livebidding.put(URL_KEY, "/livebidding");
        livebidding.put(DISPLAY_NAME_KEY, "LIVE BIDDING");
        livebidding.put(ICON_KEY, "icon-livebidding");
        livebidding.put(ICON_IMAGE, "livebidding.png");
        menuItems.put(6, livebidding);

        // profile
        Map<String, Object> profile = new HashMap<String, Object>();
        profile.put(NAME_KEY, "profile");
        profile.put(URL_KEY, "/profile");
        profile.put(DISPLAY_NAME_KEY, "MY PROFILE");
        profile.put(ICON_KEY, "icon-sales");
		profile.put(ICON_IMAGE, "profile.png");
        menuItems.put(7, profile);

        
        // check for admin role
        if (currentUser.isAdmin()) {
            // users
            Map<String, Object> users = new HashMap<String, Object>();
            users.put(NAME_KEY, "users");
            users.put(URL_KEY, "/users");
            users.put(DISPLAY_NAME_KEY, "USERS");
            users.put(ICON_KEY, "icon-sales");
            users.put(ICON_IMAGE, "users.png");
            menuItems.put(8, users);
        }
        
        
        
    }

    private void clearMenuSelection() {
        for (Iterator<Component> it = getComponentIterator(); it.hasNext();) {
            Component next = it.next();
            if (next instanceof NativeButton) {
                next.removeStyleName("selected");
            }
        }
    }

    public ExtendedNavigator getNav() {
        return nav;
    }

    public void setNav(ExtendedNavigator nav) {
        this.nav = nav;
    }

    public UserContainer getContainer() {
        return container;
    }

    public void setContainer(UserContainer container) {
        this.container = container;
    }
}
