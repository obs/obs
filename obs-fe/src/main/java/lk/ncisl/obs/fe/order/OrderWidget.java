package lk.ncisl.obs.fe.order;

import com.vaadin.server.FileResource;
import com.vaadin.shared.ui.label.ContentMode;
import com.vaadin.ui.Button;
import com.vaadin.ui.CssLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Image;
import com.vaadin.ui.Label;
import java.io.File;
import lk.ncisl.obs.data.domain.OrderRequest;
import lk.ncisl.obs.data.domain.OrderState;
import lk.ncisl.obs.data.domain.User;
import lk.ncisl.obs.fe.InvariantConfigs;
import lk.ncisl.obs.fe.utils.StaticUtils;

/**
 *
 * @author vimukthi
 */
public class OrderWidget extends CssLayout {

    private Label codeDisplay;
    private Label summaryDisplay;
    private Label quantityDisplay;
    private ExpiryTimeDisplay clockDisplay;
    private Button bid;
    private Button edit;
    private Button cancel;
    private Button phase2;
    private Button fullFilled;
    private Button unsatisfied;
    private OrderRequest order;
    private Image image;
    private User currentUser;
    private OrderContainer orders;

    public OrderWidget(OrderRequest order, String imagesPath, User currentUser, OrderContainer orders) {
        this.order = order;
        this.currentUser = currentUser;
        this.orders = orders;
        setStyleName("obs-order-frame");

        // image
        FileResource resource = new FileResource(new File(imagesPath + order.getImage()));
        image = new Image(null, resource);
        image.setWidth(InvariantConfigs.ORDERLIST_IMAGE_DISPLAY_WIDTH);
        image.setHeight(InvariantConfigs.ORDERLIST_IMAGE_DISPLAY_HEIGHT);
        image.setDescription(order.getSummary());

        // other fields
        codeDisplay = new Label(StaticUtils.lableCaptionWrap("Code : ") + order.getCode(), ContentMode.HTML);
        summaryDisplay = new Label(StaticUtils.lableCaptionWrap("Summary : ") + order.getSummary(), ContentMode.HTML);
        quantityDisplay = new Label(StaticUtils.lableCaptionWrap("Quantity : ") + order.getQuantity(), ContentMode.HTML);
        clockDisplay = new ExpiryTimeDisplay(order.getState().getDisplayName(), orders);

        addComponent(image);
        addComponent(codeDisplay);
        addComponent(summaryDisplay);
        addComponent(quantityDisplay);
        addComponent(clockDisplay);

        HorizontalLayout buttons = new HorizontalLayout();
        buttons.setSizeFull();
        buttons.setSpacing(true);
        addComponent(buttons);

        if ((order.isInState(OrderState.ValidStates.PHASE1) || order.isInState(OrderState.ValidStates.PHASE2))
                && (currentUser.isAdmin() || currentUser.isInRole("bidder"))) {
            bid = new Button("Bid");
            buttons.addComponent(bid);
            bid.setSizeFull();
            bid.setStyleName("default");
        }

        if (currentUser.isAdmin()) {
            if (order.isInState(OrderState.ValidStates.PHASE1_EXPIRED)){
                phase2 = new Button("Phase 2");
                buttons.addComponent(phase2);
                phase2.setSizeFull();
                phase2.setStyleName("default");
            }
            if (order.isInState(OrderState.ValidStates.PHASE2_EXPIRED)){
                fullFilled = new Button("Fullfilled");
                buttons.addComponent(fullFilled);
                fullFilled.setSizeFull();
                fullFilled.setStyleName("default");
                unsatisfied = new Button("Unsatisfied");
                buttons.addComponent(unsatisfied);
                unsatisfied.setSizeFull();
                unsatisfied.setStyleName("default");
            }
            if (!order.isInState(OrderState.ValidStates.CANCELLED) && 
                    !order.isInState(OrderState.ValidStates.PHASE2_EXPIRED)
                    && !order.isInState(OrderState.ValidStates.FULLFILLED)
                    && !order.isInState(OrderState.ValidStates.UNSATISFIED)) {
                edit = new Button("Edit");
                buttons.addComponent(edit);
                edit.setSizeFull();
                edit.setStyleName("default");
                cancel = new Button("Cancel");
                buttons.addComponent(cancel);
                cancel.setSizeFull();
                cancel.setStyleName("default");
            }
        }
    }

    public Label getCodeDisplay() {
        return codeDisplay;
    }

    public void setCodeDisplay(Label codeDisplay) {
        this.codeDisplay = codeDisplay;
    }

    public Image getImage() {
        return image;
    }

    public void setImage(Image image) {
        this.image = image;
    }

    public Label getSummaryDisplay() {
        return summaryDisplay;
    }

    public void setSummaryDisplay(Label summaryDisplay) {
        this.summaryDisplay = summaryDisplay;
    }

    public Label getQuantityDisplay() {
        return quantityDisplay;
    }

    public void setQuantityDisplay(Label quantityDisplay) {
        this.quantityDisplay = quantityDisplay;
    }

    public ExpiryTimeDisplay getClockDisplay() {
        return clockDisplay;
    }

    public void setClockDisplay(ExpiryTimeDisplay clockDisplay) {
        this.clockDisplay = clockDisplay;
    }

    public OrderRequest getOrder() {
        return order;
    }

    public void setOrder(OrderRequest order) {
        this.order = order;
    }

    public Button getBidButton() {
        return bid;
    }

    public Button getEditButton() {
        return edit;
    }

    public Button getCancelButton() {
        return cancel;
    }

    public Button getPhase2Button() {
        return phase2;
    }

    public Button getFullFilledButton() {
        return fullFilled;
    }

    public Button getUnsatisfiedButton() {
        return unsatisfied;
    }
    
    
}
