package lk.ncisl.obs.fe.bid;

import com.vaadin.shared.ui.label.ContentMode;
import com.vaadin.ui.Alignment;
import lk.ncisl.obs.data.domain.Bid;

import com.vaadin.ui.Button;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import lk.ncisl.obs.data.domain.BidState;
import lk.ncisl.obs.data.domain.OrderState;
import lk.ncisl.obs.data.domain.User;
import lk.ncisl.obs.fe.utils.StaticUtils;

public class BidWidget extends HorizontalLayout {

    private Bid bid;
    private Label orderCode;
    private Label organization;
    private Label pricePerUnit;
    private Label quantity;
    private Button amend;
    private Button details;
    private Button accept;
    private Button reject;
    private Button hold;
    private Button unhold;
    private Button cancel;
    private User currentUser;

    public BidWidget(Bid bid, User currentUser) {
        super();
        this.bid = bid;
        this.currentUser = currentUser;
        setWidth("100%");
        setSpacing(true);
        setStyleName("bid-component");
        
        HorizontalLayout detailsArea = new HorizontalLayout();
        addComponent(detailsArea);
        detailsArea.setSpacing(true);
        detailsArea.setSizeFull();
        

        orderCode = new Label(StaticUtils.lableCaptionWrap("Order Code : ") + bid.getOrderRequest().getCode(), ContentMode.HTML);
        detailsArea.addComponent(orderCode);
        detailsArea.setComponentAlignment(orderCode, Alignment.MIDDLE_LEFT);
        orderCode.setSizeFull();

        organization = new Label(StaticUtils.lableCaptionWrap("Bid by : ") + bid.getBidder().getOrganization(), ContentMode.HTML);
        detailsArea.addComponent(organization);
        detailsArea.setComponentAlignment(organization, Alignment.MIDDLE_LEFT);
        organization.setSizeFull();

        pricePerUnit = new Label(StaticUtils.lableCaptionWrap("Price per Unit : ") + Float.toString(bid.getPricePerUnit()), ContentMode.HTML);
        detailsArea.addComponent(pricePerUnit);
        detailsArea.setComponentAlignment(pricePerUnit, Alignment.MIDDLE_LEFT);
        pricePerUnit.setSizeFull();

        quantity = new Label(StaticUtils.lableCaptionWrap("Quantity : ") + Integer.toString(bid.getQuantity()), ContentMode.HTML);
        detailsArea.addComponent(quantity);
        detailsArea.setComponentAlignment(quantity, Alignment.MIDDLE_LEFT);
        quantity.setSizeFull();

        HorizontalLayout buttons = new HorizontalLayout();
        addComponent(buttons);
        buttons.setSpacing(true);
        setComponentAlignment(buttons, Alignment.MIDDLE_RIGHT);

        if ((currentUser.isAdmin() || bid.getBidder().equals(currentUser))
                && (bid.isInState(BidState.ValidStates.PHASE1_ACTIVE) || bid.isInState(BidState.ValidStates.PHASE1_ONHOLD)
                || bid.isInState(BidState.ValidStates.PHASE2_ACTIVE) || bid.isInState(BidState.ValidStates.PHASE2_ONHOLD)) &&
                    (! bid.getOrderRequest().isInState(OrderState.ValidStates.PHASE1_EXPIRED) || 
                    ! bid.getOrderRequest().isInState(OrderState.ValidStates.PHASE2_EXPIRED))) {
            amend = new Button("Amend");
            amend.setStyleName("default");
            buttons.addComponent(amend);
            cancel = new Button("Cancel");
            cancel.setStyleName("default");
            buttons.addComponent(cancel);
        }

        details = new Button("Details");
        details.setStyleName("default");
        buttons.addComponent(details);

        if (currentUser.isAdmin()) {
            if ((bid.isInState(BidState.ValidStates.PHASE1_ACTIVE)
                    || bid.isInState(BidState.ValidStates.PHASE2_ACTIVE)
                    || bid.isInState(BidState.ValidStates.PHASE1_REJECTED)
                    || bid.isInState(BidState.ValidStates.REJECTED)) &&
                    (bid.getOrderRequest().isInState(OrderState.ValidStates.PHASE1_EXPIRED) || 
                    bid.getOrderRequest().isInState(OrderState.ValidStates.PHASE2_EXPIRED))) {
                accept = new Button("Accept");
                accept.setStyleName("default");
                buttons.addComponent(accept);
            }
            if ((bid.isInState(BidState.ValidStates.PHASE1_ACTIVE)
                    || bid.isInState(BidState.ValidStates.PHASE2_ACTIVE)
                    || bid.isInState(BidState.ValidStates.PHASE1_ACCEPTED)
                    || bid.isInState(BidState.ValidStates.ACCEPTED)) &&
                    (bid.getOrderRequest().isInState(OrderState.ValidStates.PHASE1_EXPIRED) || 
                    bid.getOrderRequest().isInState(OrderState.ValidStates.PHASE2_EXPIRED))) {
                reject = new Button("Reject");
                reject.setStyleName("default");
                buttons.addComponent(reject);
            }
            if ((bid.isInState(BidState.ValidStates.PHASE1_ACTIVE)
                    || bid.isInState(BidState.ValidStates.PHASE2_ACTIVE)) &&
                    (bid.getOrderRequest().isInState(OrderState.ValidStates.PHASE1) || 
                    bid.getOrderRequest().isInState(OrderState.ValidStates.PHASE2))) {
                hold = new Button("Hold");
                hold.setStyleName("default");
                buttons.addComponent(hold);
            }
            if ((bid.isInState(BidState.ValidStates.PHASE1_ONHOLD)
                    || bid.isInState(BidState.ValidStates.PHASE2_ONHOLD)) &&
                    (bid.getOrderRequest().isInState(OrderState.ValidStates.PHASE1) || 
                    bid.getOrderRequest().isInState(OrderState.ValidStates.PHASE2))) {
                unhold = new Button("Unhold");
                unhold.setStyleName("default");
                buttons.addComponent(unhold);
            }

        }
        
        buttons.setWidth("350px");
        setExpandRatio(detailsArea, 1.0f);
    }

    public void hideOrderCode() {
        orderCode.setVisible(false);
    }

    public Bid getBid() {
        return bid;
    }

    public Label getOrganization() {
        return organization;
    }

    public Label getPricePerUnit() {
        return pricePerUnit;
    }

    public Label getQuantity() {
        return quantity;
    }

    public Button getAmendButton() {
        return amend;
    }

    public Button getCancelButton() {
        return cancel;
    }

    public Button getDetailsButton() {
        return details;
    }

    public Button getAcceptButton() {
        return accept;
    }

    public Button getRejectButton() {
        return reject;
    }

    public Button getHoldButton() {
        return hold;
    }

    public Button getUnholdButton() {
        return unhold;
    }    
}
