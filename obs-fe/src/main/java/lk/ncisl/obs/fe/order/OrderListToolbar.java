package lk.ncisl.obs.fe.order;

import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.TextField;
import java.util.Map;
import lk.ncisl.obs.fe.ManageableComponent;
import lk.ncisl.obs.fe.user.UserContainer;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author vimukthi
 */
public class OrderListToolbar extends HorizontalLayout implements ManageableComponent {

    private Button addOrder;
    private TextField orderfilter;
    private ComboBox sorter;
    private ComboBox stateFilter;
    @Autowired
    private UserContainer users;
    
    @Autowired
    private OrderStateContainer orderStates;

    @Override
    public void onShow(Map<String, Object> params) {
        users.onShow(params);
        orderStates.onShow(params);
        setWidth("100%");
        setSpacing(true);
        setMargin(true);
        addStyleName("toolbar");

        Label title = new Label("Orders");
        title.addStyleName("h1");
        title.setSizeUndefined();
        addComponent(title);
        setComponentAlignment(title, Alignment.MIDDLE_LEFT);

        // order filter input
        orderfilter = new TextField();
        orderfilter.setStyleName("obs-order-filterfield");
        orderfilter.setInputPrompt("Filter");
        orderfilter.setDescription("Enter 3 or more charactors..");
        addComponent(orderfilter);
        setComponentAlignment(orderfilter, Alignment.MIDDLE_LEFT);
        
        // sorting input
        sorter = new ComboBox();
        sorter.addItem(OrderListView.SORT_BY_CODE_ASC);
        sorter.addItem(OrderListView.SORT_BY_CODE_DESC);
        sorter.addItem(OrderListView.SORT_BY_EXPIRY_DATE_ASC);
        sorter.addItem(OrderListView.SORT_BY_EXPIRY_DATE_DESC);
        sorter.setValue(OrderListView.SORT_BY_EXPIRY_DATE_ASC);
        sorter.setImmediate(true);
        sorter.setInvalidAllowed(false);
        sorter.setTextInputAllowed(false);
        sorter.setWidth("200px");
        sorter.setNullSelectionAllowed(false);
        addComponent(sorter);
        setComponentAlignment(sorter, Alignment.MIDDLE_LEFT);
        
        // state filtering for orders
        stateFilter = new ComboBox();
        stateFilter.setContainerDataSource(orderStates);
        stateFilter.setValue(orderStates.getOrderStateByName("phase1").getId());
        stateFilter.setItemCaptionPropertyId("displayName");
        stateFilter.setImmediate(true);
        stateFilter.setInvalidAllowed(false);
        stateFilter.setTextInputAllowed(false);
        stateFilter.setWidth("200px");
        stateFilter.setNullSelectionAllowed(false);
        addComponent(stateFilter);
        setComponentAlignment(stateFilter, Alignment.MIDDLE_LEFT);
        
        
        if (users.getCurrentUser().isAdmin()) {
            addOrder = new Button("Add Order");
            addComponent(addOrder);
            setComponentAlignment(addOrder, Alignment.MIDDLE_LEFT);
        }
        setExpandRatio(orderfilter, 1);
    }

    @Override
    public void onHide() {
        users.onHide();
        orderStates.onHide();
        removeAllComponents();
    }

    public Button getAddOrderButton() {
        return addOrder;
    }

    public void setAddOrderButton(Button addOrder) {
        this.addOrder = addOrder;
    }

    public TextField getFilter() {
        return orderfilter;
    }

    public void setFilter(TextField filter) {
        this.orderfilter = filter;
    }
    
    public ComboBox getSorter(){
        return sorter;
    }
    
    public void setSorter(ComboBox sorter){
        this.sorter=sorter;
    }

    public ComboBox getStateFilter() {
        return stateFilter;
    }

    public void setStateFilter(ComboBox stateFilter) {
        this.stateFilter = stateFilter;
    }    
}
