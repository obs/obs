

package lk.ncisl.obs.fe;

import com.vaadin.shared.ui.label.ContentMode;
import com.vaadin.ui.Button;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;

/**
 *
 * @author vimukthi
 */
public class ConfirmWindow extends Window {
    
    public interface Decision {
        
        public void confirmed();
        
        public void refused();
    }
    
    public ConfirmWindow(String title, String message, final Decision callback) {
        super(title);
        setWidth("600px");
        setHeight("400px");
        VerticalLayout layout = new VerticalLayout();
        setContent(layout);
        layout.setSizeFull();
        layout.addComponent(new Label(message, ContentMode.HTML));
        HorizontalLayout buttons = new HorizontalLayout();
        Button ok = new Button("OK");
        Button cancel = new Button("Cancel");
        buttons.addComponent(ok);
        buttons.addComponent(cancel);
        layout.addComponent(buttons);
        ok.focus();
        final Window self = this;
        center();
        ok.addClickListener(new Button.ClickListener() {

            @Override
            public void buttonClick(Button.ClickEvent event) {
                self.close();
                callback.confirmed();
            }
        });
        cancel.addClickListener(new Button.ClickListener() {

            @Override
            public void buttonClick(Button.ClickEvent event) {
                self.close();
                callback.refused();
            }
        });
    }
}
