/**
 * DISCLAIMER
 * 
 * The quality of the code is such that you should not copy any of it as best
 * practice how to build Vaadin applications.
 * 
 * @author jouni@vaadin.com
 * 
 */

package lk.ncisl.obs.fe.home;

import com.vaadin.event.LayoutEvents.LayoutClickEvent;
import com.vaadin.event.LayoutEvents.LayoutClickListener;
import com.vaadin.event.ShortcutAction.KeyCode;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.server.ThemeResource;
import com.vaadin.shared.ui.MarginInfo;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.Component;
import com.vaadin.ui.CssLayout;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Image;
import com.vaadin.ui.Label;
import com.vaadin.ui.Table;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;
import java.util.HashMap;
import java.util.Map;
import lk.ncisl.obs.fe.ManageableComponent;
import org.springframework.beans.factory.annotation.Autowired;

public class LiveBidsView extends VerticalLayout implements View, ManageableComponent {

    Table t;
    
    @Autowired
    LiveBidsWidget liveBids;
    
    Integer bidNo=0;

    public LiveBidsView() {

    }

    private CssLayout createPanel(Component content) {
        CssLayout panel = new CssLayout();
        panel.addStyleName("layout-panel");
        panel.setSizeFull();

        Button configure = new Button();
        configure.addStyleName("configure");
        configure.addStyleName("icon-cog");
        configure.addStyleName("icon-only");
        configure.addStyleName("borderless");
        configure.setDescription("Configure");
        configure.addStyleName("small");
        configure.addClickListener(new ClickListener() {
            @Override
            public void buttonClick(ClickEvent event) {
                //liveBids.addNewBidToUI();
            }
        });
        panel.addComponent(configure);

        panel.addComponent(content);
        return panel;
    }

    @Override
    public void enter(ViewChangeEvent event) {
//        DataProvider dataProvider = ((DashboardUI) getUI()).dataProvider;
//        t.setContainerDataSource(dataProvider.getRevenueByTitle());
        
        
        setSizeFull();
        addStyleName("dashboard-view");
                
        
        HorizontalLayout top = new HorizontalLayout();
        top.setWidth("100%");
        top.setSpacing(true);
        top.addStyleName("toolbar");
        addComponent(top);
        final Label title = new Label("Live Bidding");
        title.setSizeUndefined();
        title.addStyleName("h1");
        top.addComponent(title);
        top.setComponentAlignment(title, Alignment.MIDDLE_LEFT);
        top.setExpandRatio(title, 1);
        
        

        HorizontalLayout row = new HorizontalLayout();
        row.setSizeFull();
        row.setMargin(new MarginInfo(true, true, false, true));
        row.setSpacing(true);
        addComponent(row);
        setExpandRatio(row, 1.5f);

        //row.addComponent(createPanel(new TopGrossingMoviesChart()));

        
        liveBids.setCaption("Live bidding");        
        liveBids.setWidth("100%"); 
        CssLayout panel = createPanel(liveBids);
        panel.addStyleName("notes");
        row.addComponent(panel);
        
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("allBidList", "allBidList");
        liveBids.onShow(params);
        
                
    }

    Window notifications;

    private void buildNotifications(ClickEvent event) {
        notifications = new Window("Notifications");
        VerticalLayout l = new VerticalLayout();
        l.setMargin(true);
        l.setSpacing(true);
        notifications.setContent(l);
        notifications.setWidth("300px");
        notifications.addStyleName("notifications");
        notifications.setClosable(false);
        notifications.setResizable(false);
        notifications.setDraggable(false);
        notifications.setPositionX(event.getClientX() - event.getRelativeX());
        notifications.setPositionY(event.getClientY() - event.getRelativeY());
        notifications.setCloseShortcut(KeyCode.ESCAPE, null);

//        Label label = new Label(
//                "<hr><b>"
//                        + Generator.randomFirstName()
//                        + " "
//                        + Generator.randomLastName()
//                        + " created a new report</b><br><span>25 minutes ago</span><br>"
//                        + Generator.randomText(18), ContentMode.HTML);
//        l.addComponent(label);
//
//        label = new Label("<hr><b>" + Generator.randomFirstName() + " "
//                + Generator.randomLastName()
//                + " changed the schedule</b><br><span>2 days ago</span><br>"
//                + Generator.randomText(10), ContentMode.HTML);
//        l.addComponent(label);
    }

    @Override
    public void onShow(Map<String, Object> params) {
        
    }

    @Override
    public void onHide() {
        removeAllComponents();
    }

    public LiveBidsWidget getLiveBids() {
        return liveBids;
    }

    public void setLiveBids(LiveBidsWidget liveBids) {
        this.liveBids = liveBids;
    }
    
    
        
    

}
