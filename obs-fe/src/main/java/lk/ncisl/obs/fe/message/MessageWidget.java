package lk.ncisl.obs.fe.message;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;

import lk.ncisl.obs.data.domain.Message;
import lk.ncisl.obs.data.domain.User;
import com.vaadin.server.Page;
import com.vaadin.shared.ui.label.ContentMode;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Notification;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.themes.BaseTheme;

public class MessageWidget extends HorizontalLayout {

    private Message message;

    private Button title;
    private Label body;

    private Label from;
    private Label to;
    private Label dateCreated;
    private Button reply;
    private Button delete;

    private User currentUser;

    private boolean sentMessage = false;

    @Autowired
    private MessageViewReplyPane messageViewReplyForm;

    public MessageWidget() {
        super();
    }

    public MessageWidget(Message message, User currentUser) {
        super();
        initMessageWidget(message, currentUser);
    }

    public void initMessageWidget(Message message, User currentUser) {
        this.message = message;
        this.currentUser = currentUser;
        setWidth("100%");
        setStyleName("message-component");

        HorizontalLayout detailsArea = new HorizontalLayout();
        addComponent(detailsArea);
        setExpandRatio(detailsArea, 1.0f);
        detailsArea.setSpacing(true);
        detailsArea.setSizeFull();

        title = new Button(message.getTitle());
        title.setStyleName(BaseTheme.BUTTON_LINK + " message-component-title");
        detailsArea.addComponent(title);
        detailsArea.setComponentAlignment(title, Alignment.MIDDLE_LEFT);
        detailsArea.setExpandRatio(title, 1.0f);

        VerticalLayout fromAndTime = new VerticalLayout();
        fromAndTime.setWidth("200px");
        detailsArea.addComponent(fromAndTime);
        detailsArea.setComponentAlignment(fromAndTime, Alignment.TOP_LEFT);

        if (sentMessage) {
            to = new Label("To : " + message.getReceiver().getFirstName(), ContentMode.HTML);
            to.setStyleName("message-component-from");
            fromAndTime.addComponent(to);
        } else {
            from = new Label("From : " + message.getSender().getFirstName(), ContentMode.HTML);
            from.setStyleName("message-component-from");
            fromAndTime.addComponent(from);
        }

        dateCreated = new Label(message.getDateCreated().toGMTString(), ContentMode.HTML);
        dateCreated.setStyleName("message-component-date");
        fromAndTime.addComponent(dateCreated);

        HorizontalLayout buttons = new HorizontalLayout();
        addComponent(buttons);
        buttons.setSpacing(true);
        buttons.setWidth("150px");
        setComponentAlignment(buttons, Alignment.MIDDLE_RIGHT);

        delete = new Button("Delete");
        delete.setStyleName("default");
        buttons.addComponent(delete);
        buttons.setComponentAlignment(delete, Alignment.MIDDLE_RIGHT);

        //delete section
        delete.addClickListener(new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent event) {
                Notification notif = new Notification("Info", "Not implemented yet !", Notification.Type.ERROR_MESSAGE);
                notif.setDelayMsec(3000);
                notif.show(Page.getCurrent());

            }
        });

        //reply section
        title.addClickListener(new ClickListener() {

            @Override
            public void buttonClick(ClickEvent event) {
                // Create a sub-window and set the content
                Window subWindow = new Window("Message: " + getMessage().getTitle());
                subWindow.setModal(true);
                subWindow.setWidth("80%");
                subWindow.setHeight("75%");
                subWindow.setContent(messageViewReplyForm);
                Map<String, Object> params = new HashMap<String, Object>();
                params.put("reply", "reply");
                params.put("parent_message", getMessage());

                params.put("window", subWindow);
                messageViewReplyForm.onShow(params);
                // Center it in the browser window
                subWindow.center();
                subWindow.addCloseListener(new Window.CloseListener() {
                    @Override
                    public void windowClose(Window.CloseEvent e) {
                        messageViewReplyForm.onHide();
                    }
                });

                UI.getCurrent().addWindow(subWindow);
            }
        });

    }

    public Message getMessage() {
        return message;
    }

    public void setMessage(Message message) {
        this.message = message;
    }

    public Button getTitle() {
        return title;
    }

    public void setTitle(Button title) {
        this.title = title;
    }

    public Label getBody() {
        return body;
    }

    public void setBody(Label body) {
        this.body = body;
    }

    public User getCurrentUser() {
        return currentUser;
    }

    public void setCurrentUser(User currentUser) {
        this.currentUser = currentUser;
    }

    public boolean isSentMessage() {
        return sentMessage;
    }

    public void setSentMessage(boolean sentMessage) {
        this.sentMessage = sentMessage;
    }

}
