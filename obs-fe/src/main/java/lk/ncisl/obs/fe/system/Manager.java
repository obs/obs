

package lk.ncisl.obs.fe.system;

import com.vaadin.server.WrappedSession;
import com.vaadin.ui.UI;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import lk.ncisl.obs.data.services.OrderRequestService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * This class is responsible for all the background tasks running in the system.
 * @author vimukthi
 */
public class Manager {
    
    private ExecutorService executor = Executors.newCachedThreadPool();
    
    private final static Logger logger = LoggerFactory.getLogger(Manager.class);
    
    @Autowired
    private OrderRequestService orderService;

    @Autowired
    private Messenger messenger;
    
    private Long updateIntervalMillis = 30000L; // 30 secs
    
    private ManagerTask expirator;
    private List<ManagerTask> tasks;
    private Map<UI, List<ManagerTask>> uiTasks;

    public void init() {
        uiTasks = new HashMap<UI, List<ManagerTask>>();
        tasks = new ArrayList<ManagerTask>();
        expirator = new OrderExpirator(orderService, updateIntervalMillis);
        expirator.setManager(this);
        executor.execute(expirator);
        tasks.add(expirator);
        messenger.setManager(this);
        executor.execute(messenger);
        tasks.add(messenger);
    } 
    
    public void close(){
        for (ManagerTask managerTask : tasks) {
            managerTask.stop();
        }
        executor.shutdown();
    }  
    
    public void submitTask(ManagerTask task){
        task.setManager(this);
        executor.execute(task);
        tasks.add(task);
    }
    
    public void stopTask(ManagerTask task){
        task.stop();
        tasks.remove(task);
    }
    
    public void submitUiTask(UI ui, ManagerTask task){
        task.setManager(this);
        executor.execute(task);
        if(uiTasks.containsKey(ui)){
            uiTasks.get(ui).add(task);
        } else {
            List<ManagerTask> taskList = new ArrayList<ManagerTask>();
            taskList.add(task);
            uiTasks.put(ui, taskList);
        }
    }
    
    public void stopUiTask(UI ui, ManagerTask task){
        task.stop();
        uiTasks.get(ui).remove(task);
    }
    
    private void stopAllTasksForUi(UI ui){
        for (ManagerTask task : uiTasks.get(ui)) {
            logger.debug("stopping task " + task);
            task.stop();
        }              
    }
    
    public void stopAllTasksForSession(WrappedSession session){
        Iterator<Map.Entry<UI, List<ManagerTask>>> iter = uiTasks.entrySet().iterator();
        while (iter.hasNext()) {   
            Map.Entry<UI, List<ManagerTask>> entry = iter.next();
            if(entry.getKey().getSession().getSession().equals(session)){
                logger.debug("stopping tasks for " + entry.getKey());
                stopAllTasksForUi(entry.getKey());
                iter.remove();
            }
        }      
    }
    
    public void alertExpirator(){
        expirator.alert();
    }
}
