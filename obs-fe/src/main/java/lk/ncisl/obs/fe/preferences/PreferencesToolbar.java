package lk.ncisl.obs.fe.preferences;

import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import lk.ncisl.obs.data.domain.User;
import lk.ncisl.obs.fe.ManageableComponent;

import java.util.Map;

/**
 *
 * @author vimukthi
 */
public class PreferencesToolbar extends HorizontalLayout implements ManageableComponent {
    
    private Button resetPasswd;

    @Override
    public void onShow(Map<String, Object> params) {
        setWidth("100%");
        setSpacing(true);
        setMargin(true);
        addStyleName("toolbar");

        // title
        Label title = new Label("My Profile");
        title.addStyleName("h1");
        title.setSizeUndefined();
        addComponent(title);
        setComponentAlignment(title, Alignment.MIDDLE_LEFT);

        // buttons
        resetPasswd = new Button("Reset Password");
        resetPasswd.addStyleName("small");
        addComponent(resetPasswd);
        setComponentAlignment(resetPasswd, Alignment.MIDDLE_RIGHT);
    }

    @Override
    public void onHide() {
        removeAllComponents();
    }

    public Button getResetPasswdButton() {
        return resetPasswd;
    }
}
