package lk.ncisl.obs.fe.layout;

import com.vaadin.server.ThemeResource;
import com.vaadin.server.VaadinSession;
import com.vaadin.shared.communication.PushMode;
import com.vaadin.ui.Button;
import com.vaadin.ui.Image;
import com.vaadin.ui.Label;
import com.vaadin.ui.NativeButton;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import java.util.Map;
import lk.ncisl.obs.data.domain.User;
import lk.ncisl.obs.fe.ManageableComponent;
import lk.ncisl.obs.fe.system.Manager;
import lk.ncisl.obs.fe.user.UserContainer;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author vimukthi
 */
public class UserMenuLayout extends VerticalLayout implements ManageableComponent {

    @Autowired
    private UserContainer container;
    
    @Autowired
    private Manager systemManager;

    @Override
    public void onShow(Map<String, Object> params) {
        container.onShow(params);
        setSizeUndefined();
        addStyleName("user");
        Image profilePic = new Image(
                null,
                new ThemeResource("img/profile-pic.png"));
        profilePic.setWidth("34px");
        addComponent(profilePic);
        User current = container.getCurrentUser();
        Label userName = new Label(current.getFirstName() + " " + current.getLastName());
        userName.setSizeUndefined();
        addComponent(userName);

        final Button exit = new Button("Sign Out");
        exit.addStyleName("icon-cancel signout-button");
        exit.setDescription("Sign Out");
        addComponent(exit);
        exit.addClickListener(new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent event) {
                systemManager.stopAllTasksForSession(getUI().getSession().getSession());
                // Close the current session
                // refer http://dev.vaadin.com/ticket/11721
                getUI().getPushConfiguration().setPushMode(PushMode.DISABLED);
                
                final UI ui = getUI();

                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        ui.access(new Runnable() {
                            @Override
                            public void run() {
                                VaadinSession.getCurrent().getSession().invalidate();
                                ui.getPage().reload();
                            }
                        });
                    }
                }).start();
            }
        });
    }

    @Override
    public void onHide() {
        removeAllComponents();
    }

    public UserContainer getContainer() {
        return container;
    }

    public void setContainer(UserContainer container) {
        this.container = container;
    }
}
