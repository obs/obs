package lk.ncisl.obs.fe.message;

import java.util.Date;
import java.util.Map;

import lk.ncisl.obs.data.domain.Message;
import lk.ncisl.obs.data.domain.User;
import lk.ncisl.obs.data.services.MessagingService;
import lk.ncisl.obs.data.services.exceptions.NonActiveOrderException;
import lk.ncisl.obs.fe.ManageableComponent;
import lk.ncisl.obs.fe.order.OrderContainer;
import lk.ncisl.obs.fe.order.OrderStateContainer;
import lk.ncisl.obs.fe.user.UserContainer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.vaadin.data.fieldgroup.BeanFieldGroup;
import com.vaadin.data.fieldgroup.FieldGroup;
import com.vaadin.data.fieldgroup.FieldGroup.CommitException;
import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.data.validator.StringLengthValidator;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.RichTextArea;
import com.vaadin.ui.TextField;
import com.vaadin.ui.Window;

/**
 *
 * @author prabhath
 */
public class MessageForm extends FormLayout implements ManageableComponent {

    private final static Logger logger = LoggerFactory.getLogger(MessageForm.class);
    private Message message;
    private Window parent;
    private String uploadLocation;
    private BeanFieldGroup<Message> binder;
    @Autowired
    private MessagingService messagingService;
    @Autowired
    private OrderContainer orders;
    @Autowired
    private OrderStateContainer orderStates;
    @Autowired
    private UserContainer users;
    // fields
    private ComboBox receiver;
    private TextField title;
    private RichTextArea body;
    private Message parent_message;
    private Label errorMessage = new Label();

    @Override
    public void onShow(Map<String, Object> params) {
        parent = (Window) params.get("window");

        setMargin(true);

        users.onShow(params);
        // Form for editing the bean
        binder = new BeanFieldGroup<Message>(Message.class);


        // this is a new message addition
        if (params.containsKey("new")) {
            message = new Message();

            binder.setItemDataSource(message);
            bindFields(binder, true);

            // Buffer the form content
            binder.setBuffered(true);
            addComponent(new Button("Send", new ClickListener() {
                @Override
                public void buttonClick(ClickEvent event) {
                    try {
                        binder.commit();
                        message.setDateCreated(new Date());
                        messagingService.add(message);
                        if (parent != null) {
                            parent.close();
                        }
                    } catch (CommitException e) {
                        logger.error("validation", e);
                        // add validation errors on each field here
                        errorMessage.setValue("check inputs");
                        errorMessage.setVisible(true);
                    }

                }
            }));
        }

        // this is a reply message addition
        if (params.containsKey("reply")) {
            message = new Message();

            binder.setItemDataSource(message);
            parent_message = (Message) params.get("parent_message");

            bindFields(binder, false);

            // Buffer the form content
            binder.setBuffered(true);
            addComponent(new Button("Send", new ClickListener() {
                @Override
                public void buttonClick(ClickEvent event) {
                    try {
                        binder.commit();
                        message.setDateCreated(new Date());
                        messagingService.add(message);
                        if (parent != null) {
                            parent.close();
                        }
                    } catch (CommitException e) {
                        logger.error("validation", e);
                        // add validation errors on each field here
                        errorMessage.setValue("check inputs");
                        errorMessage.setVisible(true);
                    }

                }
            }));
        }
    }

    /**
     *
     * @param binder
     * @param newmsg
     * @throws com.vaadin.data.fieldgroup.FieldGroup.BindException
     */
    private void bindFields(BeanFieldGroup<Message> binder, boolean newmsg) throws FieldGroup.BindException {
        errorMessage.setVisible(false);
        addComponent(errorMessage);

        if (newmsg) {

            //if (users.getCurrentUser().isAdmin()) {

            receiver = new ComboBox("To: ");
            BeanItemContainer<User> audience = new BeanItemContainer<User>(User.class);
            audience.addAll(users.getContactableUsers( users.getCurrentUser() ));
            receiver.setContainerDataSource(audience);
            receiver.setItemCaptionPropertyId("email");
            //receiver.setImmediate(true);
            receiver.setInvalidAllowed(false);
            receiver.setTextInputAllowed(false);
            receiver.setWidth("200px");
            receiver.setNullSelectionAllowed(false);
            addComponent(receiver);
            binder.bind(receiver, "receiver");

            message.setSender(users.getCurrentUser());
            //}

            // title
            title = new TextField("Title: ");
            addComponent(title);
            binder.bind(title, "title");

            // msg body
            body = new RichTextArea("Message: ");
            body.addValidator(new StringLengthValidator("Can not be empty", 1, 5000, false));
            addComponent(body);
            binder.bind(body, "body");

        } else { //reply msg

            message.setReceiver(parent_message.getSender());
            message.setSender(users.getCurrentUser());
            message.setParent(parent_message.getId());
            message.setTitle(parent_message.getTitle());

            // msg body
            body = new RichTextArea("Message: ");
            body.addValidator(new StringLengthValidator("Can not be empty", 1, 5000, false));
            addComponent(body);
            binder.bind(body, "body");
        }

    }

    @Override
    public void onHide() {
        binder.discard();
        removeAllComponents();
    }

    public String getUploadLocation() {
        return uploadLocation;
    }

    public void setUploadLocation(String uploadLocation) {
        this.uploadLocation = uploadLocation;
    }
}
