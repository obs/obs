package lk.ncisl.obs.fe.order;

import com.vaadin.server.FileResource;
import com.vaadin.shared.ui.label.ContentMode;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Image;
import com.vaadin.ui.Label;
import com.vaadin.ui.VerticalLayout;
import java.io.File;
import java.util.Map;

import lk.ncisl.obs.data.domain.OrderRequest;
import lk.ncisl.obs.fe.InvariantConfigs;

import lk.ncisl.obs.fe.ManageableComponent;
import lk.ncisl.obs.fe.bid.BidTable;
import lk.ncisl.obs.fe.utils.StaticUtils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author vimukthi
 */
public class OrderDetailWidget extends VerticalLayout implements ManageableComponent {

    private final static Logger logger = LoggerFactory.getLogger(OrderDetailWidget.class);
    
    @Autowired
    private BidTable bidView;    


    @Override
    public void onShow(Map<String, Object> params) {
        setSizeFull();
        setMargin(true);

        // get the order and image path from params
        OrderRequest order = (OrderRequest) params.get("order");
        String imagesPath = (String) params.get("imagesPath");

        HorizontalLayout orderDetailsWrap = new HorizontalLayout();
        
        VerticalLayout orderDetails = new VerticalLayout();
        orderDetails.setSizeFull();
        orderDetails.setHeight("150px");
        orderDetails.setStyleName("obs-order-subwindow-orderdetails");
        
        // image
        FileResource resource = new FileResource(new File(imagesPath + order.getImage()));        
        Image image = new Image(null, resource);
        image.setWidth(InvariantConfigs.ORDERLIST_IMAGE_DISPLAY_WIDTH);
        image.setHeight(InvariantConfigs.ORDERLIST_IMAGE_DISPLAY_HEIGHT);
        image.setDescription(order.getSummary());
        
        // others
        orderDetails.addComponent(new Label( StaticUtils.lableCaptionWrap("Description: ") + order.getDescription(), ContentMode.HTML));
        orderDetails.addComponent(new Label( StaticUtils.lableCaptionWrap("Quantity: ") + order.getQuantity(), ContentMode.HTML));
        
        Label placeHolder = new Label();
        orderDetails.addComponent(placeHolder);
        orderDetails.setExpandRatio(placeHolder, 1.0f);
        
        orderDetailsWrap.addComponent(image);
        orderDetailsWrap.addComponent(orderDetails);
        orderDetailsWrap.setExpandRatio(orderDetails, 1.0f);
        addComponent(orderDetailsWrap);
        
        // current bids
        Label bidsHeading = new Label("Current Bids");
        bidsHeading.setStyleName("window-subheading");
        addComponent(bidsHeading);
        
        params.put("order", order);
        addComponent(bidView);        
        bidView.onShow(params);
        
        //setExpandRatio(orderDetails, 1.0f);
        setExpandRatio(bidView, 1.0f);
        
    }

    @Override
    public void onHide() {
        removeAllComponents();
    }
}
