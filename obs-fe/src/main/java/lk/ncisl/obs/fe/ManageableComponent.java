package lk.ncisl.obs.fe;

import java.util.Map;

/**
 *
 * @author vimukthi
 * Interface to manage life cycle of OBS UI components
 */
public interface ManageableComponent {
    
    public void onShow(Map<String, Object> params);
    
    public void onHide();
    
}
