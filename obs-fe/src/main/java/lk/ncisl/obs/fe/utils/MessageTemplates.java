package lk.ncisl.obs.fe.utils;

import lk.ncisl.obs.data.domain.Bid;
import lk.ncisl.obs.data.domain.OrderRequest;

/**
 * @author vimukthi
 */
public class MessageTemplates {

    public static String bidStateChangeMessageForBidder(Bid bid){
        return "Your bid " + bid.getQuantity() + "@" + bid.getPricePerUnit()
                + " for order " + bid.getOrderRequest().getCode()
                + " changed state to " + bid.getState().getDisplayName();
    }

    public static String orderStateChangeMessage(OrderRequest order) {
        return "Order with code " + order.getCode() + " changed state to "
                + order.getState().getDisplayName();
    }
}
