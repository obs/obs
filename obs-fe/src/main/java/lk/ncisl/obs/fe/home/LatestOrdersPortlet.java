/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package lk.ncisl.obs.fe.home;

import com.vaadin.ui.Button;
import com.vaadin.ui.Component;
import com.vaadin.ui.Label;
import com.vaadin.ui.Table;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import lk.ncisl.obs.data.domain.OrderRequest;
import lk.ncisl.obs.fe.InvariantConfigs;
import lk.ncisl.obs.fe.ManageableComponent;
import lk.ncisl.obs.fe.order.OrderContainer;
import lk.ncisl.obs.fe.order.OrderDetailWidget;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author prabhathd
 */
public class LatestOrdersPortlet extends Table implements ManageableComponent {
    
    @Autowired
    private OrderContainer orders;
    
    @Autowired
    private OrderDetailWidget orderDetails;
    
    private String uploadLocation;
    
    @Override
    public void onShow(Map<String, Object> params) {
        orders.onShow(params);
        setSizeFull();
        
        List<OrderRequest> latestOrders = orders.getLatestOrders(7);
       
        addContainerProperty("Code", String.class,  null);
        addContainerProperty("Summary",  String.class,  null);
        addContainerProperty("Qty",       Integer.class, null);
        addContainerProperty("View",       Component.class, null);

        Integer i = 0;
        
        for (OrderRequest order : latestOrders) {            
            i++;
            
            Button viewButton = new Button("View");
            
            addItem(new Object[] {
            order.getCode(),order.getSummary(), order.getQuantity(), viewButton}, new Integer(i));
            
            final OrderRequest orderFinal = order;
            viewButton.addClickListener(new Button.ClickListener() {
                @Override
                public void buttonClick(Button.ClickEvent event) {
                    // Create a sub-window and set the content
                    Window subWindow = new Window("Order Request : " + orderFinal.getCode());
                    subWindow.setModal(true);
                    subWindow.setWidth("80%");
                    subWindow.setHeight("80%");
                    subWindow.setContent(orderDetails);
                    Map<String, Object> params = new HashMap<String, Object>();
                    params.put("order", orderFinal);
                    params.put("imagesPath", uploadLocation + InvariantConfigs.ORDERS_IMAGES_PATH);
                    orderDetails.onShow(params);
                    // Center it in the browser window
                    subWindow.center();
                    UI.getCurrent().addWindow(subWindow);
                    subWindow.addCloseListener(new Window.CloseListener() {
                        @Override
                        public void windowClose(Window.CloseEvent e) {
                            orderDetails.onHide();
                        }
                    });
                }
            });
            
        }
        
        
        setSizeFull();
        setPageLength(0);
        setColumnAlignment("Qty", Table.Align.RIGHT);
        setColumnAlignment("View", Table.Align.RIGHT);
        setSelectable(true);
        
    }
    
    @Override
    public void onHide() {
        //removeAllComponents();
    }

    public String getUploadLocation() {
        return uploadLocation;
    }

    public void setUploadLocation(String uploadLocation) {
        this.uploadLocation = uploadLocation;
    }
    
    
    
}
