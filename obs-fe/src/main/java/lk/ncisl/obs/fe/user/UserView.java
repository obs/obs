package lk.ncisl.obs.fe.user;

import com.vaadin.addon.jpacontainer.EntityItem;
import com.vaadin.event.ItemClickEvent;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.ui.Button;
import com.vaadin.ui.Label;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;
import java.util.HashMap;
import java.util.Map;
import javax.persistence.EntityTransaction;
import lk.ncisl.obs.data.domain.User;
import lk.ncisl.obs.fe.ManageableComponent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author vimukthi
 */
public class UserView extends VerticalLayout implements View, ManageableComponent, ItemClickEvent.ItemClickListener {

    private final static Logger logger = LoggerFactory.getLogger(UserView.class);
    @Autowired
    private UserTable table;
    @Autowired
    private UserAdminToolbar toolbar;
    @Autowired
    private UserForm userForm;
    private User selectedUser;   

    @Override
    public void enter(ViewChangeListener.ViewChangeEvent event) {
        setSizeFull();
        addStyleName("transactions");

        addComponent(toolbar);
        toolbar.onShow(null);

        addComponent(table);
        table.onShow(null);
        table.addItemClickListener(this);
        setExpandRatio(table, 1);

        // handle user add action
        toolbar.getAddUserButton().addClickListener(new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent event) {
                // Create a sub-window and set the content
                Window subWindow = new Window("New User");
                subWindow.setModal(true);
                subWindow.setWidth("80%");
                subWindow.setContent(userForm);
                Map<String, Object> params = new HashMap<String, Object>();
                params.put("new", "new");
                params.put("window", subWindow);
                userForm.onShow(params);
                // Center it in the browser window
                subWindow.center();
                subWindow.addCloseListener(new Window.CloseListener() {
                    @Override
                    public void windowClose(Window.CloseEvent e) {
                        userForm.onHide();
                        table.getContainer().refresh();
                    }
                });

                UI.getCurrent().addWindow(subWindow);

            }
        });

        // handle user edit action
        toolbar.getEditUserButton().addClickListener(new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent event) {
                // Create a sub-window and set the content
                Window subWindow = new Window("Edit User");
                subWindow.setModal(true);
                subWindow.setWidth("80%");
                subWindow.setContent(userForm);
                Map<String, Object> params = new HashMap<String, Object>();
                params.put("window", subWindow);
                params.put("user", selectedUser);
                userForm.onShow(params);
                // Center it in the browser window
                subWindow.center();
                subWindow.addCloseListener(new Window.CloseListener() {
                    @Override
                    public void windowClose(Window.CloseEvent e) {
                        userForm.onHide();
                        table.getContainer().refresh();
                    }
                });

                UI.getCurrent().addWindow(subWindow);

            }
        });

        // handle user suspend action
        toolbar.getSuspendUserButton().addClickListener(new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent event) {
                selectedUser.setActive(!selectedUser.isActive());                
                UserContainer container = table.getContainer();
                container.getEm().merge(selectedUser);
                EntityTransaction tx = container.getEm().getTransaction();
                tx.begin();
                container.getEm().flush();
                tx.commit();
                container.refresh();
                toolbar.changeSuspendButtonCaption(selectedUser.isActive());
            }
        });
    }

    @Override
    public void itemClick(ItemClickEvent event) {
        selectedUser = ((EntityItem<User>) event.getItem()).getEntity();
        toolbar.enableRowSpecificButtons(selectedUser);
        //logger.info("selected user" + selectedUser.getFirstName());

    }

    @Override
    public void onShow(Map<String, Object> params) {
//        setMargin(true);
//        addComponent(table);
//        table.onShow(params);
    }

    @Override
    public void onHide() {
        toolbar.onHide();
        table.onHide();
        removeAllComponents();
    }

    public UserTable getTable() {
        return table;
    }

    public void setTable(UserTable table) {
        this.table = table;
    }
}
