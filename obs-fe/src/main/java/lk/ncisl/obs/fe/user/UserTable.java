package lk.ncisl.obs.fe.user;

import com.vaadin.data.util.filter.Compare;
import com.vaadin.ui.Table;
import java.util.Map;
import lk.ncisl.obs.fe.ManageableComponent;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author vimukthi
 */
public class UserTable extends Table implements ManageableComponent {
    
    @Autowired
    private UserContainer container;
    
    private static final Object[] DISPLAYED_COLUMNS = 
            new Object[]{ "firstName", "phone", "email", "secondaryEmail", "organization", "active"};

    @Override
    public void onShow(Map<String, Object> params) {
        container.onShow(params);
        container.addContainerFilter(new Compare.Equal("system", 0));
        setContainerDataSource(container);
        setSizeFull();
        setVisibleColumns(DISPLAYED_COLUMNS);
        //setColumnHeader("", null);
        addStyleName("borderless");
        setSelectable(true);
    }

    @Override
    public void onHide() {
        unselect(this.getValue());
        container.removeAllContainerFilters();
        container.onHide();
    }  
    
    public UserContainer getContainer() {
        return container;
    }

    public void setContainer(UserContainer container) {
        this.container = container;
    }
    
}
