package lk.ncisl.obs.fe.system;

import lk.ncisl.obs.data.services.OrderRequestService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author vimukthi
 */
public class OrderExpirator implements ManagerTask {

    private boolean run = true;
    private OrderRequestService orderService;
    private Long updateIntervalMillis;
    private final static Logger logger = LoggerFactory.getLogger(OrderExpirator.class);
    private Manager systemManager;

    public OrderExpirator(OrderRequestService orderService, Long updateIntervalMillis) {
        this.orderService = orderService;
        this.updateIntervalMillis = updateIntervalMillis;
    }

    @Override
    public void run() {
        while (run) {
            orderService.expireOrderRequests();
            try {
                Thread.sleep(updateIntervalMillis);
            } catch (InterruptedException ex) {
                logger.info("Interrupted", ex);
            }
        }
    }

    @Override
    public void stop() {
        this.run = false;
    }

    @Override
    public boolean isRunning() {
       return run;
    }
      
    @Override
    public void setManager(Manager manager) {
        this.systemManager = manager;
    }
    
     @Override
    public void alert() {
        orderService.expireOrderRequests();
    }
}
