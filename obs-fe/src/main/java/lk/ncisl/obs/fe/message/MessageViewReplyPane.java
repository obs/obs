package lk.ncisl.obs.fe.message;

import com.vaadin.addon.jpacontainer.EntityItem;
import com.vaadin.data.Container.Filter;
import com.vaadin.data.fieldgroup.BeanFieldGroup;
import com.vaadin.data.fieldgroup.FieldGroup;
import com.vaadin.data.fieldgroup.FieldGroup.CommitException;
import com.vaadin.data.util.filter.Compare;
import com.vaadin.data.util.filter.Or;
import com.vaadin.data.validator.StringLengthValidator;
import com.vaadin.server.Page;
import com.vaadin.shared.ui.label.ContentMode;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Notification;
import com.vaadin.ui.RichTextArea;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;
import java.util.Date;
import java.util.Iterator;
import java.util.Map;

import lk.ncisl.obs.data.domain.Message;
import lk.ncisl.obs.data.services.MessagingService;
import lk.ncisl.obs.fe.ManageableComponent;
import lk.ncisl.obs.fe.user.UserContainer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author prabhath
 */
public class MessageViewReplyPane extends FormLayout implements ManageableComponent {

    private final static Logger logger = LoggerFactory.getLogger(MessageViewReplyPane.class);

    private Message message;
    private Message parent_message;

    private Window parent;

    private BeanFieldGroup<Message> binder;

    @Autowired
    private MessageContainer messages;

    @Autowired
    private MessagingService messagingService;

    @Autowired
    private UserContainer users;

    // fields
    private ComboBox receiver;
    private TextField title;
    private RichTextArea body;

    // filters
    private Filter parentFilter;
    private Filter receiverFilter;

    private Label errorMessage = new Label();

    VerticalLayout currentMsgsArea;

    @Override
    public void onShow(Map<String, Object> params) {
        setSizeFull();
        setMargin(true);
        setStyleName("obs-subwindow");
        
        users.onShow(params);
        messages.onShow(params);
       
        parent_message = (Message) params.get("parent_message");
        //set message as read
        if(parent_message.getReceiver().equals(users.getCurrentUser())){
            messagingService.markAsRead( parent_message );
        }
        
        
        parent = (Window) params.get("window");       

        // Form for editing the bean
        binder = new BeanFieldGroup<Message>(Message.class);

        // this is a reply message addition
        if (params.containsKey("reply")) {
            message = new Message();
            binder.setItemDataSource(message);
            

            currentMsgsArea = new VerticalLayout();
            addComponent(currentMsgsArea);

            showCurrentMessages();

            bindFields(binder, false);

            // Buffer the form content
            binder.setBuffered(true);
            Button replyButton = new Button("Reply");
            addComponent(replyButton);
            setComponentAlignment(replyButton, ALIGNMENT_DEFAULT.MIDDLE_RIGHT);
            replyButton.addClickListener(new ClickListener() {
                @Override
                public void buttonClick(ClickEvent event) {
                    try {
                        binder.commit();
                        message.setDateCreated(new Date());
                        messagingService.add(message);

                        body.setValue("");
                        showCurrentMessages();

                        /*if (parent != null) {
                         parent.close();
                         }*/
                    } catch (CommitException e) {
                        logger.error("validation", e);
                        // add validation errors on each field here
                        /*errorMessage.setValue("check inputs");
                         errorMessage.setVisible(true);*/

                        Notification notif = new Notification("Error", "Check Inputs !", Notification.Type.ERROR_MESSAGE);
                        notif.setDelayMsec(3000);
                        notif.show(Page.getCurrent());

                    }

                }
            });
        }

        setExpandRatio(currentMsgsArea, 1.0f);

    }

    private void showCurrentMessages() {

        currentMsgsArea.removeAllComponents();

        VerticalLayout parentMsg = new VerticalLayout();
        parentMsg.setStyleName("message-viewreply-parentmsg");
        currentMsgsArea.addComponent(parentMsg);

        Label messageTimeSender = new Label(parent_message.getDateCreated().toGMTString() + " by " + parent_message.getSender().getFirstName(), ContentMode.HTML);
        messageTimeSender.setStyleName("message-viewreply-parentmsg-time");
        parentMsg.addComponent(messageTimeSender);
        Label messageBody = new Label(parent_message.getBody(), ContentMode.HTML);
        messageBody.setStyleName("message-viewreply-parentmsg-body");
        parentMsg.addComponent(messageBody);

        //replies
        parentFilter = new Compare.Equal("parent", parent_message.getId());
        receiverFilter = new Or(new Compare.Equal("receiver", users.getCurrentUser()),
                new Compare.Equal("sender", users.getCurrentUser()));
        messages.addContainerFilter(parentFilter);
        messages.addContainerFilter(receiverFilter);

        VerticalLayout replyMsgContainer = new VerticalLayout();
        replyMsgContainer.setStyleName("message-viewreply-replymsgcontainer");
        currentMsgsArea.setSpacing(true);
        currentMsgsArea.setWidth("100%");
        currentMsgsArea.addComponent(replyMsgContainer);

        for (Iterator i = messages.getItemIds().iterator(); i.hasNext();) {
            // Get the current item identifier, which is an integer.
            int iid = (Integer) i.next();
            // Now get the actual item from the container.
            EntityItem<Message> item = messages.getItem(iid);
            Message replyMessage = item.getEntity();

            VerticalLayout replyMsg = new VerticalLayout();
            replyMsg.setStyleName("message-viewreply-replymsg");
            replyMsgContainer.addComponent(replyMsg);

            Label replyMessageTimeSender = new Label(replyMessage.getDateCreated().toGMTString() + " by " + replyMessage.getSender().getFirstName(), ContentMode.HTML);
            replyMessageTimeSender.setStyleName("message-viewreply-replymsg-time");
            replyMsg.addComponent(replyMessageTimeSender);
            Label replyMessageBody = new Label(replyMessage.getBody(), ContentMode.HTML);
            replyMessageBody.setStyleName("message-viewreply-replymsg-body");
            replyMsg.addComponent(replyMessageBody);

        }

    }

    /**
     *
     * @param binder
     * @param newmsg
     * @throws com.vaadin.data.fieldgroup.FieldGroup.BindException
     */
    private void bindFields(BeanFieldGroup<Message> binder, boolean newmsg) throws FieldGroup.BindException {

        VerticalLayout replyArea = new VerticalLayout();
        addComponent(replyArea);

        errorMessage.setVisible(false);
        replyArea.addComponent(errorMessage);

        //reply msg
        message.setReceiver(parent_message.getSender());
        message.setSender(users.getCurrentUser());
        message.setParent(parent_message.getId());
        message.setTitle(parent_message.getTitle());

        // msg body
        VerticalLayout replyAreaWrap = new VerticalLayout();
        replyAreaWrap.setStyleName("message-viewreply-replyinput");

        body = new RichTextArea("Reply Message: ");
        body.addValidator(new StringLengthValidator("Can not be empty", 1, 5000, false));
        body.setWidth("100%");
        replyAreaWrap.addComponent(body);
        replyArea.addComponent(replyAreaWrap);
        binder.bind(body, "body");

    }

    @Override
    public void onHide() {
        binder.discard();
        removeAllComponents();
    }

}
