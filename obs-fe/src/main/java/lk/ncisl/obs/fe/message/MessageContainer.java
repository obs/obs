package lk.ncisl.obs.fe.message;

import com.vaadin.addon.jpacontainer.JPAContainer;
import com.vaadin.addon.jpacontainer.provider.CachingLocalEntityProvider;
import java.util.Map;
import javax.persistence.EntityManager;


import lk.ncisl.obs.data.domain.Message;
import lk.ncisl.obs.fe.ManageableComponent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;

/**
 *
 * @author prabhath
 */
public class MessageContainer extends JPAContainer<Message> implements ManageableComponent {

    private EntityManager em;
    @Autowired
    private LocalContainerEntityManagerFactoryBean emf;

    public MessageContainer() {
        super(Message.class);
    }

    @Override
    public void onShow(Map<String, Object> params) {
        this.em = emf.getNativeEntityManagerFactory().createEntityManager();
        setEntityProvider(
                new CachingLocalEntityProvider<Message>(
                		Message.class, em));
        refresh();
    }

    @Override
    public void onHide() {
        setEntityProvider(null);
    }

    public EntityManager getEm() {
        return em;
    }

    public void setEm(EntityManager em) {
        this.em = em;
    }
}
