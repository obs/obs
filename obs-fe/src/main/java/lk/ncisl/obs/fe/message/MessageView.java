package lk.ncisl.obs.fe.message;

import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.Property.ValueChangeListener;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.CssLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;

import java.util.HashMap;
import java.util.Map;
import lk.ncisl.obs.fe.ManageableComponent;
import lk.ncisl.obs.fe.user.UserContainer;

import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author prabhath
 */
public class MessageView extends VerticalLayout implements View, ManageableComponent, ValueChangeListener {
    
    @Autowired
    private MessageTable table;    
    
    private CssLayout messageCatalog;
    
    @Autowired
    private MessageForm messageForm;    
    
    @Autowired
    private UserContainer users;
    
    @Override
    public void enter(ViewChangeListener.ViewChangeEvent event) {
        setSizeFull();
        setMargin(true);
        
        HorizontalLayout toolbar = new HorizontalLayout();
        toolbar.setWidth("100%");
        toolbar.setSpacing(true);
        toolbar.setMargin(true);
        toolbar.addStyleName("toolbar");
        addComponent(toolbar);
        
        Label title = new Label("Messages");
        title.addStyleName("h1");
        title.setSizeUndefined();
        toolbar.addComponent(title);
        toolbar.setComponentAlignment(title, Alignment.MIDDLE_LEFT);
        
        final ComboBox filter = new ComboBox();
        filter.setStyleName("obs-order-filterfield");
        filter.setNewItemsAllowed(false);
        filter.setNullSelectionAllowed(false);
        filter.addItem("unread");
        filter.addItem("inbox");
        filter.addItem("sent");
        filter.setItemCaption("unread", "Unread");
        filter.setItemCaption("inbox", "Inbox (All)");
        filter.setItemCaption("sent", "Sent (All)");        
        
        filter.setImmediate(true);
        filter.addValueChangeListener(this);
        
        filter.setInputPrompt("Filter");
        toolbar.addComponent(filter);
        toolbar.setExpandRatio(filter, 1);
        toolbar.setComponentAlignment(filter, Alignment.MIDDLE_LEFT);

        // buttons
        Button newMessage = new Button("New Message");
        newMessage.addStyleName("small");
        toolbar.addComponent(newMessage);
        toolbar.setComponentAlignment(newMessage, Alignment.MIDDLE_RIGHT);
        
        newMessage.addClickListener(new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent event) {
                // Create a sub-window and set the content
                Window subWindow = new Window("New Message");
                subWindow.setModal(true);
                subWindow.setWidth("80%");
                subWindow.setContent(messageForm);
                Map<String, Object> params = new HashMap<String, Object>();
                params.put("new", "new");
                params.put("window", subWindow);
                messageForm.onShow(params);
                // Center it in the browser window
                subWindow.center();
                subWindow.addCloseListener(new Window.CloseListener() {
                    @Override
                    public void windowClose(Window.CloseEvent e) {
                        messageForm.onHide();
                        
                        Map<String, Object> params = new HashMap<String, Object>();
                        refreshView(params);
                    }
                });
                
                UI.getCurrent().addWindow(subWindow);
                
            }
        });

        // container for order widgets
        messageCatalog = new CssLayout();
        messageCatalog.setSizeFull();
        messageCatalog.setStyleName("obs-order-catalog");
        addComponent(messageCatalog);

        //display messages
        messageCatalog.removeAllComponents();
        
        Map<String, Object> params = new HashMap<String, Object>();        
        messageCatalog.addComponent(table);
        table.setSizeFull();
        
        filter.select("unread");
        params.put("selected", "unread");
        table.applyFilters(params);
        
        table.onShow(params);
        
        setExpandRatio(messageCatalog, 1.0f);
    }
    
    @Override
    public void onShow(Map<String, Object> params) {
        setMargin(true);
        addComponent(table);
        table.onShow(params);
    }
    
    @Override
    public void onHide() {
        removeAllComponents();
    }
    
    public void refreshView(Map<String, Object> params) {
        table.onShow(params);
    }
    
    public MessageTable getTable() {
        return table;
    }
    
    public void setTable(MessageTable table) {
        this.table = table;
    }
    
    @Override
    public void valueChange(ValueChangeEvent event) {
        
        String selected = event.getProperty().getValue().toString();
        
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("allList", "allList");        
        params.put("selected", selected);        
        table.applyFilters(params);
        
    }
    
}
