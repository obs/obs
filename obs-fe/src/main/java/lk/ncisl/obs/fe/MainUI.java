package lk.ncisl.obs.fe;

import com.vaadin.annotations.Push;
import com.vaadin.annotations.Theme;
import com.vaadin.annotations.VaadinServletConfiguration;
import com.vaadin.server.VaadinRequest;
import com.vaadin.server.VaadinServlet;
import com.vaadin.server.VaadinSession;
import com.vaadin.shared.communication.PushMode;
import com.vaadin.ui.CssLayout;
import com.vaadin.ui.UI;
import java.util.HashMap;
import java.util.Map;
import javax.servlet.annotation.WebServlet;
import lk.ncisl.obs.fe.layout.LoginLayout;
import lk.ncisl.obs.fe.layout.MainLayout;
import org.springframework.context.ApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

/**
 *
 * @author vimukthi
 */
@Push
@Theme("dashboard")
@SuppressWarnings("serial")
public class MainUI extends UI {

    private ApplicationContext applicationContext;
    private MainLayout mainLayout;
    private CssLayout root;
    private LoginLayout loginLayout;

    @WebServlet(value = "/*", asyncSupported = true)
    @VaadinServletConfiguration(productionMode = false, 
            ui = MainUI.class, widgetset = "lk.ncisl.obs.fe.AppWidgetSet", closeIdleSessions = true)
    public static class Servlet extends VaadinServlet {
    }

    @Override
    protected void init(VaadinRequest request) {
        applicationContext = WebApplicationContextUtils.
                getWebApplicationContext(VaadinServlet.getCurrent()
                .getServletContext());
        root = new CssLayout();
        setContent(root);
        root.addStyleName("root");
        root.setSizeFull();
        
        /**
         * some ugly hacks to make the session work as expected with vaadin push
         * The issues faced with jetty and others can be referred with following links
         * https://github.com/Atmosphere/atmosphere/issues/978
         * http://dev.vaadin.com/ticket/11808
         * http://dev.vaadin.com/ticket/13087
         * http://dev.vaadin.com/ticket/11721
         */
        UI.getCurrent().getPushConfiguration().setPushMode(PushMode.DISABLED);
        if (VaadinSession.getCurrent().getSession().getAttribute("user") != null) {    
            UI.getCurrent().getPushConfiguration().setPushMode(PushMode.AUTOMATIC);
            buildMainView();
        } else {            
            buildLoginView(false);
        }
    }

    private void buildLoginView(boolean exit) {
        if (exit) {
            root.removeAllComponents();
        }
        addStyleName("login");
        loginLayout = (LoginLayout) applicationContext.getBean("loginLayout");
        root.addComponent(loginLayout);
        loginLayout.onShow(null);
    }

    /**
     * Called within loginview
     */
    public void buildMainView() {
        removeStyleName("login");
        root.removeAllComponents();
        mainLayout = (MainLayout) applicationContext.getBean("mainLayout");
        root.addComponent(mainLayout);
        Map<String, Object> params = new HashMap<String, Object>();
        mainLayout.onShow(params);

    }
}
