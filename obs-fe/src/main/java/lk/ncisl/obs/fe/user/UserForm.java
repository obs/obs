package lk.ncisl.obs.fe.user;

import com.vaadin.data.fieldgroup.BeanFieldGroup;
import com.vaadin.data.fieldgroup.FieldGroup;
import com.vaadin.data.fieldgroup.FieldGroup.CommitException;
import com.vaadin.data.validator.EmailValidator;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.TextField;
import com.vaadin.ui.Window;
import java.util.Map;
import lk.ncisl.obs.data.domain.Role;
import lk.ncisl.obs.data.domain.User;
import lk.ncisl.obs.fe.ManageableComponent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author vimukthi
 */
public class UserForm extends FormLayout implements ManageableComponent {

    private final static Logger logger = LoggerFactory.getLogger(UserForm.class);
    private User user;
    @Autowired
    private UserContainer container;
    @Autowired
    private RoleContainer roleContainer;
    private Window parent;
    private BeanFieldGroup<User> binder;
    private Label errorMessage = new Label();

    @Override
    public void onShow(Map<String, Object> params) {
        container.onShow(params);
        final Role bidder = roleContainer.getRoleByName("bidder");
        // Form for editing the bean
        binder = new BeanFieldGroup<User>(User.class);
        parent = (Window) params.get("window");

        // this is a new user addition
        if (params.containsKey("new")) {
            user = new User();
            binder.setItemDataSource(user);
            bindFields(binder, true);

            // Buffer the form content
            binder.setBuffered(true);
            addComponent(new Button("OK", new ClickListener() {
                @Override
                public void buttonClick(ClickEvent event) {
                    try {
                        binder.commit();
                        // container handles transaction here
                        container.addBiddingUser(user);
                        if (parent != null) {
                            parent.close();
                        }
                    } catch (CommitException e) {
                        logger.error("validation", e);
                    }
                }
            }));
        } else {
            // this is an existing user edition
            user = (User) params.get("user");
            binder.setItemDataSource(user);
            bindFields(binder, false);

            // Buffer the form content
            binder.setBuffered(true);
            addComponent(new Button("OK", new ClickListener() {
                @Override
                public void buttonClick(ClickEvent event) {
                    try {
                        binder.commit();
                        container.saveUser(user);
                        if (parent != null) {
                            parent.close();
                        }
                    } catch (CommitException e) {
                        logger.error("validation", e);
                        // add validation errors on each field here
                        errorMessage.setValue("check inputs");
                        errorMessage.setVisible(true);
                    }
                }
            }));
        }
    }

    /**
     * Bind entity variables to form fields
     *
     * @param binder
     * @param newuser if this is a new user
     * @throws com.vaadin.data.fieldgroup.FieldGroup.BindException
     */
    private void bindFields(BeanFieldGroup<User> binder, boolean newuser) throws FieldGroup.BindException {
        errorMessage.setVisible(false);
        addComponent(errorMessage);
        if (newuser) {
            TextField email = new TextField("Email");
            email.addValidator(new EmailValidator("Invalid Email address"));
            addComponent(email);
            binder.bind(email, "email");
            // this password is not set as a password field because it is not secure anyway
            addComponent(binder.buildAndBind("Password", "password"));
        }
        //-TODO fields needs to be validated and prettified
        addComponent(binder.buildAndBind("First Name", "firstName"));
        addComponent(binder.buildAndBind("Last Name", "lastName"));
        addComponent(binder.buildAndBind("Phone", "phone"));
        addComponent(binder.buildAndBind("Mobile", "mobile"));
        TextField secondaryEmail = new TextField("Secondary Email");
        secondaryEmail.addValidator(new EmailValidator("Invalid Email address"));
        addComponent(secondaryEmail);
        binder.bind(secondaryEmail, "secondaryEmail");
        addComponent(binder.buildAndBind("Organization", "organization"));
        //these will be updated by the user
        //addComponent(binder.buildAndBind("Photo", "photo"));            
        //addComponent(binder.buildAndBind("Security Question", "securityQuestion"));
        //addComponent(binder.buildAndBind("Answer", "securityQuestionAnswer"));
    }

    @Override
    public void onHide() {
        binder.discard();
        removeAllComponents();
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public UserContainer getContainer() {
        return container;
    }

    public void setContainer(UserContainer container) {
        this.container = container;
    }

    public RoleContainer getRoleContainer() {
        return roleContainer;
    }

    public void setRoleContainer(RoleContainer roleContainer) {
        this.roleContainer = roleContainer;
    }
}
