package lk.ncisl.obs.fe.order;

import com.vaadin.addon.jpacontainer.JPAContainer;
import com.vaadin.addon.jpacontainer.provider.CachingMutableLocalEntityProvider;
import java.util.Map;
import javax.persistence.EntityManager;

import lk.ncisl.obs.data.domain.OrderState;
import lk.ncisl.obs.fe.ManageableComponent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;

/**
 *
 * @author vimukthi
 */
public class OrderStateContainer extends JPAContainer<OrderState> implements ManageableComponent {

    private EntityManager em;
    @Autowired
    private LocalContainerEntityManagerFactoryBean emf;

    public OrderStateContainer() {
        super(OrderState.class);
    }

    @Override
    public void onShow(Map<String, Object> params) {
        this.em = emf.getNativeEntityManagerFactory().createEntityManager();
        setEntityProvider(
                new CachingMutableLocalEntityProvider<OrderState>(
                OrderState.class, em));
    }

    @Override
    public void onHide() {
        setEntityProvider(null);
    }
    
    /**
     * 
     * @param name
     * @return 
     */
    public OrderState getOrderStateByName(String name) {
        OrderState orderState = (OrderState) em.createNamedQuery("OrderState.findByName").setParameter("name", name).getSingleResult();
        return orderState;
    }

    public EntityManager getEm() {
        return em;
    }

    public void setEm(EntityManager em) {
        this.em = em;
    }
}
