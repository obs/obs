package lk.ncisl.obs.fe.bid;

import com.vaadin.data.fieldgroup.BeanFieldGroup;
import com.vaadin.data.fieldgroup.FieldGroup;
import com.vaadin.data.fieldgroup.FieldGroup.CommitException;
import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.data.util.filter.Compare;
import com.vaadin.data.util.filter.Or;
import com.vaadin.data.validator.IntegerRangeValidator;
import com.vaadin.data.validator.StringLengthValidator;
import com.vaadin.server.FileResource;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.DateField;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.Image;
import com.vaadin.ui.Label;
import com.vaadin.ui.Notification;
import com.vaadin.ui.RichTextArea;
import com.vaadin.ui.TextField;
import com.vaadin.ui.Upload;
import com.vaadin.ui.Upload.Receiver;
import com.vaadin.ui.Upload.SucceededListener;
import com.vaadin.ui.Window;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.util.Date;
import java.util.Map;
import lk.ncisl.obs.data.domain.Bid;
import lk.ncisl.obs.data.domain.OrderState;
import lk.ncisl.obs.data.domain.OrderRequest;
import lk.ncisl.obs.data.domain.User;
import lk.ncisl.obs.data.services.BiddingService;
import lk.ncisl.obs.data.services.exceptions.NonActiveOrderException;
import lk.ncisl.obs.fe.InvariantConfigs;
import lk.ncisl.obs.fe.ManageableComponent;
import lk.ncisl.obs.fe.order.OrderContainer;
import lk.ncisl.obs.fe.order.OrderStateContainer;
import lk.ncisl.obs.fe.user.UserContainer;
import lk.ncisl.obs.fe.utils.converters.OrderRequestConverter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author vimukthi
 */
public class BidForm extends FormLayout implements ManageableComponent, Receiver, SucceededListener {

    private final static Logger logger = LoggerFactory.getLogger(BidForm.class);
    private OrderRequest order;
    private Bid bid;
    private Window parent;
    private String uploadLocation;
    private File orderImage;
    private String orderImageFileName;
    private Image orderImageDisplay = new Image();
    private BeanFieldGroup<Bid> binder;
    @Autowired
    private BiddingService biddingService;
    @Autowired
    private OrderContainer orders;
    @Autowired
    private OrderStateContainer orderStates;
    @Autowired
    private UserContainer users;
    // fields
    private ComboBox forOrder;
    private ComboBox onBehalfOf;
    private TextField pricePerUnit;
    private RichTextArea description;
    private TextField quantity;
    private DateField expirationDate;
    private Upload upload;
    private Label errorMessage = new Label();

    @Override
    public void onShow(Map<String, Object> params) {
        parent = (Window) params.get("window");
        orders.onShow(params);
        orderStates.onShow(params);
        // only allow bids to be placed on orders which on PHASE1 or PHASE2 states
        orders.addContainerFilter(new Or(
                new Compare.Equal("state", orderStates.getOrderStateByName(OrderState.ValidStates.PHASE1)),
                new Compare.Equal("state", orderStates.getOrderStateByName(OrderState.ValidStates.PHASE2))));
        users.onShow(params);
        // Form for editing the bean
        binder = new BeanFieldGroup<Bid>(Bid.class);
        

        // this is a new bid addition
        if (params.containsKey("new")) {
            bid = new Bid();
            order = params.get("order") != null ? (OrderRequest) params.get("order") : null;
            binder.setItemDataSource(bid);
            bindFields(binder, true);

            // Buffer the form content
            binder.setBuffered(true);
            addComponent(new Button("OK", new ClickListener() {
                @Override
                public void buttonClick(ClickEvent event) {
                    try {
                        binder.commit();
                        bid.setDateCreated(new Date());
                        biddingService.add(bid);
                        if (parent != null) {
                            parent.close();
                        }
                    } catch (CommitException e) {
                        logger.error("validation", e);
                        // add validation errors on each field here
                        errorMessage.setValue("check inputs");
                        errorMessage.setVisible(true);
                    } catch (NonActiveOrderException ex) {
                        logger.error("order inactive", ex);
                        // add validation errors on each field here
//                        errorMessage.setValue("check inputs");
//                        errorMessage.setVisible(true);
                    }
                }
            }));
        } else {
//            // this is an existing user edition
//            user = (User) params.get("user");
//            binder.setItemDataSource(user);
//            bindFields(binder, false);
//
//            // Buffer the form content
//            binder.setBuffered(true);
//            addComponent(new Button("OK", new ClickListener() {
//                @Override
//                public void buttonClick(ClickEvent event) {
//                    try {
//                        binder.commit();
//                        container.getEm().merge(user);
//                        // start a transaction manually to update record
//                        EntityTransaction tx = container.getEm().getTransaction();
//                        tx.begin();
//                        container.getEm().flush();
//                        tx.commit();
//                        if (parent != null) parent.close();
//                    } catch (CommitException e) {
//                        logger.error("validation", e);
//                    }
//                }
//            }));
        }
    }

    /**
     *
     * @param binder
     * @param newbid
     * @throws com.vaadin.data.fieldgroup.FieldGroup.BindException
     */
    private void bindFields(BeanFieldGroup<Bid> binder, boolean newbid) throws FieldGroup.BindException {
        errorMessage.setVisible(false);
        addComponent(errorMessage);
        if (order == null) {
            // order if the order was not specified from the parent screen
            forOrder = new ComboBox("Order Code");
            forOrder.setContainerDataSource(orders);
            forOrder.setItemCaptionPropertyId("code");
            //forOrder.setImmediate(true);
            forOrder.setInvalidAllowed(false);
            forOrder.setConverter(new OrderRequestConverter(orders));
            forOrder.setTextInputAllowed(false);
            forOrder.setWidth("150px");
            forOrder.setNullSelectionAllowed(false);
            addComponent(forOrder);
            binder.bind(forOrder, "orderRequest");

        } else {
            bid.setOrderRequest(order);
        }
        if (users.getCurrentUser().isAdmin()) {
            // order if the order was not specified from the parent screen
            onBehalfOf = new ComboBox("On Behalf of");
            BeanItemContainer<User> bidders = new BeanItemContainer<User>(User.class);
            bidders.addAll(users.getBiddingUsers());
            onBehalfOf.setContainerDataSource(bidders);
            onBehalfOf.setItemCaptionPropertyId("email");
            //onBehalfOf.setImmediate(true);
            onBehalfOf.setInvalidAllowed(false);
            onBehalfOf.setTextInputAllowed(false);
            onBehalfOf.setWidth("200px");
            onBehalfOf.setNullSelectionAllowed(false);
            addComponent(onBehalfOf);
            binder.bind(onBehalfOf, "bidder");

        } else {
            // if this form is open with anyone other than the admin user
            // that user has to be bidder
            assert users.getCurrentUser().isInRole("bidder");
            bid.setBidder(users.getCurrentUser());
        }

        // price per unit
        pricePerUnit = new TextField("Price per Unit(Rs.)");
        //pricePerUnit.addValidator(new DoubleRangeValidator("Enter float number", 0.01, Double.MAX_VALUE));
        addComponent(pricePerUnit);
        binder.bind(pricePerUnit, "pricePerUnit");

        // quantity
        quantity = new TextField("Quantity");
        quantity.addValidator(new IntegerRangeValidator("Non zero", 1, Integer.MAX_VALUE));
        addComponent(quantity);
        binder.bind(quantity, "quantity");

        // description
        description = new RichTextArea("Description");
        description.addValidator(new StringLengthValidator("Can not be empty", 1, 5000, false));
        addComponent(description);
        binder.bind(description, "description");

        // image upload
//        upload = new Upload("Upload Image", this);
//        upload.addSucceededListener(this);
//        addComponent(upload);
//        orderImageDisplay.setVisible(false);
//        addComponent(orderImageDisplay);
    }

    @Override
    public OutputStream receiveUpload(String filename, String mimeType) {
        // Create upload stream
        FileOutputStream fos = null; // Stream to write to
        try {
            orderImageFileName = filename;
            // Open the file for writing.
            orderImage = new File(uploadLocation + InvariantConfigs.ORDERS_IMAGES_PATH + filename);
            fos = new FileOutputStream(orderImage);
        } catch (FileNotFoundException e) {
            Notification.show("Error occured while uploading", Notification.Type.ERROR_MESSAGE);
            logger.error("Image Upload error", e);
            return null;
        }
        return fos; // Return the output stream to write to
    }

    @Override
    public void uploadSucceeded(Upload.SucceededEvent event) {
        // Show the uploaded file in the image viewer
        orderImageDisplay.setVisible(true);
        orderImageDisplay.setSource(new FileResource(orderImage));
        orderImageDisplay.setWidth(InvariantConfigs.ORDERLIST_IMAGE_DISPLAY_WIDTH);
        orderImageDisplay.setHeight(InvariantConfigs.ORDERLIST_IMAGE_DISPLAY_HEIGHT);
        order.setImage(orderImageFileName);
    }

    @Override
    public void onHide() {
        binder.discard();
        removeAllComponents();
    }

    public String getUploadLocation() {
        return uploadLocation;
    }

    public void setUploadLocation(String uploadLocation) {
        this.uploadLocation = uploadLocation;
    }
}
