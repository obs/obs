package lk.ncisl.obs.fe.utils.converters;

import com.vaadin.data.util.converter.Converter;
import java.util.Locale;
import lk.ncisl.obs.data.domain.OrderRequest;
import lk.ncisl.obs.fe.order.OrderContainer;

/**
 *
 * @author vimukthi
 */
public class OrderRequestConverter implements Converter<Object, OrderRequest> {

    private OrderContainer orders;

    public OrderRequestConverter(OrderContainer orders) {
        this.orders = orders;
    }

    @Override
    public OrderRequest convertToModel(Object value, Class<? extends OrderRequest> targetType, Locale locale) throws Converter.ConversionException {
        return orders.getItem(value).getEntity();
    }

    @Override
    public Object convertToPresentation(OrderRequest value, Class<? extends Object> targetType, Locale locale) throws Converter.ConversionException {
        Object id = value == null ? 1 : value.getId();
        return id;
    }

    @Override
    public Class<OrderRequest> getModelType() {
        return OrderRequest.class;
    }

    @Override
    public Class<Object> getPresentationType() {
        return Object.class;
    }
}
