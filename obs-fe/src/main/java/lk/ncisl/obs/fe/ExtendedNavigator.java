package lk.ncisl.obs.fe;

import com.vaadin.navigator.Navigator;
import com.vaadin.navigator.View;
import com.vaadin.ui.ComponentContainer;
import com.vaadin.ui.UI;
import java.util.Map;

/**
 *
 * @author vimukthi
 */
public class ExtendedNavigator extends Navigator implements ManageableComponent {
    
    // map of all routes in app
    private Map<String, View> routes;

    public ExtendedNavigator(UI ui, ComponentContainer container) {
        super(ui, container);
    }  

    @Override
    public void onShow(Map<String, Object> params) {
        for (String route : routes.keySet()) {
            addView(route, routes.get(route));
        }
        
        //addViewChangeListener(this);
        
        // check the active session when navigating
//        addViewChangeListener(new ViewChangeListener() {
//            
//            @Override
//            public boolean beforeViewChange(ViewChangeEvent event) {
//                
//                // Check if a user has logged in
//                boolean isLoggedIn = VaadinSession.getCurrent().getSession().getAttribute("user") != null;
//                //boolean isLoginView = event.getNewView() instanceof SimpleLoginView;
//
//                if (!isLoggedIn) {
//                    // Redirect to login view always if a user has not yet
//                    // logged in
//                    navigateTo(SimpleLoginView.NAME);
//                    return false;
//
//                } else if (isLoggedIn && isLoginView) {
//                    // If someone tries to access to login view while logged in,
//                    // then cancel
//                    return false;
//                }
//
//                return true;
//            }
//            
//            @Override
//            public void afterViewChange(ViewChangeEvent event) {
//                
//            }
//        });
    }

    @Override
    public void navigateTo(String navigationState) {
        String prevState = this.getState();
        ManageableComponent current = (ManageableComponent) this.routes.get(prevState);
        // call onHide to cleanup previous view
        if(current != null){
            current.onHide();    
        }           
        super.navigateTo(navigationState);         
    }    

    @Override
    public void onHide() {
        
    }

    public Map<String, View> getRoutes() {
        return routes;
    }

    public void setRoutes(Map<String, View> routes) {
        this.routes = routes;
    }    
}
