package lk.ncisl.obs.fe.system;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import lk.ncisl.obs.data.utils.ObsTimeUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * timer to count the time till expiry date
 *
 * @author vimukthi
 */
public class ExpiryTimeCounter implements ManagerTask {

    private Manager systemManager;

    public interface Updatable {

        public void update(long diffDays, long diffHours, long diffMinutes, long diffSeconds);

        public void refreshParent();
    }
    private final static Logger logger = LoggerFactory.getLogger(ExpiryTimeCounter.class);
    private boolean run = true;
    private final Map<Updatable, Map<String, Object>> updatables = new HashMap<Updatable, Map<String, Object>>();
    private String currentUserTimeZone;

    public ExpiryTimeCounter(String currentUserTimeZone) {
        this.currentUserTimeZone = currentUserTimeZone;
    }

    public void addUpdatable(Updatable updatable, Date expiryDate, String timeZone) {
        synchronized (updatables) {
            Map<String, Object> details = new HashMap<String, Object>();
            details.put("timezone", timeZone);
            details.put("expirydate", expiryDate);
            updatables.put(updatable, details);
        }
    }

    public void removeAllUpdatables() {
        synchronized (updatables) {
            updatables.clear();
        }
    }

    @Override
    public void run() {
        while (run) {
            Updatable toRefresh = null;
            synchronized (updatables) {
                for (Map.Entry<Updatable, Map<String, Object>> entry : updatables.entrySet()) {
                    Updatable updatable = entry.getKey();
                    Map<String, Object> details = entry.getValue();
                    Long diff = ObsTimeUtils.getTimeLeft(
                            (Date) details.get("expirydate"), 
                            (String) details.get("timezone"));

                    if (diff < 0) {
                        // to avoid race conditions we just remember that ui needs to be updated instead of doing it here.
                        toRefresh = updatable;
                    } else {
                        long diffDays = diff / (24 * 60 * 60 * 1000);
                        long diffHours = diff / (60 * 60 * 1000) % 24;
                        long diffMinutes = diff / (60 * 1000) % 60;
                        long diffSeconds = diff / 1000 % 60;
                        updatable.update(diffDays, diffHours, diffMinutes, diffSeconds);
                    }
                }
            }
            if (toRefresh != null) {
                // orders needs to be expired and ui needs to be refreshed
                systemManager.alertExpirator();
                toRefresh.refreshParent();
            }
            try {
                // sleep for one second so that the clock tick 1 second at a time
                Thread.sleep(1000);
            } catch (InterruptedException ex) {
                logger.debug("Timer thread interupted", ex);
            }
        }
    }

    @Override
    public void stop() {
        this.run = false;
        removeAllUpdatables();
    }

    @Override
    public boolean isRunning() {
        return run;
    }

    @Override
    public void setManager(Manager manager) {
        this.systemManager = manager;
    }

    @Override
    public void alert() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
