package lk.ncisl.obs.fe.user;

import com.vaadin.addon.jpacontainer.JPAContainer;
import com.vaadin.addon.jpacontainer.provider.CachingLocalEntityProvider;
import java.util.Map;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import lk.ncisl.obs.data.domain.Role;
import lk.ncisl.obs.fe.ManageableComponent;

/**
 *
 * @author vimukthi
 */
public class RoleContainer extends JPAContainer<Role> implements ManageableComponent {

    @PersistenceContext
    private EntityManager em;    

    public RoleContainer() {
        super(Role.class);
    }

    @Override
    public void onShow(Map<String, Object> params) {
        setEntityProvider(
                new CachingLocalEntityProvider<Role>(
                		Role.class, em));
        refresh();
    }

    @Override
    public void onHide() {
        setEntityProvider(null);
    }
    
    /**
     * get role for name
     * @param name
     * @return 
     */
    public Role getRoleByName(String name) {
        Role role = (Role) em.createNamedQuery("Role.findByName").setParameter("name", name).getSingleResult();
        return role;
    }

    public EntityManager getEm() {
        return em;
    }

    public void setEm(EntityManager em) {
        this.em = em;
    }
    
}
