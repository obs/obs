package lk.ncisl.obs.fe.bid;

import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.CssLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.VerticalLayout;
import java.util.HashMap;
import java.util.Map;
import lk.ncisl.obs.fe.ManageableComponent;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author vimukthi
 */
public class BidView extends VerticalLayout implements View, ManageableComponent {
    
    @Autowired
    private BidTable table;    
    private CssLayout bidCatalog;

    @Override
    public void enter(ViewChangeListener.ViewChangeEvent event) {
    	setSizeFull();
    	setMargin(true);
    	
    	HorizontalLayout toolbar = new HorizontalLayout();
        toolbar.setWidth("100%");
        toolbar.setSpacing(true);
        toolbar.setMargin(true);
        toolbar.addStyleName("toolbar");
        addComponent(toolbar);

        Label title = new Label("Bids");
        title.addStyleName("h1");
        title.setSizeUndefined();
        toolbar.addComponent(title);
        toolbar.setComponentAlignment(title, Alignment.MIDDLE_LEFT);
        
        final ComboBox filter = new ComboBox();
        filter.setStyleName("obs-order-filterfield");
        filter.setNewItemsAllowed(true);
        filter.addItem("Order Request 1");
        filter.addItem("Order Request 2");
        filter.addItem("Order Request 3");
        
        filter.setInputPrompt("Filter");
        toolbar.addComponent(filter);
        toolbar.setExpandRatio(filter, 1);
        toolbar.setComponentAlignment(filter, Alignment.MIDDLE_LEFT);
        
        // container for order widgets
        bidCatalog = new CssLayout();
        bidCatalog.setSizeFull();
        bidCatalog.setStyleName("obs-order-catalog");
        addComponent(bidCatalog);
    	
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("allBidList", "allBidList");
        bidCatalog.addComponent(table);
        table.setSizeFull();
        table.onShow(params);
        
        setExpandRatio(bidCatalog, 1.0f);
    }

    @Override
    public void onShow(Map<String, Object> params) {
        setMargin(true);
        addComponent(table);
        table.onShow(params);
    }

    @Override
    public void onHide() {
        removeAllComponents();
    }
    
    public BidTable getTable() {
        return table;
    }

    public void setTable(BidTable table) {
        this.table = table;
    }
    
}
