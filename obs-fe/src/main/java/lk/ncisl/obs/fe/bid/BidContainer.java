package lk.ncisl.obs.fe.bid;

import com.vaadin.addon.jpacontainer.JPAContainer;
import com.vaadin.addon.jpacontainer.provider.CachingLocalEntityProvider;
import java.util.Map;
import javax.persistence.EntityManager;

import lk.ncisl.obs.data.domain.Bid;
import lk.ncisl.obs.fe.ManageableComponent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;

/**
 *
 * @author vimukthi
 */
public class BidContainer extends JPAContainer<Bid> implements ManageableComponent {

    private EntityManager em;
    @Autowired
    private LocalContainerEntityManagerFactoryBean emf;

    public BidContainer() {
        super(Bid.class);
    }

    @Override
    public void onShow(Map<String, Object> params) {
        this.em = emf.getNativeEntityManagerFactory().createEntityManager();
        setEntityProvider(
                new CachingLocalEntityProvider<Bid>(
                Bid.class, em));
        refresh();
    }

    @Override
    public void onHide() {
        setEntityProvider(null);
    }

    public EntityManager getEm() {
        return em;
    }

    public void setEm(EntityManager em) {
        this.em = em;
    }
}
