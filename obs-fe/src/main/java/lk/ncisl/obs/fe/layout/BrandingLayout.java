package lk.ncisl.obs.fe.layout;

import com.vaadin.server.ThemeResource;
import com.vaadin.shared.ui.label.ContentMode;
import com.vaadin.ui.CssLayout;
import com.vaadin.ui.Image;
import com.vaadin.ui.Label;
import com.vaadin.ui.VerticalLayout;

import java.util.Map;
import lk.ncisl.obs.fe.ManageableComponent;

/**
 *
 * @author vimukthi
 */
public class BrandingLayout extends VerticalLayout implements ManageableComponent {

    @Override
    public void onShow(Map<String, Object> params) {
    	    	
        addStyleName("nci-branding");
        setSizeFull();
        setHeight("70px");
        
        /*Label logo = new Label(
                "NCISL <span>Online Bidding System</span> ",
                ContentMode.HTML);
        logo.setSizeUndefined();*/
        
        Image logo = new Image(null, new ThemeResource("img/logo.png"));
        logo.setHeight("42px");
        logo.setWidth("182px");
        addComponent(logo);
        
        setComponentAlignment(logo, ALIGNMENT_DEFAULT.MIDDLE_CENTER);
                
    }

    @Override
    public void onHide() {
        removeAllComponents();
    }
}
