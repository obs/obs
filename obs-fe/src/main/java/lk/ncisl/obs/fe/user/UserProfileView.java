/**
 * DISCLAIMER
 * 
 * The quality of the code is such that you should not copy any of it as best
 * practice how to build Vaadin applications.
 * 
 * @author jouni@vaadin.com
 * 
 */

package lk.ncisl.obs.fe.user;

import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.shared.ui.MarginInfo;
import com.vaadin.shared.ui.label.ContentMode;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.Component;
import com.vaadin.ui.CssLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Table;
import com.vaadin.ui.Table.Align;
import com.vaadin.ui.Table.RowHeaderMode;
import com.vaadin.ui.VerticalLayout;
import java.util.HashMap;
import java.util.Map;
import lk.ncisl.obs.data.domain.User;
import lk.ncisl.obs.fe.ManageableComponent;
import lk.ncisl.obs.fe.utils.StaticUtils;
import org.springframework.beans.factory.annotation.Autowired;

public class UserProfileView extends VerticalLayout implements View, ManageableComponent {

    Table t;
    
    @Autowired
    private UserContainer users;

    public UserProfileView() {
        

    }

    private CssLayout createPanel(Component content) {
        CssLayout panel = new CssLayout();
        panel.addStyleName("layout-panel");
        panel.setSizeFull();

        Button configure = new Button();
        configure.addStyleName("configure");
        configure.addStyleName("icon-cog");
        configure.addStyleName("icon-only");
        configure.addStyleName("borderless");
        configure.setDescription("Configure");
        configure.addStyleName("small");
        configure.addClickListener(new ClickListener() {
            @Override
            public void buttonClick(ClickEvent event) {
                Notification.show("Not implemented in this demo");
            }
        });
        panel.addComponent(configure);

        panel.addComponent(content);
        return panel;
    }

    @Override
    public void enter(ViewChangeEvent event) {
        setSizeFull();
        addStyleName("dashboard-view");

        HorizontalLayout top = new HorizontalLayout();
        top.setWidth("100%");
        top.setSpacing(true);
        top.addStyleName("toolbar");
        addComponent(top);
        final Label title = new Label("My Profile");
        title.setSizeUndefined();
        title.addStyleName("h1");
        top.addComponent(title);
        top.setComponentAlignment(title, Alignment.MIDDLE_LEFT);
        top.setExpandRatio(title, 1);
        
        HorizontalLayout row = new HorizontalLayout();
        row.setSizeFull();
        row.setMargin(new MarginInfo(true, true, false, true));
        row.setSpacing(true);
        addComponent(row);
        setExpandRatio(row, 1.5f);

        
        Map<String, Object> params = new HashMap<String, Object>();
        users.onShow(params);
        User currentUser = users.getCurrentUser();        
        VerticalLayout profileDetails = new VerticalLayout();  
        profileDetails.setCaption("Profile Details");
        profileDetails.setSizeFull();
        profileDetails.addComponent(new Label( StaticUtils.lableCaptionWrap("Name: ") + currentUser.getFirstName() + " " + currentUser.getLastName(), ContentMode.HTML));
        profileDetails.addComponent(new Label( StaticUtils.lableCaptionWrap("About: ") + currentUser.getAboutMe(), ContentMode.HTML));
        profileDetails.addComponent(new Label( StaticUtils.lableCaptionWrap("Organization: ") + currentUser.getOrganization(), ContentMode.HTML));
        profileDetails.addComponent(new Label( StaticUtils.lableCaptionWrap("Phone: ") + currentUser.getPhone(), ContentMode.HTML));
        profileDetails.addComponent(new Label( StaticUtils.lableCaptionWrap("Mobile: ") + currentUser.getMobile(), ContentMode.HTML));
        profileDetails.addComponent(new Label( StaticUtils.lableCaptionWrap("Email: ") + currentUser.getEmail(), ContentMode.HTML));
        profileDetails.addComponent(new Label( StaticUtils.lableCaptionWrap("Secondary Email: ") + currentUser.getSecondaryEmail(), ContentMode.HTML));
        profileDetails.addComponent(new Label( StaticUtils.lableCaptionWrap("Photo TEST: ") + currentUser.getPhoto(), ContentMode.HTML));
        
        CssLayout panel = createPanel(profileDetails);         
        row.addComponent(panel);

        row = new HorizontalLayout();
        row.setMargin(true);
        row.setSizeFull();
        row.setSpacing(true);
        addComponent(row);
        setExpandRatio(row, 2);

        /* Create the table with a caption. */
        t = new Table("Cerificates");

        /* Define the names and data types of columns.
         * The "default value" parameter is meaningless here. */
        t.addContainerProperty("Document Name", String.class,  null);
        t.addContainerProperty("Ref",  String.class,  null);
        t.addContainerProperty("Year",       Integer.class, null);

        /* Add a few items in the table. */
        t.addItem(new Object[] {
            "Nicolaus","Copernicus",new Integer(1473)}, new Integer(1));
        t.addItem(new Object[] {
            "Tycho",   "Brahe",     new Integer(1546)}, new Integer(2));
        t.addItem(new Object[] {
            "Giordano","Bruno",     new Integer(1548)}, new Integer(3));
        t.addItem(new Object[] {
            "Galileo", "Galilei",   new Integer(1564)}, new Integer(4));
        t.addItem(new Object[] {
            "Johannes","Kepler",    new Integer(1571)}, new Integer(5));
        t.addItem(new Object[] {
            "Isaac",   "Newton",    new Integer(1643)}, new Integer(6));
        
        t.setSizeFull();
        t.setPageLength(0);
        t.setColumnAlignment("Year", Align.RIGHT);
        t.setSelectable(true);
        
        CssLayout panel2 = createPanel(t);         
        panel2.addStyleName("notes");
        row.addComponent(panel2);

        //row.addComponent(createPanel(new TopSixTheatersChart()));
    }


    @Override
    public void onShow(Map<String, Object> params) {
        
    }

    @Override
    public void onHide() {
        removeAllComponents();
    }

}
