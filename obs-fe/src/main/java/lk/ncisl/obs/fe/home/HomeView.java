/**
 * DISCLAIMER
 *
 * The quality of the code is such that you should not copy any of it as best
 * practice how to build Vaadin applications.
 *
 * @author jouni@vaadin.com
 *
 */
package lk.ncisl.obs.fe.home;

import com.vaadin.event.LayoutEvents.LayoutClickEvent;
import com.vaadin.event.LayoutEvents.LayoutClickListener;
import com.vaadin.event.ShortcutAction.KeyCode;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.server.ThemeResource;
import com.vaadin.shared.ui.MarginInfo;
import com.vaadin.shared.ui.label.ContentMode;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.Component;
import com.vaadin.ui.CssLayout;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Image;
import com.vaadin.ui.Label;
import com.vaadin.ui.Table;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;
import java.util.HashMap;
import java.util.Map;
import lk.ncisl.obs.data.domain.User;
import lk.ncisl.obs.fe.ManageableComponent;
import lk.ncisl.obs.fe.utils.StaticUtils;
import org.springframework.beans.factory.annotation.Autowired;

public class HomeView extends VerticalLayout implements View, ManageableComponent {

    Table t;

    @Autowired
    LatestOrdersPortlet latestOrdersPortlet;

    Integer bidNo = 0;

    public HomeView() {

    }

    private CssLayout createPanel(Component content) {
        CssLayout panel = new CssLayout();
        panel.addStyleName("layout-panel");
        panel.setSizeFull();

        Button configure = new Button();
        configure.addStyleName("configure");
        configure.addStyleName("icon-cog");
        configure.addStyleName("icon-only");
        configure.addStyleName("borderless");
        configure.setDescription("Configure");
        configure.addStyleName("small");
        configure.addClickListener(new ClickListener() {
            @Override
            public void buttonClick(ClickEvent event) {
                //liveBids.addNewBidToUI();
            }
        });
        panel.addComponent(configure);

        panel.addComponent(content);
        return panel;
    }

    @Override
    public void enter(ViewChangeEvent event) {
//        DataProvider dataProvider = ((DashboardUI) getUI()).dataProvider;
//        t.setContainerDataSource(dataProvider.getRevenueByTitle());

        setSizeFull();
        addStyleName("dashboard-view");

        HorizontalLayout top = new HorizontalLayout();
        top.setWidth("100%");
        top.setSpacing(true);
        top.addStyleName("toolbar");
        addComponent(top);
        final Label title = new Label("Home");
        title.setSizeUndefined();
        title.addStyleName("h1");
        top.addComponent(title);
        top.setComponentAlignment(title, Alignment.MIDDLE_LEFT);
        top.setExpandRatio(title, 1);

        Image sublogo1 = new Image(null, new ThemeResource("img/logo_sub1.png"));
        top.addComponent(sublogo1);
        top.setComponentAlignment(sublogo1, Alignment.MIDDLE_LEFT);

        Image sublogo2 = new Image(null, new ThemeResource("img/logo_sub2.png"));
        top.addComponent(sublogo2);
        top.setComponentAlignment(sublogo2, Alignment.MIDDLE_LEFT);

        Button notify = new Button("2");
        notify.setDescription("Notifications (2 unread)");
        // notify.addStyleName("borderless");
        notify.addStyleName("notifications");
        notify.addStyleName("unread");
        notify.addStyleName("icon-only");
        notify.addStyleName("icon-bell");
        notify.addClickListener(new ClickListener() {
            @Override
            public void buttonClick(ClickEvent event) {
                //((DashboardUI) getUI()).clearDashboardButtonBadge();
                event.getButton().removeStyleName("unread");
                event.getButton().setDescription("Notifications");

                if (notifications != null && notifications.getUI() != null) {
                    notifications.close();
                } else {
                    buildNotifications(event);
                    getUI().addWindow(notifications);
                    notifications.focus();
                    ((CssLayout) getUI().getContent())
                            .addLayoutClickListener(new LayoutClickListener() {
                                @Override
                                public void layoutClick(LayoutClickEvent event) {
                                    notifications.close();
                                    ((CssLayout) getUI().getContent())
                                    .removeLayoutClickListener(this);
                                }
                            });
                }

            }
        });
        top.addComponent(notify);
        top.setComponentAlignment(notify, Alignment.MIDDLE_LEFT);

        Button edit = new Button();
        edit.addStyleName("icon-edit");
        edit.addStyleName("icon-only");
        top.addComponent(edit);
        edit.setDescription("Edit Dashboard");
        edit.addClickListener(new ClickListener() {
            @Override
            public void buttonClick(ClickEvent event) {
                final Window w = new Window("Edit Dashboard");

                w.setModal(true);
                w.setClosable(false);
                w.setResizable(false);
                w.addStyleName("edit-dashboard");

                getUI().addWindow(w);

                w.setContent(new VerticalLayout() {
                    TextField name = new TextField("Dashboard Name");

                    {
                        addComponent(new FormLayout() {
                            {
                                setSizeUndefined();
                                setMargin(true);
                                name.setValue(title.getValue());
                                addComponent(name);
                                name.focus();
                                name.selectAll();
                            }
                        });

                        addComponent(new HorizontalLayout() {
                            {
                                setMargin(true);
                                setSpacing(true);
                                addStyleName("footer");
                                setWidth("100%");

                                Button cancel = new Button("Cancel");
                                cancel.addClickListener(new ClickListener() {
                                    @Override
                                    public void buttonClick(ClickEvent event) {
                                        w.close();
                                    }
                                });
                                cancel.setClickShortcut(KeyCode.ESCAPE, null);
                                addComponent(cancel);
                                setExpandRatio(cancel, 1);
                                setComponentAlignment(cancel,
                                        Alignment.TOP_RIGHT);

                                Button ok = new Button("Save");
                                ok.addStyleName("wide");
                                ok.addStyleName("default");
                                ok.addClickListener(new ClickListener() {
                                    @Override
                                    public void buttonClick(ClickEvent event) {
                                        title.setValue(name.getValue());
                                        w.close();
                                    }
                                });
                                ok.setClickShortcut(KeyCode.ENTER, null);
                                addComponent(ok);
                            }
                        });

                    }
                });

            }
        });
        top.setComponentAlignment(edit, Alignment.MIDDLE_LEFT);

        HorizontalLayout row = new HorizontalLayout();
        row.setSizeFull();
        row.setMargin(new MarginInfo(true, true, false, true));
        row.setSpacing(true);
        addComponent(row);
        setExpandRatio(row, 1.5f);

        Label welcomeText = new Label();
        welcomeText.setCaption("Welcome to NICSL - Bidding System");
        welcomeText.setValue("<div class='padding10' align='justify'>National  Cancer Institute, Maharagama, is the premier tertiary referral hospital  dedicated to the diagnosis and follow up treatment of cancer patients in Sri Lanka,  which is under the control of Department of Health, Provides all its services  free of charge.<br>\n"
                + "                              <br>\n"
                + "                        The  NCI offers a full range of modern diagnostic facilities necessary for the  identification and confirmation of all kinds of cancers. Specialized  therapeutic Care,  such as Surgeries, Chemotherapy, and Radiotherapy, are provided on the  supervision of the Consultant oncologist...</div>");
        welcomeText.setContentMode(ContentMode.HTML);

        CssLayout panel = createPanel(welcomeText);
        panel.addStyleName("notes");
        row.addComponent(panel);

        row = new HorizontalLayout();
        row.setMargin(true);
        row.setSizeFull();
        row.setSpacing(true);
        addComponent(row);
        setExpandRatio(row, 2);

        /* Create the table with a caption. */
        latestOrdersPortlet.setCaption("Latest Orders");
        Map<String, Object> params = new HashMap<String, Object>(); 
        CssLayout panel2 = createPanel(latestOrdersPortlet);
        row.addComponent(panel2);
        latestOrdersPortlet.onShow(params);
        
    }

    Window notifications;

    private void buildNotifications(ClickEvent event) {
        notifications = new Window("Notifications");
        VerticalLayout l = new VerticalLayout();
        l.setMargin(true);
        l.setSpacing(true);
        notifications.setContent(l);
        notifications.setWidth("300px");
        notifications.addStyleName("notifications");
        notifications.setClosable(false);
        notifications.setResizable(false);
        notifications.setDraggable(false);
        notifications.setPositionX(event.getClientX() - event.getRelativeX());
        notifications.setPositionY(event.getClientY() - event.getRelativeY());
        notifications.setCloseShortcut(KeyCode.ESCAPE, null);

//        Label label = new Label(
//                "<hr><b>"
//                        + Generator.randomFirstName()
//                        + " "
//                        + Generator.randomLastName()
//                        + " created a new report</b><br><span>25 minutes ago</span><br>"
//                        + Generator.randomText(18), ContentMode.HTML);
//        l.addComponent(label);
//
//        label = new Label("<hr><b>" + Generator.randomFirstName() + " "
//                + Generator.randomLastName()
//                + " changed the schedule</b><br><span>2 days ago</span><br>"
//                + Generator.randomText(10), ContentMode.HTML);
//        l.addComponent(label);
    }

    @Override
    public void onShow(Map<String, Object> params) {

    }

    @Override
    public void onHide() {
        removeAllComponents();
    }

    public LatestOrdersPortlet getLatestOrdersPortlet() {
        return latestOrdersPortlet;
    }

    public void setLatestOrdersPortlet(LatestOrdersPortlet latestOrdersPortlet) {
        this.latestOrdersPortlet = latestOrdersPortlet;
    }

}
