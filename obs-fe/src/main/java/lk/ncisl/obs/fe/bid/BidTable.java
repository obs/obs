package lk.ncisl.obs.fe.bid;

import com.vaadin.data.Container;
import com.vaadin.ui.Button;
import com.vaadin.ui.Label;
import com.vaadin.ui.Notification;
import com.vaadin.ui.VerticalLayout;
import java.util.Iterator;
import java.util.Map;
import lk.ncisl.obs.data.domain.Bid;
import lk.ncisl.obs.data.domain.OrderRequest;
import lk.ncisl.obs.data.domain.OrderState;
import lk.ncisl.obs.data.domain.User;
import lk.ncisl.obs.data.services.BiddingService;
import lk.ncisl.obs.data.services.exceptions.InvalidBidStateTransitionException;
import lk.ncisl.obs.data.services.exceptions.InvalidOrderStateForTransactionException;
import lk.ncisl.obs.fe.ManageableComponent;
import lk.ncisl.obs.fe.user.UserContainer;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author vimukthi
 */
public class BidTable extends VerticalLayout implements ManageableComponent, Container.ItemSetChangeListener {

    private final static org.slf4j.Logger logger = LoggerFactory.getLogger(BidTable.class);
    @Autowired
    private BiddingService biddingService;
    @Autowired
    private BidContainer bids;
    @Autowired
    private UserContainer users;
    private Boolean allBidList = false;
    private OrderRequest order;

    @Override
    public void onShow(Map<String, Object> params) {
        allBidList = params.containsKey("allBidList");
        if (!allBidList) {
            order = (OrderRequest) params.get("order");
        }
        setSizeFull();
        bids.onShow(params);
        refreshBidwidgets();

        Label placeHolder = new Label("");
        addComponent(placeHolder);
        setExpandRatio(placeHolder, 1.0f);

        bids.addItemSetChangeListener(this);
    }

    @Override
    public void containerItemSetChange(Container.ItemSetChangeEvent event) {
        refreshBidwidgets();
    }

    private void refreshBidwidgets() {
        removeAllComponents();
        if (allBidList) {
            for (Iterator i = bids.getItemIds().iterator(); i.hasNext();) {
                int iid = (Integer) i.next();
                Bid bid = bids.getItem(iid).getEntity();
                Boolean currentUserIsBidder = bid.getBidder().equals(users.getCurrentUser());
                Boolean currentUserIsAdmin = users.getCurrentUser().isAdmin();
                if (// Deciding on whether to show the bid or not to the current user
                        currentUserIsBidder || currentUserIsAdmin) {
                    BidWidget bidWidget = new BidWidget(bid, users.getCurrentUser());
                    addComponent(bidWidget);
                    addEventHandlers(bidWidget);
                }
            }
        } else {
            // this comes from a order detail view
            Boolean currentUserIsaBiddingUserForOrder = isBiddingUserForOrder(order, users.getCurrentUser());
            for (Bid bid : order.getBidList()) {
                Boolean currentUserIsBidder = bid.getBidder().equals(users.getCurrentUser());
                Boolean currentUserIsAdmin = users.getCurrentUser().isAdmin();
                Boolean isOrderLessThanPhase2 = bid.getOrderRequest().isInState(OrderState.ValidStates.PHASE1)
                        || bid.getOrderRequest().isInState(OrderState.ValidStates.PHASE1_EXPIRED)
                        || bid.getOrderRequest().isInState(OrderState.ValidStates.CANCELLED);

                if (// Deciding on whether to show the bid or not to the current user
                        ((currentUserIsBidder || currentUserIsAdmin) && isOrderLessThanPhase2)
                        || ((currentUserIsaBiddingUserForOrder || currentUserIsAdmin) && !isOrderLessThanPhase2)) {
                    BidWidget bidWidget = new BidWidget(bid, users.getCurrentUser());
                    bidWidget.hideOrderCode();
                    addComponent(bidWidget);
                    addEventHandlers(bidWidget);
                }
            }
        }
    }

    private void addEventHandlers(final BidWidget widget) {
        if (widget.getAmendButton() != null) {
            widget.getAmendButton().addClickListener(new Button.ClickListener() {
                @Override
                public void buttonClick(Button.ClickEvent event) {
                    Notification.show("Not implemented");
                }
            });
        }
        if (widget.getCancelButton() != null) {
            widget.getCancelButton().addClickListener(new Button.ClickListener() {
                @Override
                public void buttonClick(Button.ClickEvent event) {
                    try {
                        if (users.getCurrentUser().isAdmin()) {
                            biddingService.adminCancel(widget.getBid());
                            Notification.show("Cancelled bid successfully");
                        } else {
                            biddingService.cancel(widget.getBid());
                            Notification.show("Cancelled bid successfully");
                        }
                        bids.refresh();
                    } catch (InvalidBidStateTransitionException ex) {
                        logger.error("", ex);
                    } catch (InvalidOrderStateForTransactionException ex) {
                        logger.error("", ex);
                    }
                }
            });
        }
        if (widget.getAcceptButton() != null) {
            widget.getAcceptButton().addClickListener(new Button.ClickListener() {
                @Override
                public void buttonClick(Button.ClickEvent event) {
                    try {
                        if (users.getCurrentUser().isAdmin()) {
                            biddingService.accept(widget.getBid());
                            Notification.show("Accepted bid successfully");
                            bids.refresh();
                        } else {
                            logger.info("Bid caught while trying to be accepted as another user.");
                        }
                    } catch (InvalidBidStateTransitionException ex) {
                        logger.error("", ex);
                    } catch (InvalidOrderStateForTransactionException ex) {
                        logger.error("", ex);
                    }
                }
            });
        }
        if (widget.getDetailsButton() != null) {
            widget.getDetailsButton().addClickListener(new Button.ClickListener() {
                @Override
                public void buttonClick(Button.ClickEvent event) {
                    Notification.show("Not implemented");
                }
            });
        }
        if (widget.getHoldButton() != null) {
            widget.getHoldButton().addClickListener(new Button.ClickListener() {
                @Override
                public void buttonClick(Button.ClickEvent event) {
                    try {
                        if (users.getCurrentUser().isAdmin()) {
                            biddingService.hold(widget.getBid());
                            Notification.show("Bid put on hold successfully");
                            bids.refresh();
                        } else {
                            logger.info("Bid caught while trying to be hold as another user.");
                        }
                    } catch (InvalidBidStateTransitionException ex) {
                        logger.error("", ex);
                    } catch (InvalidOrderStateForTransactionException ex) {
                        logger.error("", ex);
                    }
                }
            });
        }
        if (widget.getRejectButton() != null) {
            widget.getRejectButton().addClickListener(new Button.ClickListener() {
                @Override
                public void buttonClick(Button.ClickEvent event) {
                    try {
                        if (users.getCurrentUser().isAdmin()) {
                            biddingService.reject(widget.getBid());
                            Notification.show("Bid rejected successfully");
                            bids.refresh();
                        } else {
                            logger.info("Bid caught while trying to be rejected as another user.");
                        }
                    } catch (InvalidBidStateTransitionException ex) {
                        logger.error("", ex);
                    } catch (InvalidOrderStateForTransactionException ex) {
                        logger.error("", ex);
                    }
                }
            });
        }
        if (widget.getUnholdButton() != null) {
            widget.getUnholdButton().addClickListener(new Button.ClickListener() {
                @Override
                public void buttonClick(Button.ClickEvent event) {
                    try {
                        if (users.getCurrentUser().isAdmin()) {
                            biddingService.unhold(widget.getBid());
                            Notification.show("Bid put off hold successfully");
                            bids.refresh();
                        } else {
                            logger.info("Bid caught while trying to be unhold as another user.");
                        }
                    } catch (InvalidBidStateTransitionException ex) {
                        logger.error("", ex);
                    } catch (InvalidOrderStateForTransactionException ex) {
                        logger.error("", ex);
                    }
                }
            });
        }
    }

    private boolean isBiddingUserForOrder(OrderRequest order, User user) {
        for (Bid bid : order.getBidList()) {
            if (bid.getBidder().equals(user)) {
                return true;
            }
        }
        return false;
    }

    @Override
    public void onHide() {
        removeAllComponents();
    }

    public BidContainer getContainer() {
        return bids;
    }

    public void setContainer(BidContainer container) {
        this.bids = container;
    }
}
