
package lk.ncisl.obs.fe.system;

/**
 *
 * @author vimukthi
 */
public interface ManagerTask extends Runnable {
    
    public void stop();
    
    public boolean isRunning();
    
    public void setManager(Manager manager);
    
    public void alert();
    
}
