package lk.ncisl.obs.fe.order;

import lk.ncisl.obs.fe.system.ExpiryTimeCounter;
import com.vaadin.addon.jpacontainer.EntityItem;
import com.vaadin.addon.jpacontainer.JPAContainerItem;
import com.vaadin.data.Container;
import com.vaadin.data.Container.Filter;
import com.vaadin.data.Property;
import com.vaadin.data.util.filter.Compare;
import com.vaadin.data.util.filter.Or;
import com.vaadin.data.util.filter.SimpleStringFilter;
import com.vaadin.event.FieldEvents.TextChangeEvent;
import com.vaadin.event.FieldEvents.TextChangeListener;
import com.vaadin.event.LayoutEvents.LayoutClickEvent;
import com.vaadin.event.LayoutEvents.LayoutClickListener;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.shared.MouseEventDetails.MouseButton;
import com.vaadin.ui.Button;
import com.vaadin.ui.CssLayout;
import com.vaadin.ui.Notification;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import lk.ncisl.obs.data.domain.OrderRequest;
import lk.ncisl.obs.data.domain.OrderState;
import lk.ncisl.obs.data.services.OrderRequestService;
import lk.ncisl.obs.data.services.exceptions.InvalidOrderStateTransitionException;
import lk.ncisl.obs.fe.ConfirmWindow;
import lk.ncisl.obs.fe.InvariantConfigs;

import lk.ncisl.obs.fe.ManageableComponent;
import lk.ncisl.obs.fe.bid.BidForm;
import lk.ncisl.obs.fe.bid.BidTable;
import lk.ncisl.obs.fe.system.Manager;
import lk.ncisl.obs.fe.user.UserContainer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author vimukthi
 */
public class OrderListView extends VerticalLayout implements
        View, ManageableComponent, TextChangeListener, Container.ItemSetChangeListener {

    // sorting input options
    public final static String SORT_BY_CODE_DESC = "Code Descending";
    public final static String SORT_BY_CODE_ASC = "Code Ascending";
    public final static String SORT_BY_EXPIRY_DATE_DESC = "Last to expire first";
    public final static String SORT_BY_EXPIRY_DATE_ASC = "First to expire first";
    // logger
    private final static Logger logger = LoggerFactory.getLogger(OrderListView.class);
    @Autowired
    private Manager systemManager;
    @Autowired
    private OrderRequestService orderService;
    @Autowired
    private OrderContainer orders;
    @Autowired
    private OrderStateContainer orderStates;
    @Autowired
    private UserContainer users;
    @Autowired
    private BidTable bidtable;
    @Autowired
    private OrderListToolbar toolbar;
    @Autowired
    private OrderForm orderForm;
    @Autowired
    private BidForm bidForm;
    @Autowired
    private OrderDetailWidget orderDetails;
    private CssLayout orderCatalog;
    private ExpiryTimeCounter updateTask;
    // keep track of orderrequests and their widgets for filtering and reference
    private final Map<OrderRequest, OrderWidget> orderWidgets = new HashMap<OrderRequest, OrderWidget>();
    private String uploadLocation;
    // filters
    private Filter stateFilter;
    private Filter textFilter;

    @Override
    public void enter(ViewChangeListener.ViewChangeEvent event) {
        createView(null);
    }

    @Override
    public void onShow(Map<String, Object> params) {
        // enable push only when needed
        createView(params);
    }

    private void createView(Map<String, Object> params) {
        setSizeFull();
        setMargin(true);
        addComponent(toolbar);
        toolbar.onShow(params);
        toolbar.getFilter().addTextChangeListener(this);

        // container for order widgets
        orderCatalog = new CssLayout();
        orderCatalog.setSizeFull();
        orderCatalog.setStyleName("obs-order-catalog");
        addComponent(orderCatalog);

        // init the order container and order state container
        orders.onShow(params);
        orderStates.onShow(params);
        // sort so that the order expiring earliest are showed first
        orders.sort(new String[]{"expirationDate"}, new boolean[]{true});
        // filter the container to show phase 1 orders by default
        OrderState defaultState = orderStates.getOrderStateByName(OrderState.ValidStates.PHASE1);
        removeStateFilter();
        stateFilter = new Compare.Equal("state", defaultState);
        orders.addContainerFilter(stateFilter);
        orders.addItemSetChangeListener(this);

        // start the counter thread
        updateTask = new ExpiryTimeCounter(users.getCurrentUser().getTimeZone());
        systemManager.submitUiTask(UI.getCurrent(), updateTask);
        for (Iterator i = orders.getItemIds().iterator(); i.hasNext();) {
            // Get the current item identifier, which is an integer.
            int iid = (Integer) i.next();
            // Now get the actual item from the container.
            EntityItem<OrderRequest> item = orders.getItem(iid);
            addOrderWidget(item.getEntity());
        }
        setExpandRatio(orderCatalog, 1.0f);

        // handle add order action
        if (toolbar.getAddOrderButton() != null) {
            toolbar.getAddOrderButton().addClickListener(new Button.ClickListener() {
                @Override
                public void buttonClick(Button.ClickEvent event) {
                    // Create a sub-window and set the content
                    Window subWindow = new Window("New Order");
                    subWindow.setModal(true);
                    subWindow.setWidth("80%");
                    subWindow.setHeight("80%");
                    subWindow.setContent(orderForm);
                    Map<String, Object> params = new HashMap<String, Object>();
                    params.put("new", "new");
                    params.put("window", subWindow);
                    orderForm.onShow(params);
                    // Center it in the browser window
                    subWindow.center();
                    subWindow.addCloseListener(new Window.CloseListener() {
                        @Override
                        public void windowClose(Window.CloseEvent e) {
                            orderForm.onHide();
                            orders.refresh();
                        }
                    });

                    UI.getCurrent().addWindow(subWindow);

                }
            });
        }

        toolbar.getSorter().addValueChangeListener(new Property.ValueChangeListener() {
            @Override
            public void valueChange(Property.ValueChangeEvent event) {
                String sortType = (String) toolbar.getSorter().getValue();
                if (sortType.equals(SORT_BY_CODE_ASC)) {
                    orders.sort(new String[]{"code"}, new boolean[]{true});
                } else if (sortType.equals(SORT_BY_CODE_ASC)) {
                    orders.sort(new String[]{"code"}, new boolean[]{false});
                } else if (sortType.equals(SORT_BY_EXPIRY_DATE_ASC)) {
                    orders.sort(new String[]{"expirationDate"}, new boolean[]{true});
                } else {
                    orders.sort(new String[]{"expirationDate"}, new boolean[]{false});
                }
            }
        });

        toolbar.getStateFilter().addValueChangeListener(new Property.ValueChangeListener() {
            @Override
            public void valueChange(Property.ValueChangeEvent event) {
                JPAContainerItem<OrderState> stateItem = (JPAContainerItem<OrderState>) toolbar.getStateFilter().
                        getContainerDataSource().getItem(toolbar.getStateFilter().getValue());
                OrderState state = stateItem.getEntity();
                removeStateFilter();
                stateFilter = new Compare.Equal("state", state);
                orders.addContainerFilter(stateFilter);
                orders.refresh();                
            }
        });


    }

    @Override
    public void containerItemSetChange(Container.ItemSetChangeEvent event) {
        updateTask.removeAllUpdatables(); // stop the thread from updating the components
        orderCatalog.removeAllComponents(); // remove all previous widgets
        for (Iterator i = orders.getItemIds().iterator(); i.hasNext();) {
            // Get the current item identifier, which is an integer.
            int iid = (Integer) i.next();
            // Now get the actual item from the container.
            EntityItem<OrderRequest> item = orders.getItem(iid);
            addOrderWidget(item.getEntity());
        }
    }

    /**
     * create and add an orderwidget given the OrderRequest
     *
     * @param item
     */
    private void addOrderWidget(final OrderRequest order) {
        OrderWidget orderDisplay = new OrderWidget(order,
                uploadLocation + InvariantConfigs.ORDERS_IMAGES_PATH, users.getCurrentUser(), orders);
        orderWidgets.put(order, orderDisplay);
        // online show time to expire if the orders are active in phase 1 or phase 2
        if (order.getState().getName().equals("phase1") || order.getState().getName().equals("phase2")) {
            updateTask.addUpdatable(orderDisplay.getClockDisplay(), order.getExpirationDate(), order.getTimeZone());
        }
        // open details view on click
        orderDisplay.addLayoutClickListener(new LayoutClickListener() {
            @Override
            public void layoutClick(LayoutClickEvent event) {
                if (event.getButton() == MouseButton.LEFT) {
                    //OrderRequest order = ((OrderWidget) event.getComponent()).getOrder();

                    // Create a sub-window and set the content
                    Window subWindow = new Window("Order Request : " + order.getCode());
                    subWindow.setModal(true);
                    subWindow.setWidth("80%");
                    subWindow.setHeight("80%");
                    subWindow.setContent(orderDetails);
                    Map<String, Object> params = new HashMap<String, Object>();
                    params.put("order", order);
                    params.put("imagesPath", uploadLocation + InvariantConfigs.ORDERS_IMAGES_PATH);
                    orderDetails.onShow(params);
                    // Center it in the browser window
                    subWindow.center();
                    UI.getCurrent().addWindow(subWindow);
                    subWindow.addCloseListener(new Window.CloseListener() {
                        @Override
                        public void windowClose(Window.CloseEvent e) {
                            orderDetails.onHide();
                        }
                    });

                }
            }
        });
        // handle cancel
        if (orderDisplay.getCancelButton() != null) {
            orderDisplay.getCancelButton().addClickListener(new Button.ClickListener() {
                @Override
                public void buttonClick(Button.ClickEvent event) {
                    ConfirmWindow confirm = new ConfirmWindow("Confirm order cancellation",
                            "You are about to cancel an order after which the order will not be active in the system and can not be restored."
                            + " All existing bids for the order will also be cancelled. <br/>Are you sure?",
                            new ConfirmWindow.Decision() {
                        @Override
                        public void confirmed() {
                            try {
                                orderService.cancel(order);
                            } catch (InvalidOrderStateTransitionException ex) {
                                logger.error("Error occured", ex);
                            }
                            // refresh container
                            orders.refresh();
                        }

                        @Override
                        public void refused() {
                        }
                    });
                    UI.getCurrent().addWindow(confirm);
                }
            });
        }

        // handle edit
        if (orderDisplay.getEditButton() != null) {
            orderDisplay.getEditButton().addClickListener(new Button.ClickListener() {
                @Override
                public void buttonClick(Button.ClickEvent event) {
                    // Create a sub-window and set the content
                    Window subWindow = new Window("Edit Order");
                    subWindow.setModal(true);
                    subWindow.setWidth("80%");
                    subWindow.setHeight("80%");
                    subWindow.setContent(orderForm);
                    Map<String, Object> params = new HashMap<String, Object>();
                    params.put("window", subWindow);
                    params.put("order", order);
                    orderForm.onShow(params);
                    // Center it in the browser window
                    subWindow.center();
                    subWindow.addCloseListener(new Window.CloseListener() {
                        @Override
                        public void windowClose(Window.CloseEvent e) {
                            orderForm.onHide();
                            orders.refresh();
                        }
                    });

                    UI.getCurrent().addWindow(subWindow);
                }
            });
        }

        // open bid form
        if (orderDisplay.getBidButton() != null) {
            orderDisplay.getBidButton().addClickListener(new Button.ClickListener() {
                @Override
                public void buttonClick(Button.ClickEvent event) {
                    // Create a sub-window and set the content
                    Window subWindow = new Window("New Bid");
                    subWindow.setModal(true);
                    subWindow.setWidth("80%");
                    subWindow.setHeight("80%");
                    subWindow.setContent(bidForm);
                    Map<String, Object> params = new HashMap<String, Object>();
                    params.put("new", "new");
                    params.put("window", subWindow);
                    params.put("order", order);
                    bidForm.onShow(params);
                    // Center it in the browser window
                    subWindow.center();
                    subWindow.addCloseListener(new Window.CloseListener() {
                        @Override
                        public void windowClose(Window.CloseEvent e) {
                            bidForm.onHide();
                            orders.refresh();
                        }
                    });

                    UI.getCurrent().addWindow(subWindow);
                }
            });
        }

        // start phase 2 for phase 1 expired orders
        if (orderDisplay.getPhase2Button() != null) {
            orderDisplay.getPhase2Button().addClickListener(new Button.ClickListener() {
                @Override
                public void buttonClick(Button.ClickEvent event) {
                    if (order.hasPhase1AcceptedBids()) {
                        // Create a sub-window and set the content
                        Window subWindow = new Window("Start Phase 2");
                        subWindow.setModal(true);
                        subWindow.setWidth("80%");
                        subWindow.setHeight("80%");
                        subWindow.setContent(orderForm);
                        Map<String, Object> params = new HashMap<String, Object>();
                        params.put("window", subWindow);
                        params.put("order", order);
                        params.put("phase2", "phase2");
                        orderForm.onShow(params);
                        // Center it in the browser window
                        subWindow.center();
                        subWindow.addCloseListener(new Window.CloseListener() {
                            @Override
                            public void windowClose(Window.CloseEvent e) {
                                orderForm.onHide();
                                orders.refresh();
                            }
                        });

                        UI.getCurrent().addWindow(subWindow);
                    } else {
                        Notification.show("Please accept some phase 1 bids for the order before proceeding to phase 2", Notification.Type.ERROR_MESSAGE);
                    }
                }
            });
        }

        // mark fullfilled orders
        if (orderDisplay.getFullFilledButton() != null) {
            orderDisplay.getFullFilledButton().addClickListener(new Button.ClickListener() {
                @Override
                public void buttonClick(Button.ClickEvent event) {
                    if (order.hasPhase2AcceptedBids()) {
                        try {
                            orderService.fullfilled(order);
                        } catch (InvalidOrderStateTransitionException ex) {
                            logger.error("Error occured", ex);
                        }
                        orders.refresh();
                    } else {
                        Notification.show("An order can not be marked as fullfilled without accepted orders", Notification.Type.ERROR_MESSAGE);
                    }
                }
            });
        }

        // mark unsatisfied orders
        if (orderDisplay.getUnsatisfiedButton() != null) {
            orderDisplay.getUnsatisfiedButton().addClickListener(new Button.ClickListener() {
                @Override
                public void buttonClick(Button.ClickEvent event) {
                    if (order.hasPhase2AcceptedBids()) {
                        Notification.show("The order has accepted bids, can not be marked as unsatisfied", Notification.Type.ERROR_MESSAGE);
                    } else {
                        try {
                            orderService.unsatisfied(order);
                        } catch (InvalidOrderStateTransitionException ex) {
                            logger.error("Error occured", ex);
                        }
                        orders.refresh();
                    }
                }
            });
        }

        orderCatalog.addComponent(orderDisplay);
    }

    @Override
    public void onHide() {
        if (updateTask != null) {
            systemManager.stopUiTask(UI.getCurrent(), updateTask);
            updateTask = null;
        }
        if (toolbar.getFilter() != null) {
            toolbar.getFilter().removeTextChangeListener(this);
        }
        orders.removeItemSetChangeListener(this);
        toolbar.onHide();
        removeAllComponents();
    }
    
    private void removeStateFilter(){
        if(stateFilter != null) 
            orders.removeContainerFilter(stateFilter);
    }

    @Override
    public void textChange(TextChangeEvent event) {
        String text = event.getText();
        if (text.length() >= 3 || text.length() == 0) {
            if (textFilter != null) {
                orders.removeContainerFilter(textFilter);
            }
            textFilter = new Or(
                    new SimpleStringFilter("code", text, true, true),
                    new SimpleStringFilter("summary", text, true, true));
            orders.addContainerFilter(textFilter);
        }
    }

    public OrderContainer getContainer() {
        return orders;
    }

    public void setContainer(OrderContainer container) {
        this.orders = container;
    }

    public BidTable getBidtable() {
        return bidtable;
    }

    public void setBidtable(BidTable bidtable) {
        this.bidtable = bidtable;
    }

    public String getUploadLocation() {
        return uploadLocation;
    }

    public void setUploadLocation(String uploadLocation) {
        this.uploadLocation = uploadLocation;
    }
}
