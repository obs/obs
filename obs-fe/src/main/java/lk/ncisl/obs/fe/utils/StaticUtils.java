package lk.ncisl.obs.fe.utils;

public class StaticUtils {

    public static String lableCaptionWrap(String labelIn) {
        return "<span class='label-caption'>" + labelIn + "</span>";
    }
    
    public static String createdByGeoclipseHtml() {
        return "<p class='geo-devtext'>Creation by <a href='#'>Geoclipse</a> <span class='geo-copyright'>&copy; 2014</span></p>";
    }
}
