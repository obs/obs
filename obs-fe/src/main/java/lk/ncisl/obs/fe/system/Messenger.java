package lk.ncisl.obs.fe.system;

import lk.ncisl.obs.data.domain.Bid;
import lk.ncisl.obs.data.domain.Message;
import lk.ncisl.obs.data.domain.OrderRequest;
import lk.ncisl.obs.data.domain.User;
import lk.ncisl.obs.data.services.BidStateObserver;
import lk.ncisl.obs.data.services.MessagingService;
import lk.ncisl.obs.data.services.OrderStateObserver;
import lk.ncisl.obs.data.services.UserService;
import lk.ncisl.obs.fe.utils.MessageTemplates;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * This class must be used for all messaging purposes as it allows for updating the UI when required.
 * @author vimukthi
 */
public class Messenger implements ManagerTask, OrderStateObserver, BidStateObserver {

    private final static Logger logger = LoggerFactory.getLogger(Messenger.class);

    @Autowired
    private MessagingService messagingService;

    @Autowired
    private UserService userService;
    private boolean run;
    private Manager systemManager;

    @Override
    public void stateChanged(Bid bid) {
        logger.debug("======================== Called on bid state change with bidder " + bid.getBidder().getEmail() + " ====================");
        String title = "One of your bids changed state";
        messagingService.sendMessage(
                title,
                MessageTemplates.bidStateChangeMessageForBidder(bid),
                userService.getSystemUser(),
                bid.getBidder()
        );
    }

    @Override
    public void stateChanged(OrderRequest order) {
        logger.debug("======================== Called on order state change ====================");
        String title = "An order changed state";
        for (User user : userService.getAllUsers()){
            messagingService.sendMessage(
                    title,
                    MessageTemplates.orderStateChangeMessage(order),
                    userService.getSystemUser(),
                    user
            );
        }
    }

    public void add(Message message){
        messagingService.add(message);
    }

    public void sendMessage(String title, String body, User sender, User receiver){
        messagingService.sendMessage(title, body, sender, receiver);
    }

    @Override
    public void stop() {
        this.run = false;
    }

    @Override
    public boolean isRunning() {
        return run;
    }

    @Override
    public void setManager(Manager manager) {
        this.systemManager = manager;
    }

    @Override
    public void alert() {

    }

    @Override
    public void run() {
        while(run){
            try {
                Thread.sleep(5000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
