package lk.ncisl.obs.fe.preferences;

import com.vaadin.addon.jpacontainer.EntityItem;
import com.vaadin.event.ItemClickEvent;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.ui.Button;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;
import lk.ncisl.obs.data.domain.User;
import lk.ncisl.obs.fe.ManageableComponent;
import lk.ncisl.obs.fe.user.UserAdminToolbar;
import lk.ncisl.obs.fe.user.UserContainer;
import lk.ncisl.obs.fe.user.UserForm;
import lk.ncisl.obs.fe.user.UserTable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import javax.persistence.EntityTransaction;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author vimukthi
 */
public class PreferencesView extends VerticalLayout implements View, ManageableComponent {

    private final static Logger logger = LoggerFactory.getLogger(PreferencesView.class);
    @Autowired
    private PreferencesToolbar toolbar;
    @Autowired
    private UserForm userForm;
    private User selectedUser;   

    @Override
    public void enter(ViewChangeListener.ViewChangeEvent event) {
        setSizeFull();
        addStyleName("transactions");

        addComponent(toolbar);
        toolbar.onShow(null);

        // handle user add action
        toolbar.getResetPasswdButton().addClickListener(new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent event) {
                // Create a sub-window and set the content
                Window subWindow = new Window("New User");
                subWindow.setModal(true);
                subWindow.setWidth("80%");
                subWindow.setContent(userForm);
                Map<String, Object> params = new HashMap<String, Object>();
                params.put("new", "new");
                params.put("window", subWindow);
                userForm.onShow(params);
                // Center it in the browser window
                subWindow.center();
                subWindow.addCloseListener(new Window.CloseListener() {
                    @Override
                    public void windowClose(Window.CloseEvent e) {
                        userForm.onHide();
                    }
                });

                UI.getCurrent().addWindow(subWindow);

            }
        });
    }

    @Override
    public void onShow(Map<String, Object> params) {
//        setMargin(true);
//        addComponent(table);
//        table.onShow(params);
    }

    @Override
    public void onHide() {
        toolbar.onHide();
        removeAllComponents();
    }
}
