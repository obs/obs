package lk.ncisl.obs.fe.order;

import com.vaadin.addon.jpacontainer.JPAContainer;
import com.vaadin.addon.jpacontainer.provider.CachingMutableLocalEntityProvider;
import java.util.List;
import java.util.Map;
import javax.persistence.EntityManager;
import lk.ncisl.obs.data.domain.OrderRequest;
import lk.ncisl.obs.data.domain.OrderState;
import lk.ncisl.obs.fe.ManageableComponent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;

/**
 *
 * @author vimukthi
 */
public class OrderContainer extends JPAContainer<OrderRequest> implements ManageableComponent {    

    private EntityManager em;
    @Autowired
    private LocalContainerEntityManagerFactoryBean emf;

    public OrderContainer() {
        super(OrderRequest.class);
    }

    @Override
    public void onShow(Map<String, Object> params) {
        this.em = emf.getNativeEntityManagerFactory().createEntityManager();
        setEntityProvider(
                new CachingMutableLocalEntityProvider<OrderRequest>(
                OrderRequest.class, em));
        refresh();
    }

    @Override
    public void onHide() {
        setEntityProvider(null);
    }
    
    
    public List<OrderRequest> getLatestOrders(Integer count){
        List<OrderRequest> orderRequests = em.createNamedQuery("OrderRequest.findLatestOrders").setMaxResults(count).getResultList();        
        return orderRequests;        
    }
    

    public EntityManager getEm() {
        return em;
    }

    public void setEm(EntityManager em) {
        this.em = em;
    }

    public LocalContainerEntityManagerFactoryBean getEmf() {
        return emf;
    }

    public void setEmf(LocalContainerEntityManagerFactoryBean emf) {
        this.emf = emf;
    }
}
