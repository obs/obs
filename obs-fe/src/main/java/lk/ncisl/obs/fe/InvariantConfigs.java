package lk.ncisl.obs.fe;

/**
 * Add configs that won't change from deployment to deployment
 * @author vimukthi
 */
public interface InvariantConfigs {
    
    /**
     * uploaded images path relative to upload path
     */
    public static final String UPLOADED_IMAGE_PATH = "images/";
    
    /**
     * Uploaded order images path relative to base image path
     */
    public static final String ORDERS_IMAGES_PATH = UPLOADED_IMAGE_PATH + "orders/";
    
    public static final String ORDERLIST_IMAGE_DISPLAY_WIDTH = "265px";
    public static final String ORDERLIST_IMAGE_DISPLAY_HEIGHT = "150px";
    
    
}
