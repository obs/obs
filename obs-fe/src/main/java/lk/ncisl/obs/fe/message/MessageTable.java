package lk.ncisl.obs.fe.message;

import com.vaadin.data.Container;
import com.vaadin.data.Container.Filter;
import com.vaadin.data.util.filter.Compare;
import com.vaadin.server.VaadinServlet;
import com.vaadin.ui.Label;
import com.vaadin.ui.VerticalLayout;
import java.util.Iterator;
import java.util.Map;
import lk.ncisl.obs.data.domain.Message;
import lk.ncisl.obs.data.services.BiddingService;
import lk.ncisl.obs.fe.ManageableComponent;
import lk.ncisl.obs.fe.user.UserContainer;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

/**
 *
 * @author prabhath
 */
public class MessageTable extends VerticalLayout implements ManageableComponent, Container.ItemSetChangeListener {

    private final static org.slf4j.Logger logger = LoggerFactory.getLogger(MessageTable.class);
    @Autowired
    private BiddingService biddingService;
    @Autowired
    private MessageContainer messages;
    @Autowired
    private UserContainer users;

    private ApplicationContext applicationContext;

    @Autowired
    private MessageWidget messageWidget;
    
    @Override
    public void onShow(Map<String, Object> params) {
        /*allBidList = params.containsKey("allBidList");
         if (!allBidList) {
         order = (OrderRequest) params.get("order");
         }*/
        setSizeFull();
        messages.sort(new String[]{"dateCreated"}, new boolean[]{false});

        messages.onShow(params);
        refreshMessagewidgets();

        Label placeHolder = new Label("");
        addComponent(placeHolder);
        setExpandRatio(placeHolder, 1.0f);

        messages.addItemSetChangeListener(this);

    }

    @Override
    public void containerItemSetChange(Container.ItemSetChangeEvent event) {
        refreshMessagewidgets();
    }

    private void refreshMessagewidgets() {
        removeAllComponents();

        applicationContext = WebApplicationContextUtils.
                getWebApplicationContext(VaadinServlet.getCurrent()
                        .getServletContext());

        //if (allBidList) {
        for (Iterator i = messages.getItemIds().iterator(); i.hasNext();) {
            int iid = (Integer) i.next();
            Message message = messages.getItem(iid).getEntity();
            messageWidget = applicationContext.getBean(MessageWidget.class);

                //change view accordingly for sent messages
                messageWidget.setSentMessage( message.getSender().equals( users.getCurrentUser() ));
                
            messageWidget.initMessageWidget(message, users.getCurrentUser());
            addComponent(messageWidget);
            addEventHandlers(messageWidget);
        }
        //} else {

        //}
    }

    private void addEventHandlers(final MessageWidget widget) {

    }

    @Override
    public void onHide() {
        removeAllComponents();
    }

    public MessageContainer getContainer() {
        return messages;
    }

    public void setContainer(MessageContainer container) {
        this.messages = container;
    }

    public void applyFilters(Map<String, Object> params) {
        String selected = (String) params.get("selected");
        
        messages.removeAllContainerFilters();

        // filters
        Filter stateFilter;
        Filter userFilter;
        
        //hide nested childs
        Filter childFilter = new Compare.Equal("parent", null); 
        messages.addContainerFilter(childFilter);

        if (selected.equals("unread")) {            
            stateFilter = new Compare.Equal("state", "0");
            userFilter = new Compare.Equal("receiver", users.getCurrentUser());            
            messages.addContainerFilter(stateFilter);
            messages.addContainerFilter(userFilter);
            onShow(params);
        }

        if (selected.equals("inbox")) {
            userFilter = new Compare.Equal("receiver", users.getCurrentUser());
            messages.addContainerFilter(userFilter);
            onShow(params);
        }

        if (selected.equals("sent")) {
            userFilter = new Compare.Equal("sender", users.getCurrentUser());
            messages.addContainerFilter(userFilter);
            onShow(params);
        }
    }

    
    
    
    

}
