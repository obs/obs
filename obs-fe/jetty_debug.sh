export MAVEN_OPTS="-Xms256m -Xmx512m -XX:PermSize=128m -Xdebug -javaagent:lib/spring-instrument-4.0.1.RELEASE.jar -Xnoagent -Djava.compiler=NONE -Xrunjdwp:transport=dt_socket,address=4000,server=y,suspend=n"
mvn package jetty:run
