# --------------------------------------------------------
# Host:                         localhost
# Server version:               5.5.8
# Server OS:                    Win32
# HeidiSQL version:             6.0.0.3603
# Date/time:                    2014-03-27 11:35:45
# --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

# Dumping structure for table medibid.announcement
DROP TABLE IF EXISTS `announcement`;
CREATE TABLE IF NOT EXISTS `announcement` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date_created` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `description` varchar(1000) DEFAULT NULL,
  `global` smallint(6) DEFAULT NULL,
  `title` varchar(50) DEFAULT NULL,
  `to_user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_announcement_to_user_id` (`to_user_id`),
  CONSTRAINT `FK_announcement_to_user_id` FOREIGN KEY (`to_user_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

# Dumping data for table medibid.announcement: ~0 rows (approximately)
/*!40000 ALTER TABLE `announcement` DISABLE KEYS */;
/*!40000 ALTER TABLE `announcement` ENABLE KEYS */;


# Dumping structure for table medibid.bid
DROP TABLE IF EXISTS `bid`;
CREATE TABLE IF NOT EXISTS `bid` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date_created` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `amends_bid_id` datetime DEFAULT NULL,
  `description` varchar(5000) DEFAULT NULL,
  `price_per_unit` float DEFAULT NULL,
  `quantity` int(11) DEFAULT NULL,
  `bidder_id` int(11) DEFAULT NULL,
  `order_id` int(11) DEFAULT NULL,
  `state_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_bid_order_id` (`order_id`),
  KEY `FK_bid_state_id` (`state_id`),
  KEY `FK_bid_bidder_id` (`bidder_id`),
  CONSTRAINT `FK_bid_bidder_id` FOREIGN KEY (`bidder_id`) REFERENCES `user` (`id`),
  CONSTRAINT `FK_bid_order_id` FOREIGN KEY (`order_id`) REFERENCES `order_request` (`id`),
  CONSTRAINT `FK_bid_state_id` FOREIGN KEY (`state_id`) REFERENCES `bid_state` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

# Dumping data for table medibid.bid: ~2 rows (approximately)
/*!40000 ALTER TABLE `bid` DISABLE KEYS */;
REPLACE INTO `bid` (`id`, `date_created`, `date_modified`, `amends_bid_id`, `description`, `price_per_unit`, `quantity`, `bidder_id`, `order_id`, `state_id`) VALUES
	(6, '2014-03-18 11:18:49', '2014-03-24 23:14:56', '0000-00-00 00:00:00', '0', 123, 21, 2, 7, 1),
	(7, '2014-03-18 11:18:49', '2014-03-24 23:14:56', '0000-00-00 00:00:00', '0', 2154, 32, 2, 7, 1);
/*!40000 ALTER TABLE `bid` ENABLE KEYS */;


# Dumping structure for table medibid.bid_state
DROP TABLE IF EXISTS `bid_state`;
CREATE TABLE IF NOT EXISTS `bid_state` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date_modified` datetime DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `display_name` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

# Dumping data for table medibid.bid_state: ~10 rows (approximately)
/*!40000 ALTER TABLE `bid_state` DISABLE KEYS */;
REPLACE INTO `bid_state` (`id`, `date_modified`, `description`, `display_name`, `name`) VALUES
	(1, '2014-03-24 12:51:56', 'bid is active for the phase 1 of the order', 'Phase 1 Active', 'phase1_active'),
	(2, '2014-03-24 12:48:29', 'bid was rejected on phase 1 of the order', 'Rejected on Phase 1', 'phase1_rejected'),
	(3, '2014-03-24 12:49:14', 'bid was accepted on phase 1 of the order', 'Accepted on Phase 1', 'phase1_accepted'),
	(4, '2014-03-24 12:47:43', 'bid is active for the phase 2 of the order', 'Phase 2 Active', 'phase2_active'),
	(5, '2014-03-24 12:47:43', 'bid was rejected on final phase of the order', 'Rejected', 'rejected'),
	(6, '2014-03-24 12:53:59', 'bid was accepted on final phase of the order', 'accepted', 'accepted'),
	(7, '2014-03-24 12:55:11', 'bid was cancelled by the bidder', 'Cancelled', 'cancelled'),
	(8, '2014-03-24 12:55:11', 'bid was cancelled by the admin', 'Admin Cancelled', 'admin_cancelled'),
	(9, '2014-03-24 12:49:14', 'bid was put onhold on phase 1 of the order', 'Put onhold on Phase 1', 'phase1_onhold'),
	(10, '2014-03-24 12:58:47', 'bid was put onhold on phase 2 of the order', 'Put onhold on Phase 2', 'phase2_onhold');
/*!40000 ALTER TABLE `bid_state` ENABLE KEYS */;


# Dumping structure for table medibid.message
DROP TABLE IF EXISTS `message`;
CREATE TABLE IF NOT EXISTS `message` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date_created` datetime DEFAULT NULL,
  `date_viewed` datetime DEFAULT NULL,
  `description` varchar(500) DEFAULT NULL,
  `parent` int(11) DEFAULT NULL,
  `title` varchar(100) DEFAULT NULL,
  `from_user_id` int(11) DEFAULT NULL,
  `to_user_id` int(11) DEFAULT NULL,
  `state` int(11) DEFAULT NULL,
  `global` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_message_from_user_id` (`from_user_id`),
  KEY `FK_message_to_user_id` (`to_user_id`),
  CONSTRAINT `FK_message_from_user_id` FOREIGN KEY (`from_user_id`) REFERENCES `user` (`id`),
  CONSTRAINT `FK_message_to_user_id` FOREIGN KEY (`to_user_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

# Dumping data for table medibid.message: ~2 rows (approximately)
/*!40000 ALTER TABLE `message` DISABLE KEYS */;
REPLACE INTO `message` (`id`, `date_created`, `date_viewed`, `description`, `parent`, `title`, `from_user_id`, `to_user_id`, `state`, `global`) VALUES
	(1, '2014-03-27 01:09:13', '2014-03-27 01:09:14', 'test desc', 1, 'test title', 1, 2, NULL, NULL),
	(2, '2014-03-27 01:09:13', '2014-03-27 01:09:14', 'test desc2', 2, 'test title2', 1, 2, NULL, NULL);
/*!40000 ALTER TABLE `message` ENABLE KEYS */;


# Dumping structure for table medibid.order_request
DROP TABLE IF EXISTS `order_request`;
CREATE TABLE IF NOT EXISTS `order_request` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(50) DEFAULT NULL,
  `date_created` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `description` varchar(5000) DEFAULT NULL,
  `expiration_date` datetime DEFAULT NULL,
  `image` varchar(500) DEFAULT NULL,
  `quantity` int(11) DEFAULT NULL,
  `summary` varchar(200) DEFAULT NULL,
  `state_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `code` (`code`),
  KEY `FK_order_request_state_id` (`state_id`),
  CONSTRAINT `FK_order_request_state_id` FOREIGN KEY (`state_id`) REFERENCES `order_state` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

# Dumping data for table medibid.order_request: ~9 rows (approximately)
/*!40000 ALTER TABLE `order_request` DISABLE KEYS */;
REPLACE INTO `order_request` (`id`, `code`, `date_created`, `date_modified`, `description`, `expiration_date`, `image`, `quantity`, `summary`, `state_id`) VALUES
	(1, 'order1', '2014-03-11 18:16:20', '2014-03-17 01:04:55', '0', '2014-03-25 18:16:16', 'image.jpg', 21, 'fefe', 4),
	(2, 'order2', '2014-03-11 18:16:20', '2014-03-16 17:40:40', '0', '2014-03-04 18:16:16', 'image.jpg', 21, 'rgeg', 2),
	(3, 'order3', '2014-03-11 18:16:20', '2014-03-15 21:47:20', '0', '2014-04-04 18:16:16', 'image.jpg', 21, 'rgeg', 1),
	(4, 'test21', '2014-03-11 18:16:20', '2014-03-17 12:59:06', '0', '2014-03-29 13:15:53', 'image.jpg', 21, 'rewre', 4),
	(5, 'blah21', '2014-03-11 18:16:20', '2014-03-17 01:20:31', '0', '2014-03-29 13:15:53', 'image.jpg', 21, 'wqvvfd', 4),
	(6, 'mnnewe2', '2014-03-11 18:16:20', '2014-03-17 11:53:40', '0', '2014-03-29 12:15:53', 'image.jpg', 323, 'lkloa', 4),
	(7, 'test121', '2014-03-16 09:16:05', '2014-03-16 09:16:06', '<i>test order</i>', '2014-03-19 00:00:00', 'Chrysanthemum.jpg', 21, 'test order', 1),
	(8, 'test214', '2014-03-16 10:53:21', '2014-03-16 10:53:21', '<u>test order</u>', '2014-03-26 00:00:00', 'Jellyfish.jpg', 2134, 'test order', 1),
	(9, 'test oreder', '2014-03-17 11:53:11', '2014-03-17 11:53:11', 'the order test', '2014-03-27 00:00:00', 'Koala.jpg', 32, 'fdgrgr', 1);
/*!40000 ALTER TABLE `order_request` ENABLE KEYS */;


# Dumping structure for table medibid.order_state
DROP TABLE IF EXISTS `order_state`;
CREATE TABLE IF NOT EXISTS `order_state` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date_modified` datetime DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `display_name` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

# Dumping data for table medibid.order_state: ~7 rows (approximately)
/*!40000 ALTER TABLE `order_state` DISABLE KEYS */;
REPLACE INTO `order_state` (`id`, `date_modified`, `description`, `display_name`, `name`) VALUES
	(1, '2014-03-16 16:44:10', 'phase1 bidding started', 'Phase 1', 'phase1'),
	(2, '2014-03-16 16:44:20', 'phase 1 bidding time expired', 'Phase 1 Expired', 'phase1_expired'),
	(3, '2014-03-16 16:44:28', 'phase 2 bidding started', 'Phase 2', 'phase2'),
	(4, '2014-03-16 16:44:34', 'bidding cancelled primaturely', 'Cancelled', 'cancelled'),
	(5, '2014-03-16 16:44:43', 'phase 2 bidding time expired', 'Phase 2 Expired', 'phase2_expired'),
	(6, '2014-03-16 16:44:52', 'order request fullfilled', 'Fullfilled', 'fullfilled'),
	(7, '2014-03-16 16:44:58', 'after phase 2 the order request is still unsatisfied', 'Unsatisfied', 'unsatisfied');
/*!40000 ALTER TABLE `order_state` ENABLE KEYS */;


# Dumping structure for table medibid.permission
DROP TABLE IF EXISTS `permission`;
CREATE TABLE IF NOT EXISTS `permission` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date_created` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `name` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

# Dumping data for table medibid.permission: ~1 rows (approximately)
/*!40000 ALTER TABLE `permission` DISABLE KEYS */;
REPLACE INTO `permission` (`id`, `date_created`, `date_modified`, `name`) VALUES
	(1, '0000-00-00 00:00:00', '2014-02-21 15:22:11', 'addbid');
/*!40000 ALTER TABLE `permission` ENABLE KEYS */;


# Dumping structure for table medibid.preference
DROP TABLE IF EXISTS `preference`;
CREATE TABLE IF NOT EXISTS `preference` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date_created` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `description` varchar(500) DEFAULT NULL,
  `name` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

# Dumping data for table medibid.preference: ~0 rows (approximately)
/*!40000 ALTER TABLE `preference` DISABLE KEYS */;
/*!40000 ALTER TABLE `preference` ENABLE KEYS */;


# Dumping structure for table medibid.role
DROP TABLE IF EXISTS `role`;
CREATE TABLE IF NOT EXISTS `role` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date_created` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `description` varchar(500) DEFAULT NULL,
  `name` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

# Dumping data for table medibid.role: ~2 rows (approximately)
/*!40000 ALTER TABLE `role` DISABLE KEYS */;
REPLACE INTO `role` (`id`, `date_created`, `date_modified`, `description`, `name`) VALUES
	(1, '2014-02-18 17:15:11', '2014-02-18 17:15:12', '0', 'admin'),
	(2, '2014-02-21 12:41:33', '2014-02-21 12:41:34', '0', 'bidder');
/*!40000 ALTER TABLE `role` ENABLE KEYS */;


# Dumping structure for table medibid.role_permission
DROP TABLE IF EXISTS `role_permission`;
CREATE TABLE IF NOT EXISTS `role_permission` (
  `permission_id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL,
  `id` int(10) DEFAULT NULL,
  `date_created` int(10) DEFAULT NULL,
  `date_modified` int(10) DEFAULT NULL,
  PRIMARY KEY (`permission_id`,`role_id`),
  KEY `FK_role_permission_role_id` (`role_id`),
  CONSTRAINT `FK_role_permission_permission_id` FOREIGN KEY (`permission_id`) REFERENCES `permission` (`id`),
  CONSTRAINT `FK_role_permission_role_id` FOREIGN KEY (`role_id`) REFERENCES `role` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

# Dumping data for table medibid.role_permission: ~1 rows (approximately)
/*!40000 ALTER TABLE `role_permission` DISABLE KEYS */;
REPLACE INTO `role_permission` (`permission_id`, `role_id`, `id`, `date_created`, `date_modified`) VALUES
	(1, 2, 1, 0, 2014);
/*!40000 ALTER TABLE `role_permission` ENABLE KEYS */;


# Dumping structure for table medibid.user
DROP TABLE IF EXISTS `user`;
CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `about_me` varchar(500) DEFAULT NULL,
  `active` int(11) DEFAULT NULL,
  `admin_comment` varchar(500) DEFAULT NULL,
  `date_created` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `email` varchar(200) DEFAULT NULL,
  `first_name` varchar(50) DEFAULT NULL,
  `last_name` varchar(50) DEFAULT NULL,
  `mobile` varchar(20) DEFAULT NULL,
  `organization` varchar(50) DEFAULT NULL,
  `password` varchar(20) DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL,
  `photo` varchar(500) DEFAULT NULL,
  `secondary_email` varchar(200) DEFAULT NULL,
  `security_question` varchar(500) DEFAULT NULL,
  `security_question_answer` varchar(500) DEFAULT NULL,
  `system` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`),
  UNIQUE KEY `phone` (`phone`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

# Dumping data for table medibid.user: ~4 rows (approximately)
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
REPLACE INTO `user` (`id`, `about_me`, `active`, `admin_comment`, `date_created`, `date_modified`, `email`, `first_name`, `last_name`, `mobile`, `organization`, `password`, `phone`, `photo`, `secondary_email`, `security_question`, `security_question_answer`, `system`) VALUES
	(1, '0', 1, '0', '2014-02-18 17:16:42', '2014-03-24 21:52:33', 'vimukthi@geoclipse.com', 'vimukthi', '0', '0', 'Geoclipse', 'test123', '0', '0', '0', '0', '0', 1),
	(2, '0', 1, '0', '2014-02-21 12:42:09', '2014-03-24 21:52:40', 'tocha@geoclipse.com', 'tocha', 'fewf', '213412', 'Geoclipse', 'test123', '12231', 'tocha@geoclipse.com', 'tocha@geoclipse.com', 'tocha@geoclipse.com', 'tocha@geoclipse.com', 0),
	(4, '0', 0, '0', '2014-03-06 13:32:37', '2014-03-24 21:52:45', 'bfn@ermr.ckn', 'vjnj', 'froeo', '3132', 'Glaxo', 'fefw', '2121', 'ewrew', 'bfn@ermr.ckn', 'What is your birth place?', 'Colombo ???', 0),
	(6, '0', 1, '0', '2014-03-07 01:07:03', '2014-03-24 21:52:55', 'test1@test.com', 'test', 'test', '32143', 'Pfizer', 'test', '12321', '', 'test1@test.com', 'What is your birth place?', 'Colombo ???', 0);
/*!40000 ALTER TABLE `user` ENABLE KEYS */;


# Dumping structure for table medibid.user_preference
DROP TABLE IF EXISTS `user_preference`;
CREATE TABLE IF NOT EXISTS `user_preference` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date_created` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `value` varchar(255) DEFAULT NULL,
  `preference_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_user_preference_user_id` (`user_id`),
  KEY `FK_user_preference_preference_id` (`preference_id`),
  CONSTRAINT `FK_user_preference_preference_id` FOREIGN KEY (`preference_id`) REFERENCES `preference` (`id`),
  CONSTRAINT `FK_user_preference_user_id` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

# Dumping data for table medibid.user_preference: ~0 rows (approximately)
/*!40000 ALTER TABLE `user_preference` DISABLE KEYS */;
/*!40000 ALTER TABLE `user_preference` ENABLE KEYS */;


# Dumping structure for table medibid.user_role
DROP TABLE IF EXISTS `user_role`;
CREATE TABLE IF NOT EXISTS `user_role` (
  `role_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `id` int(10) DEFAULT NULL,
  `date_modified` int(10) DEFAULT NULL,
  PRIMARY KEY (`role_id`,`user_id`),
  KEY `FK_user_role_user_id` (`user_id`),
  CONSTRAINT `FK_user_role_role_id` FOREIGN KEY (`role_id`) REFERENCES `role` (`id`),
  CONSTRAINT `FK_user_role_user_id` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

# Dumping data for table medibid.user_role: ~3 rows (approximately)
/*!40000 ALTER TABLE `user_role` DISABLE KEYS */;
REPLACE INTO `user_role` (`role_id`, `user_id`, `id`, `date_modified`) VALUES
	(1, 1, 1, 2014),
	(2, 4, 2, 2014),
	(2, 6, 3, 2014);
/*!40000 ALTER TABLE `user_role` ENABLE KEYS */;


# Dumping structure for table medibid.valid_bid_transition
DROP TABLE IF EXISTS `valid_bid_transition`;
CREATE TABLE IF NOT EXISTS `valid_bid_transition` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date_modified` datetime DEFAULT NULL,
  `state1_id` int(11) DEFAULT NULL,
  `state2_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_valid_bid_transition_state1_id` (`state1_id`),
  KEY `FK_valid_bid_transition_state2_id` (`state2_id`),
  CONSTRAINT `FK_valid_bid_transition_state2_id` FOREIGN KEY (`state2_id`) REFERENCES `bid_state` (`id`),
  CONSTRAINT `FK_valid_bid_transition_state1_id` FOREIGN KEY (`state1_id`) REFERENCES `bid_state` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=latin1;

# Dumping data for table medibid.valid_bid_transition: ~19 rows (approximately)
/*!40000 ALTER TABLE `valid_bid_transition` DISABLE KEYS */;
REPLACE INTO `valid_bid_transition` (`id`, `date_modified`, `state1_id`, `state2_id`) VALUES
	(1, '2014-03-24 13:01:48', 1, 3),
	(2, '2014-03-24 13:02:05', 1, 9),
	(3, '2014-03-24 13:02:19', 1, 2),
	(4, '2014-03-24 13:03:17', 9, 1),
	(5, '2014-03-24 13:11:08', 9, 8),
	(6, '2014-03-24 13:04:58', 2, 3),
	(7, '2014-03-24 13:05:47', 3, 4),
	(8, '2014-03-24 13:06:01', 3, 2),
	(9, '2014-03-24 13:07:19', 4, 10),
	(10, '2014-03-24 13:08:52', 4, 7),
	(11, '2014-03-24 13:09:32', 4, 6),
	(12, '2014-03-24 13:09:55', 4, 5),
	(13, '2014-03-24 13:11:25', 1, 7),
	(14, '2014-03-24 13:12:40', 10, 4),
	(15, '2014-03-24 13:13:01', 10, 8),
	(16, '2014-03-24 13:14:24', 6, 5),
	(17, '2014-03-24 13:14:36', 5, 6),
	(18, '2014-03-24 13:17:41', 1, 8),
	(19, '2014-03-24 13:18:13', 4, 8);
/*!40000 ALTER TABLE `valid_bid_transition` ENABLE KEYS */;


# Dumping structure for table medibid.valid_order_bid_state_combo
DROP TABLE IF EXISTS `valid_order_bid_state_combo`;
CREATE TABLE IF NOT EXISTS `valid_order_bid_state_combo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date_modified` datetime DEFAULT NULL,
  `bid_state_id` int(11) DEFAULT NULL,
  `order_state_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_valid_order_bid_state_combo_order_state_id` (`order_state_id`),
  KEY `FK_valid_order_bid_state_combo_bid_state_id` (`bid_state_id`),
  CONSTRAINT `FK_valid_order_bid_state_combo_bid_state_id` FOREIGN KEY (`bid_state_id`) REFERENCES `bid_state` (`id`),
  CONSTRAINT `FK_valid_order_bid_state_combo_order_state_id` FOREIGN KEY (`order_state_id`) REFERENCES `order_state` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=40 DEFAULT CHARSET=latin1;

# Dumping data for table medibid.valid_order_bid_state_combo: ~34 rows (approximately)
/*!40000 ALTER TABLE `valid_order_bid_state_combo` DISABLE KEYS */;
REPLACE INTO `valid_order_bid_state_combo` (`id`, `date_modified`, `bid_state_id`, `order_state_id`) VALUES
	(1, '2014-03-24 23:30:19', 1, 1),
	(2, '2014-03-24 23:30:47', 9, 1),
	(3, '2014-03-24 23:30:58', 7, 1),
	(4, '2014-03-24 23:31:14', 8, 1),
	(5, '2014-03-24 23:32:23', 1, 2),
	(6, '2014-03-24 23:32:43', 3, 2),
	(7, '2014-03-24 23:33:46', 2, 2),
	(8, '2014-03-24 23:36:38', 7, 2),
	(9, '2014-03-24 23:37:42', 8, 2),
	(10, '2014-03-24 23:37:41', 3, 3),
	(11, '2014-03-24 23:38:10', 2, 3),
	(12, '2014-03-24 23:39:00', 4, 3),
	(13, '2014-03-24 23:39:15', 7, 3),
	(14, '2014-03-24 23:39:27', 8, 3),
	(15, '2014-03-24 23:40:09', 10, 3),
	(17, '2014-03-24 23:41:34', 4, 5),
	(18, '2014-03-24 23:42:58', 2, 5),
	(20, '2014-03-24 23:44:59', 6, 5),
	(21, '2014-03-24 23:45:24', 5, 5),
	(22, '2014-03-24 23:45:35', 7, 5),
	(24, '2014-03-24 23:45:51', 8, 5),
	(26, '2014-03-24 23:51:20', 5, 4),
	(27, '2014-03-24 23:49:15', 2, 4),
	(28, '2014-03-24 23:50:05', 8, 4),
	(30, '2014-03-24 23:52:00', 7, 4),
	(31, '2014-03-24 23:54:25', 6, 6),
	(32, '2014-03-24 23:54:47', 2, 6),
	(33, '2014-03-24 23:55:18', 5, 6),
	(34, '2014-03-24 23:55:32', 7, 6),
	(35, '2014-03-24 23:55:44', 8, 6),
	(36, '2014-03-24 23:57:20', 5, 7),
	(37, '2014-03-24 23:57:43', 2, 7),
	(38, '2014-03-24 23:58:51', 7, 7),
	(39, '2014-03-24 23:59:11', 8, 7);
/*!40000 ALTER TABLE `valid_order_bid_state_combo` ENABLE KEYS */;


# Dumping structure for table medibid.valid_order_transition
DROP TABLE IF EXISTS `valid_order_transition`;
CREATE TABLE IF NOT EXISTS `valid_order_transition` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date_modified` datetime DEFAULT NULL,
  `state1_id` int(11) DEFAULT NULL,
  `state2_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_valid_order_transition_state1_id` (`state1_id`),
  KEY `FK_valid_order_transition_state2_id` (`state2_id`),
  CONSTRAINT `FK_valid_order_transition_state2_id` FOREIGN KEY (`state2_id`) REFERENCES `order_state` (`id`),
  CONSTRAINT `FK_valid_order_transition_state1_id` FOREIGN KEY (`state1_id`) REFERENCES `order_state` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

# Dumping data for table medibid.valid_order_transition: ~7 rows (approximately)
/*!40000 ALTER TABLE `valid_order_transition` DISABLE KEYS */;
REPLACE INTO `valid_order_transition` (`id`, `date_modified`, `state1_id`, `state2_id`) VALUES
	(1, '2014-03-14 14:35:27', 1, 2),
	(2, '2014-03-14 14:35:42', 1, 4),
	(3, '2014-03-14 14:36:00', 2, 3),
	(4, '2014-03-14 14:36:23', 3, 5),
	(5, '2014-03-14 14:36:32', 3, 4),
	(6, '2014-03-14 14:37:37', 5, 6),
	(7, '2014-03-14 14:37:49', 5, 7);
/*!40000 ALTER TABLE `valid_order_transition` ENABLE KEYS */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
