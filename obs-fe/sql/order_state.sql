# --------------------------------------------------------
# Host:                         127.0.0.1
# Server version:               5.5.34-0ubuntu0.12.04.1
# Server OS:                    debian-linux-gnu
# HeidiSQL version:             6.0.0.3603
# Date/time:                    2014-03-16 16:46:42
# --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
# Dumping data for table medibid.order_state: ~7 rows (approximately)
/*!40000 ALTER TABLE `order_state` DISABLE KEYS */;
INSERT INTO `order_state` (`id`, `name`, `display_name`, `description`, `date_modified`) VALUES
	(1, 'phase1', 'Phase 1', 'phase1 bidding started', '2014-03-16 16:44:10'),
	(2, 'phase1_expired', 'Phase 1 Expired', 'phase 1 bidding time expired', '2014-03-16 16:44:20'),
	(3, 'phase2', 'Phase 2', 'phase 2 bidding started', '2014-03-16 16:44:28'),
	(4, 'cancelled', 'Cancelled', 'bidding cancelled primaturely', '2014-03-16 16:44:34'),
	(5, 'phase2_expired', 'Phase 2 Expired', 'phase 2 bidding time expired', '2014-03-16 16:44:43'),
	(6, 'fullfilled', 'Fullfilled', 'order request fullfilled', '2014-03-16 16:44:52'),
	(7, 'unsatisfied', 'Unsatisfied', 'after phase 2 the order request is still unsatisfied', '2014-03-16 16:44:58');
/*!40000 ALTER TABLE `order_state` ENABLE KEYS */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;



# --------------------------------------------------------
# Host:                         127.0.0.1
# Server version:               5.5.34-0ubuntu0.12.04.1
# Server OS:                    debian-linux-gnu
# HeidiSQL version:             6.0.0.3603
# Date/time:                    2014-03-15 20:35:30
# --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
# Dumping data for table medibid.valid_order_transition: ~7 rows (approximately)
/*!40000 ALTER TABLE `valid_order_transition` DISABLE KEYS */;
INSERT INTO `valid_order_transition` (`id`, `state1_id`, `state2_id`, `date_modified`) VALUES
	(1, 1, 2, '2014-03-14 14:35:27'),
	(2, 1, 4, '2014-03-14 14:35:42'),
	(3, 2, 3, '2014-03-14 14:36:00'),
	(4, 3, 5, '2014-03-14 14:36:23'),
	(5, 3, 4, '2014-03-14 14:36:32'),
	(6, 5, 6, '2014-03-14 14:37:37'),
	(7, 5, 7, '2014-03-14 14:37:49');
/*!40000 ALTER TABLE `valid_order_transition` ENABLE KEYS */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
