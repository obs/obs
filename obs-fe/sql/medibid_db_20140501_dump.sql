# --------------------------------------------------------
# Host:                         127.0.0.1
# Server version:               5.5.34-0ubuntu0.12.04.1
# Server OS:                    debian-linux-gnu
# HeidiSQL version:             6.0.0.3603
# Date/time:                    2014-05-01 20:20:30
# --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
# Dumping data for table medibid.announcement: ~0 rows (approximately)
/*!40000 ALTER TABLE `announcement` DISABLE KEYS */;
/*!40000 ALTER TABLE `announcement` ENABLE KEYS */;

# Dumping data for table medibid.bid: ~16 rows (approximately)
/*!40000 ALTER TABLE `bid` DISABLE KEYS */;
INSERT INTO `bid` (`id`, `amends_bid_id`, `date_created`, `date_modified`, `description`, `price_per_unit`, `quantity`, `bidder_id`, `order_id`, `state_id`) VALUES
	(6, 0, '2014-03-18 11:18:49', '2014-03-25 14:16:59', 'Blah blah', 123, 21, 2, 7, 1),
	(7, 0, '2014-03-18 11:18:49', '2014-03-25 14:17:03', 'Blah blah Blah blahBlah blahBlah blahBlah blah', 2154, 32, 2, 7, 8),
	(8, 0, '2014-03-26 11:03:12', '2014-03-26 16:05:04', 'fwwt', 32.09, 213, 4, 3, 8),
	(9, 0, '2014-03-26 11:13:15', '2014-03-26 11:13:15', 'fewfew', 1, 21, 6, 9, 8),
	(10, 0, '2014-03-28 16:44:35', '2014-03-28 16:44:35', '<span style="background-color: green;">Nice Bids this is</span>', 0.1, 21, 6, 10, 8),
	(11, 0, '2014-03-28 16:47:23', '2014-03-28 16:47:23', 'Another one', 0.1, 21, 4, 10, 1),
	(12, 0, '2014-03-28 16:52:44', '2014-03-28 16:52:44', 'fewfe ewf', 0.121, 3121, 4, 10, 7),
	(13, 0, '2014-03-28 17:34:16', '2014-03-28 17:34:16', 'ewet&nbsp;', 0.1, 321, 4, 10, 1),
	(14, 0, '2014-03-28 17:38:17', '2014-03-28 17:38:17', '<u>Test bid nice</u>', 13, 65, 6, 11, 1),
	(15, 0, '2014-03-28 21:47:32', '2014-03-28 21:47:32', 'Blah blahkbw&nbsp;', 0.21, 321, 4, 9, 1),
	(16, 0, '2014-03-28 21:49:50', '2014-03-28 21:49:50', 'test&nbsp;test&nbsp;testtesttesttesttesttesttesttes<br>', 123, 345, 4, 8, 1),
	(17, 0, '2014-03-28 16:47:23', '2014-03-28 16:47:23', 'Another one', 0.1, 21, 4, 10, 8),
	(18, 0, '2014-03-28 16:47:23', '2014-03-28 16:47:23', 'Another one', 0.1, 21, 4, 10, 1),
	(19, 0, '2014-03-28 16:47:23', '2014-03-28 16:47:23', 'Another one', 0.1, 21, 4, 10, 1),
	(20, 0, '2014-03-28 16:47:23', '2014-03-28 16:47:23', 'Another one', 0.1, 21, 4, 10, 1),
	(21, 0, '2014-03-28 16:47:23', '2014-03-28 16:47:23', 'Another one', 0.1, 21, 4, 10, 8);
/*!40000 ALTER TABLE `bid` ENABLE KEYS */;

# Dumping data for table medibid.bid_state: ~10 rows (approximately)
/*!40000 ALTER TABLE `bid_state` DISABLE KEYS */;
INSERT INTO `bid_state` (`id`, `date_modified`, `description`, `display_name`, `name`) VALUES
	(1, '2014-03-24 12:51:56', 'bid is active for the phase 1 of the order', 'Phase 1 Active', 'phase1_active'),
	(2, '2014-03-24 12:48:29', 'bid was rejected on phase 1 of the order', 'Rejected on Phase 1', 'phase1_rejected'),
	(3, '2014-03-24 12:49:14', 'bid was accepted on phase 1 of the order', 'Accepted on Phase 1', 'phase1_accepted'),
	(4, '2014-03-24 12:47:43', 'bid is active for the phase 2 of the order', 'Phase 2 Active', 'phase2_active'),
	(5, '2014-03-24 12:47:43', 'bid was rejected on final phase of the order', 'Rejected', 'rejected'),
	(6, '2014-03-24 12:53:59', 'bid was accepted on final phase of the order', 'Accepted', 'accepted'),
	(7, '2014-03-24 12:55:11', 'bid was cancelled by the bidder', 'Cancelled', 'cancelled'),
	(8, '2014-03-24 12:55:11', 'bid was cancelled by the admin', 'Admin Cancelled', 'admin_cancelled'),
	(9, '2014-03-24 12:49:14', 'bid was put onhold on phase 1 of the order', 'Put onhold on Phase 1', 'phase1_onhold'),
	(10, '2014-03-24 12:58:47', 'bid was put onhold on phase 2 of the order', 'Put onhold on Phase 2', 'phase2_onhold');
/*!40000 ALTER TABLE `bid_state` ENABLE KEYS */;

# Dumping data for table medibid.message: ~0 rows (approximately)
/*!40000 ALTER TABLE `message` DISABLE KEYS */;
INSERT INTO `message` (`id`, `body`, `date_created`, `date_viewed`, `parent`, `state`, `title`, `receiver_id`, `sender_id`) VALUES
	(1, 'Your bid 21@1.0 for order test oreder changed state to Admin Cancelled', '2014-05-01 19:28:35', NULL, NULL, 0, 'One of your bids changed state', 6, 1),
	(2, 'Your bid 32@2154.0 for order test121 changed state to Admin Cancelled', '2014-05-01 19:39:00', NULL, NULL, 0, 'One of your bids changed state', 2, 1),
	(3, 'Order with code TEST32 changed state to Phase 1', '2014-05-01 20:18:19', NULL, NULL, 0, 'An order changed state', 1, 1),
	(4, 'Order with code TEST32 changed state to Phase 1', '2014-05-01 20:18:19', NULL, NULL, 0, 'An order changed state', 2, 1),
	(5, 'Order with code TEST32 changed state to Phase 1', '2014-05-01 20:18:19', NULL, NULL, 0, 'An order changed state', 4, 1),
	(6, 'Order with code TEST32 changed state to Phase 1', '2014-05-01 20:18:19', NULL, NULL, 0, 'An order changed state', 6, 1);
/*!40000 ALTER TABLE `message` ENABLE KEYS */;

# Dumping data for table medibid.order_request: ~13 rows (approximately)
/*!40000 ALTER TABLE `order_request` DISABLE KEYS */;
INSERT INTO `order_request` (`id`, `code`, `date_created`, `date_modified`, `description`, `expiration_date`, `timezone`, `image`, `quantity`, `summary`, `state_id`) VALUES
	(1, 'order1', '2014-03-11 18:16:20', '2014-03-26 19:41:18', '0', '2014-03-25 18:16:16', 'Asia/Colombo', 'image.jpg', 21, 'fefe', 6),
	(2, 'order2', '2014-03-11 18:16:20', '2014-03-28 12:18:39', '0', '2014-03-30 18:16:16', 'Asia/Colombo', 'image.jpg', 21, 'rgeg', 2),
	(3, 'order3', '2014-03-11 18:16:20', '2014-03-26 16:05:03', '0', '2014-04-04 18:16:16', 'Asia/Colombo', 'image.jpg', 21, 'rgeg', 4),
	(4, 'test21', '2014-03-11 18:16:20', '2014-03-26 19:41:08', '0', '2014-03-29 13:15:53', 'Asia/Colombo', 'image.jpg', 21, 'rewre', 7),
	(5, 'blah21', '2014-03-11 18:16:20', '2014-03-17 01:20:31', '0', '2014-03-29 13:15:53', 'Asia/Colombo', 'image.jpg', 21, 'wqvvfd', 4),
	(6, 'mnnewe2', '2014-03-11 18:16:20', '2014-03-28 12:33:53', '0', '2014-03-29 12:15:53', 'Asia/Colombo', 'image.jpg', 323, 'lkloa', 7),
	(7, 'test121', '2014-03-16 09:16:05', '2014-03-26 09:30:14', '<i>test order</i>', '2014-03-19 00:00:00', 'Asia/Colombo', 'Chrysanthemum.jpg', 21, 'test order', 4),
	(8, 'test214', '2014-03-16 10:53:21', '2014-03-28 17:40:10', '<u>test order</u>', '2014-04-14 17:47:00', 'Asia/Colombo', 'Jellyfish.jpg', 2134, 'test order', 2),
	(9, 'test oreder', '2014-03-17 11:53:11', '2014-03-28 11:32:01', 'the order test', '2014-03-31 00:00:00', 'Asia/Colombo', 'Koala.jpg', 32, 'THis is the summary', 2),
	(10, 'Test order new', '2014-03-26 09:31:53', '2014-03-28 11:37:39', '<i>Nice Order</i>', '2014-03-29 00:00:00', 'Asia/Colombo', '20140204_161710.jpg', 1256, 'Nice Order', 2),
	(11, 'Test order new 1', '2014-03-26 09:43:23', '2014-03-28 17:40:49', 'Test order new 1', '2014-05-29 00:30:00', 'Asia/Colombo', 'Lighthouse.jpg', 543, 'Test order new 1', 1),
	(12, 'userTimeZone', '2014-04-14 16:35:34', NULL, 'userTimeZone', '2014-04-14 20:24:10', 'Asia/Colombo', 'Chrysanthemum.jpg', 321, 'userTimeZone', 2),
	(13, 'one', '2014-04-17 00:30:22', NULL, 'one', '2014-04-17 00:31:25', 'Asia/Colombo', 'Lighthouse.jpg', 23, 'one', 2),
	(14, 'TEST32', '2014-05-01 20:18:19', NULL, 'Test 32', '2014-05-15 20:18:08', 'Asia/Colombo', 'Penguins.jpg', 50000, 'Test 32', 1);
/*!40000 ALTER TABLE `order_request` ENABLE KEYS */;

# Dumping data for table medibid.order_state: ~7 rows (approximately)
/*!40000 ALTER TABLE `order_state` DISABLE KEYS */;
INSERT INTO `order_state` (`id`, `date_modified`, `description`, `display_name`, `name`) VALUES
	(1, '2014-03-16 16:44:10', 'phase1 bidding started', 'Phase 1', 'phase1'),
	(2, '2014-03-16 16:44:20', 'phase 1 bidding time expired', 'Phase 1 Expired', 'phase1_expired'),
	(3, '2014-03-16 16:44:28', 'phase 2 bidding started', 'Phase 2', 'phase2'),
	(4, '2014-03-16 16:44:34', 'bidding cancelled primaturely', 'Cancelled', 'cancelled'),
	(5, '2014-03-16 16:44:43', 'phase 2 bidding time expired', 'Phase 2 Expired', 'phase2_expired'),
	(6, '2014-03-16 16:44:52', 'order request fullfilled', 'Fullfilled', 'fullfilled'),
	(7, '2014-03-16 16:44:58', 'after phase 2 the order request is still unsatisfied', 'Unsatisfied', 'unsatisfied');
/*!40000 ALTER TABLE `order_state` ENABLE KEYS */;

# Dumping data for table medibid.permission: ~1 rows (approximately)
/*!40000 ALTER TABLE `permission` DISABLE KEYS */;
INSERT INTO `permission` (`id`, `date_created`, `date_modified`, `name`) VALUES
	(1, '0000-00-00 00:00:00', '2014-02-21 15:22:11', 'addbid');
/*!40000 ALTER TABLE `permission` ENABLE KEYS */;

# Dumping data for table medibid.preference: ~1 rows (approximately)
/*!40000 ALTER TABLE `preference` DISABLE KEYS */;
INSERT INTO `preference` (`id`, `date_created`, `date_modified`, `description`, `name`) VALUES
	(1, '2014-04-14 15:33:27', '2014-04-14 15:33:28', 'Timezone for user', 'timezone');
/*!40000 ALTER TABLE `preference` ENABLE KEYS */;

# Dumping data for table medibid.role: ~2 rows (approximately)
/*!40000 ALTER TABLE `role` DISABLE KEYS */;
INSERT INTO `role` (`id`, `date_created`, `date_modified`, `description`, `name`) VALUES
	(1, '2014-02-18 17:15:11', '2014-02-18 17:15:12', '0', 'admin'),
	(2, '2014-02-21 12:41:33', '2014-02-21 12:41:34', '0', 'bidder');
/*!40000 ALTER TABLE `role` ENABLE KEYS */;

# Dumping data for table medibid.role_permission: ~1 rows (approximately)
/*!40000 ALTER TABLE `role_permission` DISABLE KEYS */;
INSERT INTO `role_permission` (`permission_id`, `role_id`) VALUES
	(1, 2);
/*!40000 ALTER TABLE `role_permission` ENABLE KEYS */;

# Dumping data for table medibid.user: ~4 rows (approximately)
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` (`id`, `about_me`, `active`, `admin_comment`, `date_created`, `date_modified`, `email`, `first_name`, `last_name`, `mobile`, `organization`, `password`, `phone`, `photo`, `secondary_email`, `security_question`, `security_question_answer`, `system`) VALUES
	(1, '0', 1, '0', '2014-02-18 17:16:42', '2014-03-24 21:52:33', 'vimukthi@geoclipse.com', 'vimukthi', '0', '0', 'Geoclipse', 'test123', '0', '0', '0', '0', '0', 1),
	(2, '0', 1, '0', '2014-02-21 12:42:09', '2014-03-24 21:52:40', 'tocha@geoclipse.com', 'tocha', 'fewf', '213412', 'Geoclipse', 'test123', '12231', 'tocha@geoclipse.com', 'tocha@geoclipse.com', 'tocha@geoclipse.com', 'tocha@geoclipse.com', 0),
	(4, '0', 1, '0', '2014-03-06 13:32:37', '2014-03-28 21:41:54', 'bfn@ermr.ckn', 'vjnj', 'froeo', '3132', 'Glaxo', 'test123', '2121', 'ewrew', 'bfn@ermr.ckn', 'What is your birth place?', 'Colombo ???', 0),
	(6, '0', 1, '0', '2014-03-07 01:07:03', '2014-03-24 21:52:55', 'test1@test.com', 'test', 'test', '32143', 'Pfizer', 'test123', '12321', '', 'test1@test.com', 'What is your birth place?', 'Colombo ???', 0);
/*!40000 ALTER TABLE `user` ENABLE KEYS */;

# Dumping data for table medibid.user_preference: ~1 rows (approximately)
/*!40000 ALTER TABLE `user_preference` DISABLE KEYS */;
INSERT INTO `user_preference` (`id`, `date_created`, `date_modified`, `value`, `preference_id`, `user_id`) VALUES
	(1, '2014-04-14 15:34:00', '2014-04-14 15:34:01', 'Asia/Colombo', 1, 1);
/*!40000 ALTER TABLE `user_preference` ENABLE KEYS */;

# Dumping data for table medibid.user_role: ~3 rows (approximately)
/*!40000 ALTER TABLE `user_role` DISABLE KEYS */;
INSERT INTO `user_role` (`user_id`, `role_id`) VALUES
	(1, 1),
	(4, 2),
	(6, 2);
/*!40000 ALTER TABLE `user_role` ENABLE KEYS */;

# Dumping data for table medibid.valid_bid_transition: ~19 rows (approximately)
/*!40000 ALTER TABLE `valid_bid_transition` DISABLE KEYS */;
INSERT INTO `valid_bid_transition` (`id`, `date_modified`, `state1_id`, `state2_id`) VALUES
	(1, '2014-03-24 13:01:48', 1, 3),
	(2, '2014-03-24 13:02:05', 1, 9),
	(3, '2014-03-24 13:02:19', 1, 2),
	(4, '2014-03-24 13:03:17', 9, 1),
	(5, '2014-03-24 13:11:08', 9, 8),
	(6, '2014-03-24 13:04:58', 2, 3),
	(7, '2014-03-24 13:05:47', 3, 4),
	(8, '2014-03-24 13:06:01', 3, 2),
	(9, '2014-03-24 13:07:19', 4, 10),
	(10, '2014-03-24 13:08:52', 4, 7),
	(11, '2014-03-24 13:09:32', 4, 6),
	(12, '2014-03-24 13:09:55', 4, 5),
	(13, '2014-03-24 13:11:25', 1, 7),
	(14, '2014-03-24 13:12:40', 10, 4),
	(15, '2014-03-24 13:13:01', 10, 8),
	(16, '2014-03-24 13:14:24', 6, 5),
	(17, '2014-03-24 13:14:36', 5, 6),
	(18, '2014-03-24 13:17:41', 1, 8),
	(19, '2014-03-24 13:18:13', 4, 8);
/*!40000 ALTER TABLE `valid_bid_transition` ENABLE KEYS */;

# Dumping data for table medibid.valid_order_bid_state_combo: ~34 rows (approximately)
/*!40000 ALTER TABLE `valid_order_bid_state_combo` DISABLE KEYS */;
INSERT INTO `valid_order_bid_state_combo` (`id`, `date_modified`, `bid_state_id`, `order_state_id`) VALUES
	(1, '2014-03-24 23:30:19', 1, 1),
	(2, '2014-03-24 23:30:47', 9, 1),
	(3, '2014-03-24 23:30:58', 7, 1),
	(4, '2014-03-24 23:31:14', 8, 1),
	(5, '2014-03-24 23:32:23', 1, 2),
	(6, '2014-03-24 23:32:43', 3, 2),
	(7, '2014-03-24 23:33:46', 2, 2),
	(8, '2014-03-24 23:36:38', 7, 2),
	(9, '2014-03-24 23:37:42', 8, 2),
	(10, '2014-03-24 23:37:41', 3, 3),
	(11, '2014-03-24 23:38:10', 2, 3),
	(12, '2014-03-24 23:39:00', 4, 3),
	(13, '2014-03-24 23:39:15', 7, 3),
	(14, '2014-03-24 23:39:27', 8, 3),
	(15, '2014-03-24 23:40:09', 10, 3),
	(17, '2014-03-24 23:41:34', 4, 5),
	(18, '2014-03-24 23:42:58', 2, 5),
	(20, '2014-03-24 23:44:59', 6, 5),
	(21, '2014-03-24 23:45:24', 5, 5),
	(22, '2014-03-24 23:45:35', 7, 5),
	(24, '2014-03-24 23:45:51', 8, 5),
	(26, '2014-03-24 23:51:20', 5, 4),
	(27, '2014-03-24 23:49:15', 2, 4),
	(28, '2014-03-24 23:50:05', 8, 4),
	(30, '2014-03-24 23:52:00', 7, 4),
	(31, '2014-03-24 23:54:25', 6, 6),
	(32, '2014-03-24 23:54:47', 2, 6),
	(33, '2014-03-24 23:55:18', 5, 6),
	(34, '2014-03-24 23:55:32', 7, 6),
	(35, '2014-03-24 23:55:44', 8, 6),
	(36, '2014-03-24 23:57:20', 5, 7),
	(37, '2014-03-24 23:57:43', 2, 7),
	(38, '2014-03-24 23:58:51', 7, 7),
	(39, '2014-03-24 23:59:11', 8, 7);
/*!40000 ALTER TABLE `valid_order_bid_state_combo` ENABLE KEYS */;

# Dumping data for table medibid.valid_order_transition: ~7 rows (approximately)
/*!40000 ALTER TABLE `valid_order_transition` DISABLE KEYS */;
INSERT INTO `valid_order_transition` (`id`, `date_modified`, `state1_id`, `state2_id`) VALUES
	(1, '2014-03-14 14:35:27', 1, 2),
	(2, '2014-03-14 14:35:42', 1, 4),
	(3, '2014-03-14 14:36:00', 2, 3),
	(4, '2014-03-14 14:36:23', 3, 5),
	(5, '2014-03-14 14:36:32', 3, 4),
	(6, '2014-03-14 14:37:37', 5, 6),
	(7, '2014-03-14 14:37:49', 5, 7);
/*!40000 ALTER TABLE `valid_order_transition` ENABLE KEYS */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
