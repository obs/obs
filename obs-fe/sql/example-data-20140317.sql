# --------------------------------------------------------
# Host:                         127.0.0.1
# Server version:               5.5.34-0ubuntu0.12.04.1
# Server OS:                    debian-linux-gnu
# HeidiSQL version:             6.0.0.3603
# Date/time:                    2014-03-17 15:19:04
# --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
# Dumping data for table medibid.announcement: ~0 rows (approximately)
/*!40000 ALTER TABLE `announcement` DISABLE KEYS */;
/*!40000 ALTER TABLE `announcement` ENABLE KEYS */;

# Dumping data for table medibid.bid: ~0 rows (approximately)
/*!40000 ALTER TABLE `bid` DISABLE KEYS */;
/*!40000 ALTER TABLE `bid` ENABLE KEYS */;

# Dumping data for table medibid.order_request: ~8 rows (approximately)
/*!40000 ALTER TABLE `order_request` DISABLE KEYS */;
INSERT INTO `order_request` (`id`, `state_id`, `code`, `summary`, `description`, `quantity`, `expiration_date`, `image`, `date_created`, `date_modified`) VALUES
	(1, 4, 'order1', 'fefe', '0', 21, '2014-03-25 18:16:16', 'image.jpg', '2014-03-11 18:16:20', '2014-03-17 01:04:55'),
	(2, 2, 'order2', 'rgeg', '0', 21, '2014-03-04 18:16:16', 'image.jpg', '2014-03-11 18:16:20', '2014-03-16 17:40:40'),
	(3, 1, 'order3', 'rgeg', '0', 21, '2014-04-04 18:16:16', 'image.jpg', '2014-03-11 18:16:20', '2014-03-15 21:47:20'),
	(4, 4, 'test21', 'rewre', '0', 21, '2014-03-29 13:15:53', 'image.jpg', '2014-03-11 18:16:20', '2014-03-17 12:59:06'),
	(5, 4, 'blah21', 'wqvvfd', '0', 21, '2014-03-29 13:15:53', 'image.jpg', '2014-03-11 18:16:20', '2014-03-17 01:20:31'),
	(6, 4, 'mnnewe2', 'lkloa', '0', 323, '2014-03-29 12:15:53', 'image.jpg', '2014-03-11 18:16:20', '2014-03-17 11:53:40'),
	(7, 1, 'test121', 'test order', '<i>test order</i>', 21, '2014-03-19 00:00:00', 'Chrysanthemum.jpg', '2014-03-16 09:16:05', '2014-03-16 09:16:06'),
	(8, 1, 'test214', 'test order', '<u>test order</u>', 2134, '2014-03-26 00:00:00', 'Jellyfish.jpg', '2014-03-16 10:53:21', '2014-03-16 10:53:21'),
	(9, 1, 'test oreder', 'fdgrgr', 'the order test', 32, '2014-03-27 00:00:00', 'Koala.jpg', '2014-03-17 11:53:11', '2014-03-17 11:53:11');
/*!40000 ALTER TABLE `order_request` ENABLE KEYS */;

# Dumping data for table medibid.order_state: ~7 rows (approximately)
/*!40000 ALTER TABLE `order_state` DISABLE KEYS */;
INSERT INTO `order_state` (`id`, `name`, `display_name`, `description`, `date_modified`) VALUES
	(1, 'phase1', 'Phase 1', 'phase1 bidding started', '2014-03-16 16:44:10'),
	(2, 'phase1_expired', 'Phase 1 Expired', 'phase 1 bidding time expired', '2014-03-16 16:44:20'),
	(3, 'phase2', 'Phase 2', 'phase 2 bidding started', '2014-03-16 16:44:28'),
	(4, 'cancelled', 'Cancelled', 'bidding cancelled primaturely', '2014-03-16 16:44:34'),
	(5, 'phase2_expired', 'Phase 2 Expired', 'phase 2 bidding time expired', '2014-03-16 16:44:43'),
	(6, 'fullfilled', 'Fullfilled', 'order request fullfilled', '2014-03-16 16:44:52'),
	(7, 'unsatisfied', 'Unsatisfied', 'after phase 2 the order request is still unsatisfied', '2014-03-16 16:44:58');
/*!40000 ALTER TABLE `order_state` ENABLE KEYS */;

# Dumping data for table medibid.permission: ~1 rows (approximately)
/*!40000 ALTER TABLE `permission` DISABLE KEYS */;
INSERT INTO `permission` (`id`, `name`, `date_created`, `date_modified`) VALUES
	(1, 'addbid', '0000-00-00 00:00:00', '2014-02-21 15:22:11');
/*!40000 ALTER TABLE `permission` ENABLE KEYS */;

# Dumping data for table medibid.preference: ~0 rows (approximately)
/*!40000 ALTER TABLE `preference` DISABLE KEYS */;
/*!40000 ALTER TABLE `preference` ENABLE KEYS */;

# Dumping data for table medibid.role: ~2 rows (approximately)
/*!40000 ALTER TABLE `role` DISABLE KEYS */;
INSERT INTO `role` (`id`, `name`, `description`, `date_created`, `date_modified`) VALUES
	(1, 'admin', '0', '2014-02-18 17:15:11', '2014-02-18 17:15:12'),
	(2, 'bidder', '0', '2014-02-21 12:41:33', '2014-02-21 12:41:34');
/*!40000 ALTER TABLE `role` ENABLE KEYS */;

# Dumping data for table medibid.role_permission: ~1 rows (approximately)
/*!40000 ALTER TABLE `role_permission` DISABLE KEYS */;
INSERT INTO `role_permission` (`id`, `role_id`, `permission_id`, `date_created`, `date_modified`) VALUES
	(1, 2, 1, '0000-00-00 00:00:00', '2014-02-21 15:22:53');
/*!40000 ALTER TABLE `role_permission` ENABLE KEYS */;

# Dumping data for table medibid.user: ~4 rows (approximately)
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` (`id`, `first_name`, `last_name`, `about_me`, `admin_comment`, `phone`, `mobile`, `email`, `secondary_email`, `photo`, `password`, `security_question`, `security_question_answer`, `organization`, `active`, `system`, `date_created`, `date_modified`) VALUES
	(1, 'vimukthi', '0', '0', '0', '0', '0', 'vimukthi@geoclipse.com', '0', '0', 'test123', '0', '0', 'Test', 1, 1, '2014-02-18 17:16:42', '2014-03-06 23:48:38'),
	(2, 'tocha', 'fewf', '0', '0', '12231', '213412', 'tocha@geoclipse.com', 'tocha@geoclipse.com', 'tocha@geoclipse.com', 'test123', 'tocha@geoclipse.com', 'tocha@geoclipse.com', 'test', 1, 0, '2014-02-21 12:42:09', '2014-03-07 00:54:13'),
	(4, 'vjnj', 'froeo', '0', '0', '2121', '3132', 'bfn@ermr.ckn', 'bfn@ermr.ckn', 'ewrew', 'fefw', 'What is your birth place?', 'Colombo ???', 'retwrq', 0, 0, '2014-03-06 13:32:37', '2014-03-07 01:14:18'),
	(6, 'test', 'test', '0', '0', '12321', '32143', 'test1@test.com', 'test1@test.com', '', 'test', 'What is your birth place?', 'Colombo ???', 'test', 1, 0, '2014-03-07 01:07:03', NULL);
/*!40000 ALTER TABLE `user` ENABLE KEYS */;

# Dumping data for table medibid.user_preference: ~0 rows (approximately)
/*!40000 ALTER TABLE `user_preference` DISABLE KEYS */;
/*!40000 ALTER TABLE `user_preference` ENABLE KEYS */;

# Dumping data for table medibid.user_role: ~3 rows (approximately)
/*!40000 ALTER TABLE `user_role` DISABLE KEYS */;
INSERT INTO `user_role` (`id`, `user_id`, `role_id`, `date_modified`) VALUES
	(1, 1, 1, '2014-02-21 15:39:39'),
	(2, 4, 2, '2014-03-06 13:32:37'),
	(3, 6, 2, '2014-03-07 01:07:03');
/*!40000 ALTER TABLE `user_role` ENABLE KEYS */;

# Dumping data for table medibid.valid_order_transition: ~7 rows (approximately)
/*!40000 ALTER TABLE `valid_order_transition` DISABLE KEYS */;
INSERT INTO `valid_order_transition` (`id`, `state1_id`, `state2_id`, `date_modified`) VALUES
	(1, 1, 2, '2014-03-14 14:35:27'),
	(2, 1, 4, '2014-03-14 14:35:42'),
	(3, 2, 3, '2014-03-14 14:36:00'),
	(4, 3, 5, '2014-03-14 14:36:23'),
	(5, 3, 4, '2014-03-14 14:36:32'),
	(6, 5, 6, '2014-03-14 14:37:37'),
	(7, 5, 7, '2014-03-14 14:37:49');
/*!40000 ALTER TABLE `valid_order_transition` ENABLE KEYS */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
