# --------------------------------------------------------
# Host:                         127.0.0.1
# Server version:               5.5.34-0ubuntu0.12.04.1
# Server OS:                    debian-linux-gnu
# HeidiSQL version:             6.0.0.3603
# Date/time:                    2014-03-29 19:48:43
# --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
# Dumping data for table medibid_20`medibid`140329.announcement: ~0 rows (approximately)
/*!40000 ALTER TABLE `announcement` DISABLE KEYS */;
/*!40000 ALTER TABLE `announcement` ENABLE KEYS */;

# Dumping data for table medibid_20140329.bid: ~11 rows (approximately)
/*!40000 ALTER TABLE `bid` DISABLE KEYS */;
INSERT INTO `bid` (`id`, `order_id`, `bidder_id`, `state_id`, `amends_bid_id`, `price_per_unit`, `quantity`, `description`, `date_created`, `date_modified`) VALUES
	(6, 7, 2, 1, 0, 123, 21, 'Blah blah', '2014-03-18 11:18:49', '2014-03-25 14:16:59'),
	(7, 7, 2, 1, 0, 2154, 32, 'Blah blah Blah blahBlah blahBlah blahBlah blah', '2014-03-18 11:18:49', '2014-03-25 14:17:03'),
	(8, 3, 4, 8, 0, 32.09, 213, 'fwwt', '2014-03-26 11:03:12', '2014-03-26 16:05:04'),
	(9, 9, 6, 1, 0, 1, 21, 'fewfew', '2014-03-26 11:13:15', '2014-03-26 11:13:15'),
	(10, 10, 6, 1, 0, 0.1, 21, '<span style="background-color: green;">Nice Bids this is</span>', '2014-03-28 16:44:35', '2014-03-28 16:44:35'),
	(11, 10, 4, 1, 0, 0.1, 21, 'Another one', '2014-03-28 16:47:23', '2014-03-28 16:47:23'),
	(12, 10, 4, 1, 0, 0.121, 3121, 'fewfe ewf', '2014-03-28 16:52:44', '2014-03-28 16:52:44'),
	(13, 10, 4, 1, 0, 0.1, 321, 'ewet&nbsp;', '2014-03-28 17:34:16', '2014-03-28 17:34:16'),
	(14, 11, 6, 1, 0, 13, 65, '<u>Test bid nice</u>', '2014-03-28 17:38:17', '2014-03-28 17:38:17'),
	(15, 9, 4, 1, 0, 0.21, 321, 'Blah blahkbw&nbsp;', '2014-03-28 21:47:32', '2014-03-28 21:47:32'),
	(16, 8, 4, 1, 0, 123, 345, 'test&nbsp;test&nbsp;testtesttesttesttesttesttesttes<br>', '2014-03-28 21:49:50', '2014-03-28 21:49:50');
/*!40000 ALTER TABLE `bid` ENABLE KEYS */;

# Dumping data for table medibid_20140329.bid_state: ~10 rows (approximately)
/*!40000 ALTER TABLE `bid_state` DISABLE KEYS */;
INSERT INTO `bid_state` (`id`, `name`, `display_name`, `description`, `date_modified`) VALUES
	(1, 'phase1_active', 'Phase 1 Active', 'bid is active for the phase 1 of the order', '2014-03-24 12:51:56'),
	(2, 'phase1_rejected', 'Rejected on Phase 1', 'bid was rejected on phase 1 of the order', '2014-03-24 12:48:29'),
	(3, 'phase1_accepted', 'Accepted on Phase 1', 'bid was accepted on phase 1 of the order', '2014-03-24 12:49:14'),
	(4, 'phase2_active', 'Phase 2 Active', 'bid is active for the phase 2 of the order', '2014-03-24 12:47:43'),
	(5, 'rejected', 'Rejected', 'bid was rejected on final phase of the order', '2014-03-24 12:47:43'),
	(6, 'accepted', 'accepted', 'bid was accepted on final phase of the order', '2014-03-24 12:53:59'),
	(7, 'cancelled', 'Cancelled', 'bid was cancelled by the bidder', '2014-03-24 12:55:11'),
	(8, 'admin_cancelled', 'Admin Cancelled', 'bid was cancelled by the admin', '2014-03-24 12:55:11'),
	(9, 'phase1_onhold', 'Put onhold on Phase 1', 'bid was put onhold on phase 1 of the order', '2014-03-24 12:49:14'),
	(10, 'phase2_onhold', 'Put onhold on Phase 2', 'bid was put onhold on phase 2 of the order', '2014-03-24 12:58:47');
/*!40000 ALTER TABLE `bid_state` ENABLE KEYS */;

# Dumping data for table medibid_20140329.order_request: ~11 rows (approximately)
/*!40000 ALTER TABLE `order_request` DISABLE KEYS */;
INSERT INTO `order_request` (`id`, `state_id`, `code`, `summary`, `description`, `quantity`, `expiration_date`, `image`, `date_created`, `date_modified`) VALUES
	(1, 6, 'order1', 'fefe', '0', 21, '2014-03-25 18:16:16', 'image.jpg', '2014-03-11 18:16:20', '2014-03-26 19:41:18'),
	(2, 2, 'order2', 'rgeg', '0', 21, '2014-03-30 18:16:16', 'image.jpg', '2014-03-11 18:16:20', '2014-03-28 12:18:39'),
	(3, 4, 'order3', 'rgeg', '0', 21, '2014-04-04 18:16:16', 'image.jpg', '2014-03-11 18:16:20', '2014-03-26 16:05:03'),
	(4, 7, 'test21', 'rewre', '0', 21, '2014-03-29 13:15:53', 'image.jpg', '2014-03-11 18:16:20', '2014-03-26 19:41:08'),
	(5, 4, 'blah21', 'wqvvfd', '0', 21, '2014-03-29 13:15:53', 'image.jpg', '2014-03-11 18:16:20', '2014-03-17 01:20:31'),
	(6, 7, 'mnnewe2', 'lkloa', '0', 323, '2014-03-29 12:15:53', 'image.jpg', '2014-03-11 18:16:20', '2014-03-28 12:33:53'),
	(7, 4, 'test121', 'test order', '<i>test order</i>', 21, '2014-03-19 00:00:00', 'Chrysanthemum.jpg', '2014-03-16 09:16:05', '2014-03-26 09:30:14'),
	(8, 1, 'test214', 'test order', '<u>test order</u>', 2134, '2014-05-15 00:00:00', 'Jellyfish.jpg', '2014-03-16 10:53:21', '2014-03-28 17:40:10'),
	(9, 1, 'test oreder', 'THis is the summary', 'the order test', 32, '2014-03-31 00:00:00', 'Koala.jpg', '2014-03-17 11:53:11', '2014-03-28 11:32:01'),
	(10, 1, 'Test order new', 'Nice Order', '<i>Nice Order</i>', 1256, '2014-03-29 00:00:00', '20140204_161710.jpg', '2014-03-26 09:31:53', '2014-03-28 11:37:39'),
	(11, 1, 'Test order new 1', 'Test order new 1', 'Test order new 1', 543, '2014-05-09 00:00:00', 'Lighthouse.jpg', '2014-03-26 09:43:23', '2014-03-28 17:40:49');
/*!40000 ALTER TABLE `order_request` ENABLE KEYS */;

# Dumping data for table medibid_20140329.order_state: ~7 rows (approximately)
/*!40000 ALTER TABLE `order_state` DISABLE KEYS */;
INSERT INTO `order_state` (`id`, `name`, `display_name`, `description`, `date_modified`) VALUES
	(1, 'phase1', 'Phase 1', 'phase1 bidding started', '2014-03-16 16:44:10'),
	(2, 'phase1_expired', 'Phase 1 Expired', 'phase 1 bidding time expired', '2014-03-16 16:44:20'),
	(3, 'phase2', 'Phase 2', 'phase 2 bidding started', '2014-03-16 16:44:28'),
	(4, 'cancelled', 'Cancelled', 'bidding cancelled primaturely', '2014-03-16 16:44:34'),
	(5, 'phase2_expired', 'Phase 2 Expired', 'phase 2 bidding time expired', '2014-03-16 16:44:43'),
	(6, 'fullfilled', 'Fullfilled', 'order request fullfilled', '2014-03-16 16:44:52'),
	(7, 'unsatisfied', 'Unsatisfied', 'after phase 2 the order request is still unsatisfied', '2014-03-16 16:44:58');
/*!40000 ALTER TABLE `order_state` ENABLE KEYS */;

# Dumping data for table medibid_20140329.permission: ~1 rows (approximately)
/*!40000 ALTER TABLE `permission` DISABLE KEYS */;
INSERT INTO `permission` (`id`, `name`, `date_created`, `date_modified`) VALUES
	(1, 'addbid', '0000-00-00 00:00:00', '2014-02-21 15:22:11');
/*!40000 ALTER TABLE `permission` ENABLE KEYS */;

# Dumping data for table medibid_20140329.preference: ~0 rows (approximately)
/*!40000 ALTER TABLE `preference` DISABLE KEYS */;
/*!40000 ALTER TABLE `preference` ENABLE KEYS */;

# Dumping data for table medibid_20140329.role: ~2 rows (approximately)
/*!40000 ALTER TABLE `role` DISABLE KEYS */;
INSERT INTO `role` (`id`, `name`, `description`, `date_created`, `date_modified`) VALUES
	(1, 'admin', '0', '2014-02-18 17:15:11', '2014-02-18 17:15:12'),
	(2, 'bidder', '0', '2014-02-21 12:41:33', '2014-02-21 12:41:34');
/*!40000 ALTER TABLE `role` ENABLE KEYS */;

# Dumping data for table medibid_20140329.role_permission: ~1 rows (approximately)
/*!40000 ALTER TABLE `role_permission` DISABLE KEYS */;
INSERT INTO `role_permission` (`role_id`, `permission_id`) VALUES
	(2, 1);
/*!40000 ALTER TABLE `role_permission` ENABLE KEYS */;

# Dumping data for table medibid_20140329.user: ~4 rows (approximately)
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` (`id`, `first_name`, `last_name`, `about_me`, `admin_comment`, `phone`, `mobile`, `email`, `secondary_email`, `photo`, `password`, `security_question`, `security_question_answer`, `organization`, `active`, `system`, `date_created`, `date_modified`) VALUES
	(1, 'vimukthi', '0', '0', '0', '0', '0', 'vimukthi@geoclipse.com', '0', '0', 'test123', '0', '0', 'Geoclipse', 1, 1, '2014-02-18 17:16:42', '2014-03-24 21:52:33'),
	(2, 'tocha', 'fewf', '0', '0', '12231', '213412', 'tocha@geoclipse.com', 'tocha@geoclipse.com', 'tocha@geoclipse.com', 'test123', 'tocha@geoclipse.com', 'tocha@geoclipse.com', 'Geoclipse', 1, 0, '2014-02-21 12:42:09', '2014-03-24 21:52:40'),
	(4, 'vjnj', 'froeo', '0', '0', '2121', '3132', 'bfn@ermr.ckn', 'bfn@ermr.ckn', 'ewrew', 'fefw', 'What is your birth place?', 'Colombo ???', 'Glaxo', 1, 0, '2014-03-06 13:32:37', '2014-03-28 21:41:54'),
	(6, 'test', 'test', '0', '0', '12321', '32143', 'test1@test.com', 'test1@test.com', '', 'test', 'What is your birth place?', 'Colombo ???', 'Pfizer', 1, 0, '2014-03-07 01:07:03', '2014-03-24 21:52:55');
/*!40000 ALTER TABLE `user` ENABLE KEYS */;

# Dumping data for table medibid_20140329.user_preference: ~0 rows (approximately)
/*!40000 ALTER TABLE `user_preference` DISABLE KEYS */;
/*!40000 ALTER TABLE `user_preference` ENABLE KEYS */;

# Dumping data for table medibid_20140329.user_role: ~3 rows (approximately)
/*!40000 ALTER TABLE `user_role` DISABLE KEYS */;
INSERT INTO `user_role` (`user_id`, `role_id`) VALUES
	(1, 1),
	(4, 2),
	(6, 2);
/*!40000 ALTER TABLE `user_role` ENABLE KEYS */;

# Dumping data for table medibid_20140329.valid_bid_transition: ~19 rows (approximately)
/*!40000 ALTER TABLE `valid_bid_transition` DISABLE KEYS */;
INSERT INTO `valid_bid_transition` (`id`, `state1_id`, `state2_id`, `date_modified`) VALUES
	(1, 1, 3, '2014-03-24 13:01:48'),
	(2, 1, 9, '2014-03-24 13:02:05'),
	(3, 1, 2, '2014-03-24 13:02:19'),
	(4, 9, 1, '2014-03-24 13:03:17'),
	(5, 9, 8, '2014-03-24 13:11:08'),
	(6, 2, 3, '2014-03-24 13:04:58'),
	(7, 3, 4, '2014-03-24 13:05:47'),
	(8, 3, 2, '2014-03-24 13:06:01'),
	(9, 4, 10, '2014-03-24 13:07:19'),
	(10, 4, 7, '2014-03-24 13:08:52'),
	(11, 4, 6, '2014-03-24 13:09:32'),
	(12, 4, 5, '2014-03-24 13:09:55'),
	(13, 1, 7, '2014-03-24 13:11:25'),
	(14, 10, 4, '2014-03-24 13:12:40'),
	(15, 10, 8, '2014-03-24 13:13:01'),
	(16, 6, 5, '2014-03-24 13:14:24'),
	(17, 5, 6, '2014-03-24 13:14:36'),
	(18, 1, 8, '2014-03-24 13:17:41'),
	(19, 4, 8, '2014-03-24 13:18:13');
/*!40000 ALTER TABLE `valid_bid_transition` ENABLE KEYS */;

# Dumping data for table medibid_20140329.valid_order_bid_state_combo: ~34 rows (approximately)
/*!40000 ALTER TABLE `valid_order_bid_state_combo` DISABLE KEYS */;
INSERT INTO `valid_order_bid_state_combo` (`id`, `order_state_id`, `bid_state_id`, `date_modified`) VALUES
	(1, 1, 1, '2014-03-24 23:30:19'),
	(2, 1, 9, '2014-03-24 23:30:47'),
	(3, 1, 7, '2014-03-24 23:30:58'),
	(4, 1, 8, '2014-03-24 23:31:14'),
	(5, 2, 1, '2014-03-24 23:32:23'),
	(6, 2, 3, '2014-03-24 23:32:43'),
	(7, 2, 2, '2014-03-24 23:33:46'),
	(8, 2, 7, '2014-03-24 23:36:38'),
	(9, 2, 8, '2014-03-24 23:37:42'),
	(10, 3, 3, '2014-03-24 23:37:41'),
	(11, 3, 2, '2014-03-24 23:38:10'),
	(12, 3, 4, '2014-03-24 23:39:00'),
	(13, 3, 7, '2014-03-24 23:39:15'),
	(14, 3, 8, '2014-03-24 23:39:27'),
	(15, 3, 10, '2014-03-24 23:40:09'),
	(17, 5, 4, '2014-03-24 23:41:34'),
	(18, 5, 2, '2014-03-24 23:42:58'),
	(20, 5, 6, '2014-03-24 23:44:59'),
	(21, 5, 5, '2014-03-24 23:45:24'),
	(22, 5, 7, '2014-03-24 23:45:35'),
	(24, 5, 8, '2014-03-24 23:45:51'),
	(26, 4, 5, '2014-03-24 23:51:20'),
	(27, 4, 2, '2014-03-24 23:49:15'),
	(28, 4, 8, '2014-03-24 23:50:05'),
	(30, 4, 7, '2014-03-24 23:52:00'),
	(31, 6, 6, '2014-03-24 23:54:25'),
	(32, 6, 2, '2014-03-24 23:54:47'),
	(33, 6, 5, '2014-03-24 23:55:18'),
	(34, 6, 7, '2014-03-24 23:55:32'),
	(35, 6, 8, '2014-03-24 23:55:44'),
	(36, 7, 5, '2014-03-24 23:57:20'),
	(37, 7, 2, '2014-03-24 23:57:43'),
	(38, 7, 7, '2014-03-24 23:58:51'),
	(39, 7, 8, '2014-03-24 23:59:11');
/*!40000 ALTER TABLE `val`bid`id_order_bid_state_combo` ENABLE KEYS */;

# Dumping data for table medibid_20140329.valid_order_transition: ~7 rows (approximately)
/*!40000 ALTER TABLE `valid_order_transition` DISABLE KEYS */;
INSERT INTO `valid_order_transition` (`id`, `state1_id`, `state2_id`, `date_modified`) VALUES
	(1, 1, 2, '2014-03-14 14:35:27'),
	(2, 1, 4, '2014-03-14 14:35:42'),
	(3, 2, 3, '2014-03-14 14:36:00'),
	(4, 3, 5, '2014-03-14 14:36:23'),
	(5, 3, 4, '2014-03-14 14:36:32'),
	(6, 5, 6, '2014-03-14 14:37:37'),
	(7, 5, 7, '2014-03-14 14:37:49');
/*!40000 ALTER TABLE `valid_order_transition` ENABLE KEYS */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;

INSERT INTO `message` (`id`, `date_created`, `date_viewed`, `description`, `parent`, `title`, `from_user_id`, `to_user_id`, `state`, `global`) VALUES
	(1, '2014-03-27 01:09:13', '2014-03-27 01:09:14', 'test desc', 1, 'test title', 1, 2, NULL, NULL),
	(2, '2014-03-27 01:09:13', '2014-03-27 01:09:14', 'test desc2', 2, 'test title2', 1, 2, NULL, NULL);
