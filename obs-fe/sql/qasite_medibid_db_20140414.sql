# --------------------------------------------------------
# Host:                         us-cdbr-cb-east-01.cleardb.net
# Server version:               5.5.34-log
# Server OS:                    Linux
# HeidiSQL version:             6.0.0.3603
# Date/time:                    2014-04-14 19:33:13
# --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
# Dumping data for table cb_medibid.announcement: ~0 rows (approximately)
/*!40000 ALTER TABLE `announcement` DISABLE KEYS */;
/*!40000 ALTER TABLE `announcement` ENABLE KEYS */;

# Dumping data for table cb_medibid.bid: ~7 rows (approximately)
/*!40000 ALTER TABLE `bid` DISABLE KEYS */;
INSERT INTO `bid` (`id`, `amends_bid_id`, `date_created`, `date_modified`, `description`, `price_per_unit`, `quantity`, `bidder_id`, `order_id`, `state_id`) VALUES
	(1, NULL, '2014-04-14 00:06:37', NULL, 'If the quantity is increased to 50000 the price could be reduced to 193.50', 194.5, 25000, 91, 101, 1),
	(11, NULL, '2014-04-14 00:08:38', NULL, 'Holds certification for the distribution of the requested drug', 5, 20000, 101, 81, 1),
	(21, NULL, '2014-04-14 00:21:06', NULL, 'If the Qty is greater than 50000 the price could be reduced to 194.75', 195, 25000, 101, 101, 1),
	(31, NULL, '2014-04-14 00:27:16', NULL, 'Test', 5.1, 20000, 91, 81, 1),
	(41, NULL, '2014-04-14 00:30:25', NULL, 'test', 245, 100000, 111, 8, 1),
	(51, NULL, '2014-04-14 00:31:38', NULL, 'Test', 243.75, 10000, 101, 8, 1),
	(61, NULL, '2014-04-14 02:20:10', NULL, 'Tamoxifen&nbsp;', 0.13, 5000, 81, 121, 1);
/*!40000 ALTER TABLE `bid` ENABLE KEYS */;

# Dumping data for table cb_medibid.bid_state: ~10 rows (approximately)
/*!40000 ALTER TABLE `bid_state` DISABLE KEYS */;
INSERT INTO `bid_state` (`id`, `date_modified`, `description`, `display_name`, `name`) VALUES
	(1, '2014-03-24 12:51:56', 'bid is active for the phase 1 of the order', 'Phase 1 Active', 'phase1_active'),
	(2, '2014-03-24 12:48:29', 'bid was rejected on phase 1 of the order', 'Rejected on Phase 1', 'phase1_rejected'),
	(3, '2014-03-24 12:49:14', 'bid was accepted on phase 1 of the order', 'Accepted on Phase 1', 'phase1_accepted'),
	(4, '2014-03-24 12:47:43', 'bid is active for the phase 2 of the order', 'Phase 2 Active', 'phase2_active'),
	(5, '2014-03-24 12:47:43', 'bid was rejected on final phase of the order', 'Rejected', 'rejected'),
	(6, '2014-03-24 12:53:59', 'bid was accepted on final phase of the order', 'accepted', 'accepted'),
	(7, '2014-03-24 12:55:11', 'bid was cancelled by the bidder', 'Cancelled', 'cancelled'),
	(8, '2014-03-24 12:55:11', 'bid was cancelled by the admin', 'Admin Cancelled', 'admin_cancelled'),
	(9, '2014-03-24 12:49:14', 'bid was put onhold on phase 1 of the order', 'Put onhold on Phase 1', 'phase1_onhold'),
	(10, '2014-03-24 12:58:47', 'bid was put onhold on phase 2 of the order', 'Put onhold on Phase 2', 'phase2_onhold');
/*!40000 ALTER TABLE `bid_state` ENABLE KEYS */;

# Dumping data for table cb_medibid.message: ~1 rows (approximately)
/*!40000 ALTER TABLE `message` DISABLE KEYS */;
INSERT INTO `message` (`id`, `body`, `date_created`, `date_viewed`, `parent`, `state`, `title`, `receiver_id`, `sender_id`) VALUES
	(1, '<span style="background-color: red;">I need info on some drugs ...&nbsp;</span>', '2014-04-14 03:01:27', NULL, NULL, 0, 'Requesting info', 91, 1);
/*!40000 ALTER TABLE `message` ENABLE KEYS */;

# Dumping data for table cb_medibid.order_request: ~22 rows (approximately)
/*!40000 ALTER TABLE `order_request` DISABLE KEYS */;
INSERT INTO `order_request` (`id`, `code`, `date_created`, `date_modified`, `description`, `expiration_date`, `timezone`, `image`, `quantity`, `summary`, `state_id`) VALUES
	(1, 'order1', '2014-03-11 18:16:20', '2014-03-26 19:41:18', '0', '2014-03-25 18:16:16', 'Asia/Colombo', 'image.jpg', 21, 'fefe', 6),
	(2, 'order2', '2014-03-11 18:16:20', '2014-03-28 12:18:39', '0', '2014-03-30 18:16:16', 'Asia/Colombo', 'image.jpg', 21, 'rgeg', 2),
	(3, 'order3', '2014-03-11 18:16:20', '2014-03-26 16:05:03', '0', '2014-04-04 18:16:16', 'Asia/Colombo', 'image.jpg', 21, 'rgeg', 4),
	(4, 'test21', '2014-03-11 18:16:20', '2014-03-26 19:41:08', '0', '2014-03-29 13:15:53', 'Asia/Colombo', 'image.jpg', 21, 'rewre', 7),
	(5, 'blah21', '2014-03-11 18:16:20', '2014-03-17 01:20:31', '0', '2014-03-29 13:15:53', 'Asia/Colombo', 'image.jpg', 21, 'wqvvfd', 4),
	(6, 'mnnewe2', '2014-03-11 18:16:20', '2014-03-28 12:33:53', '0', '2014-03-29 12:15:53', 'Asia/Colombo', 'image.jpg', 323, 'lkloa', 7),
	(7, 'test121', '2014-03-16 09:16:05', '2014-03-26 09:30:14', '<i>test order</i>', '2014-03-19 00:00:00', 'Asia/Colombo', 'Chrysanthemum.jpg', 21, 'test order', 4),
	(8, 'test214', '2014-03-16 10:53:21', '2014-03-28 17:40:10', '&lt;test order&gt;10000 pills, delivery required on or before 08/05/2014.&nbsp;', '2014-06-10 00:00:00', 'Asia/Colombo', 'halaven.jpg', 10000, 'Halaven 0.5mg', 1),
	(9, 'test oreder', '2014-03-17 11:53:11', '2014-03-28 11:32:01', 'the order test', '2014-03-31 00:00:00', 'Asia/Colombo', 'Koala.jpg', 32, 'THis is the summary', 2),
	(10, 'Test order new', '2014-03-26 09:31:53', '2014-03-28 11:37:39', '<i>Nice Order</i>', '2014-04-12 00:00:00', 'Asia/Colombo', 'Koala.jpg', 1256, 'Nice Order', 2),
	(11, 'Test order new 1', '2014-03-26 09:43:23', '2014-03-28 17:40:49', 'Test order new 1<div>Will expire on 13 Ari 2014</div>', '2014-04-13 00:00:00', 'Asia/Colombo', 'USF.PNG', 543, 'Test order new 1', 2),
	(21, 'testqa', '2014-04-06 07:12:11', NULL, '<u>testqa</u>', '2014-04-10 00:00:00', 'Asia/Colombo', 'Hydrangeas.jpg', 21, 'testqa', 2),
	(31, '001', '2014-04-10 12:16:16', NULL, 'TestPanadol', '2014-04-17 00:00:00', 'Asia/Colombo', 'Tulips.jpg', 200, 'Test', 4),
	(41, '004', '2014-04-10 12:40:52', NULL, '6576tfuh', '2014-04-24 00:00:00', 'Asia/Colombo', NULL, 500, 'ghgfhg', 4),
	(51, '005', '2014-04-10 12:43:16', NULL, 'rrrrrrrrrrr', '2014-04-30 00:00:00', 'Asia/Colombo', NULL, 900, 'ttttttttt', 4),
	(61, '006', '2014-04-10 12:45:21', NULL, 'dddddddd', '2014-04-22 00:00:00', 'Asia/Colombo', NULL, 1000, 'nnnnnnnnn', 4),
	(71, 'EBR236', '2014-04-13 10:39:09', NULL, 'Expiry Date should not be before: 01/10/2015', '2014-05-27 04:18:15', 'Asia/Colombo', 'ebritux.jpg', 50000, 'Ebritux', 1),
	(81, 'KX238', '2014-04-13 10:41:54', NULL, '500mg; Continuous supply for an year (delivery required bi-annually)', '2014-05-20 00:00:00', 'Asia/Colombo', 'kadian.jpg', 20000, 'Kardian pills', 1),
	(91, 'AX-345', '2014-04-13 10:44:44', NULL, 'Selected supplier will get a 2 year contract', '2014-06-10 00:00:00', 'Asia/Colombo', 'afinitor.jpg', 1000000, 'Afinitor', 4),
	(101, 'BX1345', '2014-04-13 10:48:51', NULL, 'the expiry date should not be less than 01/01/2016', '2014-04-28 00:13:13', 'Asia/Colombo', 'bromfenac.gif', 25000, 'Bromfenac', 1),
	(111, 'AX124', '2014-04-13 11:05:42', NULL, '&lt;test&gt; expiry date should not be less than 01/01/2016', '2014-05-28 00:00:00', 'Asia/Colombo', 'afinitor.jpg', 100000, 'Afinator', 1),
	(121, 'AA1106', '2014-04-13 15:16:58', NULL, 'Tamoxifen 20 mg tablets', '2014-11-20 00:00:00', 'Asia/Colombo', 'download.jpg', 90000, 'Tamoxifen', 1);
/*!40000 ALTER TABLE `order_request` ENABLE KEYS */;

# Dumping data for table cb_medibid.order_state: ~7 rows (approximately)
/*!40000 ALTER TABLE `order_state` DISABLE KEYS */;
INSERT INTO `order_state` (`id`, `date_modified`, `description`, `display_name`, `name`) VALUES
	(1, '2014-03-16 16:44:10', 'phase1 bidding started', 'Phase 1', 'phase1'),
	(2, '2014-03-16 16:44:20', 'phase 1 bidding time expired', 'Phase 1 Expired', 'phase1_expired'),
	(3, '2014-03-16 16:44:28', 'phase 2 bidding started', 'Phase 2', 'phase2'),
	(4, '2014-03-16 16:44:34', 'bidding cancelled primaturely', 'Cancelled', 'cancelled'),
	(5, '2014-03-16 16:44:43', 'phase 2 bidding time expired', 'Phase 2 Expired', 'phase2_expired'),
	(6, '2014-03-16 16:44:52', 'order request fullfilled', 'Fullfilled', 'fullfilled'),
	(7, '2014-03-16 16:44:58', 'after phase 2 the order request is still unsatisfied', 'Unsatisfied', 'unsatisfied');
/*!40000 ALTER TABLE `order_state` ENABLE KEYS */;

# Dumping data for table cb_medibid.permission: ~1 rows (approximately)
/*!40000 ALTER TABLE `permission` DISABLE KEYS */;
INSERT INTO `permission` (`id`, `date_created`, `date_modified`, `name`) VALUES
	(1, '0000-00-00 00:00:00', '2014-02-21 15:22:11', 'addbid');
/*!40000 ALTER TABLE `permission` ENABLE KEYS */;

# Dumping data for table cb_medibid.preference: ~0 rows (approximately)
/*!40000 ALTER TABLE `preference` DISABLE KEYS */;
INSERT INTO `preference` (`id`, `date_created`, `date_modified`, `description`, `name`) VALUES
	(1, '2014-04-14 15:33:27', '2014-04-14 15:33:28', 'Timezone for user', 'timezone');
/*!40000 ALTER TABLE `preference` ENABLE KEYS */;

# Dumping data for table cb_medibid.role: ~2 rows (approximately)
/*!40000 ALTER TABLE `role` DISABLE KEYS */;
INSERT INTO `role` (`id`, `date_created`, `date_modified`, `description`, `name`) VALUES
	(1, '2014-02-18 17:15:11', '2014-02-18 17:15:12', '0', 'admin'),
	(2, '2014-02-21 12:41:33', '2014-02-21 12:41:34', '0', 'bidder');
/*!40000 ALTER TABLE `role` ENABLE KEYS */;

# Dumping data for table cb_medibid.role_permission: ~1 rows (approximately)
/*!40000 ALTER TABLE `role_permission` DISABLE KEYS */;
INSERT INTO `role_permission` (`role_id`, `permission_id`) VALUES
	(2, 1);
/*!40000 ALTER TABLE `role_permission` ENABLE KEYS */;

# Dumping data for table cb_medibid.user: ~5 rows (approximately)
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` (`id`, `about_me`, `active`, `admin_comment`, `date_created`, `date_modified`, `email`, `first_name`, `last_name`, `mobile`, `organization`, `password`, `phone`, `photo`, `secondary_email`, `security_question`, `security_question_answer`, `system`) VALUES
	(1, '0', 1, '0', '2014-02-18 17:16:42', '2014-03-24 21:52:33', 'vimukthi@geoclipse.com', 'vimukthi', '0', '0', 'Geoclipse', 'test123', '0', '0', '0', '0', '0', 1),
	(81, '', 1, '', '2014-04-13 15:34:08', NULL, 'aravindi@gmail.com', 'Aravindi', 'amarasinghe', '0382293468', 'MIT', '123bambalapi', '0772171753', '', 'vimukthi@gmail.com', 'What is your birth place?', 'Colombo ???', 0),
	(91, '', 1, '', '2014-04-13 23:52:39', NULL, 'admin.sales@roche.com', 'Mark', 'Wolf', '94112777855865', 'Roche', 'mit1234', '94112566566', '', 'mark.w@roche.com', 'What is your birth place?', 'Colombo ???', 0),
	(101, '', 1, '', '2014-04-13 23:54:55', NULL, 'admin.sales@gsk.com', 'Damian', 'Fernando', '94777544785', 'GlaxoSmithKline', 'mit1234', '94112705689', '', 'damian.f@gsk.com', 'What is your birth place?', 'Colombo ???', 0),
	(111, '', 1, '', '2014-04-14 00:29:28', NULL, 'admin.sales@hemas.lk', 'Joanne', 'Willson', '94725874332', 'Hemas', 'obs123', '94112735578', '', 'joanne.w@hemas.lk', 'What is your birth place?', 'Colombo ???', 0);
/*!40000 ALTER TABLE `user` ENABLE KEYS */;

# Dumping data for table cb_medibid.user_preference: ~0 rows (approximately)
/*!40000 ALTER TABLE `user_preference` DISABLE KEYS */;
INSERT INTO `user_preference` (`id`, `date_created`, `date_modified`, `value`, `preference_id`, `user_id`) VALUES
	(1, '2014-04-14 15:34:00', '2014-04-14 15:34:01', 'Asia/Colombo', 1, 1),
	(11, '2014-04-14 15:34:00', '2014-04-14 15:34:01', 'Asia/Colombo', 1, 81),
	(21, '2014-04-14 15:34:00', '2014-04-14 15:34:01', 'Asia/Colombo', 1, 91),
	(31, '2014-04-14 15:34:00', '2014-04-14 15:34:01', 'Asia/Colombo', 1, 101),
	(41, '2014-04-14 15:34:00', '2014-04-14 15:34:01', 'Asia/Colombo', 1, 111);
/*!40000 ALTER TABLE `user_preference` ENABLE KEYS */;

# Dumping data for table cb_medibid.user_role: ~5 rows (approximately)
/*!40000 ALTER TABLE `user_role` DISABLE KEYS */;
INSERT INTO `user_role` (`user_id`, `role_id`) VALUES
	(1, 1),
	(81, 2),
	(91, 2),
	(101, 2),
	(111, 2);
/*!40000 ALTER TABLE `user_role` ENABLE KEYS */;

# Dumping data for table cb_medibid.valid_bid_transition: ~19 rows (approximately)
/*!40000 ALTER TABLE `valid_bid_transition` DISABLE KEYS */;
INSERT INTO `valid_bid_transition` (`id`, `date_modified`, `state1_id`, `state2_id`) VALUES
	(1, '2014-03-24 13:01:48', 1, 3),
	(2, '2014-03-24 13:02:05', 1, 9),
	(3, '2014-03-24 13:02:19', 1, 2),
	(4, '2014-03-24 13:03:17', 9, 1),
	(5, '2014-03-24 13:11:08', 9, 8),
	(6, '2014-03-24 13:04:58', 2, 3),
	(7, '2014-03-24 13:05:47', 3, 4),
	(8, '2014-03-24 13:06:01', 3, 2),
	(9, '2014-03-24 13:07:19', 4, 10),
	(10, '2014-03-24 13:08:52', 4, 7),
	(11, '2014-03-24 13:09:32', 4, 6),
	(12, '2014-03-24 13:09:55', 4, 5),
	(13, '2014-03-24 13:11:25', 1, 7),
	(14, '2014-03-24 13:12:40', 10, 4),
	(15, '2014-03-24 13:13:01', 10, 8),
	(16, '2014-03-24 13:14:24', 6, 5),
	(17, '2014-03-24 13:14:36', 5, 6),
	(18, '2014-03-24 13:17:41', 1, 8),
	(19, '2014-03-24 13:18:13', 4, 8);
/*!40000 ALTER TABLE `valid_bid_transition` ENABLE KEYS */;

# Dumping data for table cb_medibid.valid_order_bid_state_combo: ~34 rows (approximately)
/*!40000 ALTER TABLE `valid_order_bid_state_combo` DISABLE KEYS */;
INSERT INTO `valid_order_bid_state_combo` (`id`, `date_modified`, `bid_state_id`, `order_state_id`) VALUES
	(1, '2014-03-24 23:30:19', 1, 1),
	(2, '2014-03-24 23:30:47', 9, 1),
	(3, '2014-03-24 23:30:58', 7, 1),
	(4, '2014-03-24 23:31:14', 8, 1),
	(5, '2014-03-24 23:32:23', 1, 2),
	(6, '2014-03-24 23:32:43', 3, 2),
	(7, '2014-03-24 23:33:46', 2, 2),
	(8, '2014-03-24 23:36:38', 7, 2),
	(9, '2014-03-24 23:37:42', 8, 2),
	(10, '2014-03-24 23:37:41', 3, 3),
	(11, '2014-03-24 23:38:10', 2, 3),
	(12, '2014-03-24 23:39:00', 4, 3),
	(13, '2014-03-24 23:39:15', 7, 3),
	(14, '2014-03-24 23:39:27', 8, 3),
	(15, '2014-03-24 23:40:09', 10, 3),
	(17, '2014-03-24 23:41:34', 4, 5),
	(18, '2014-03-24 23:42:58', 2, 5),
	(20, '2014-03-24 23:44:59', 6, 5),
	(21, '2014-03-24 23:45:24', 5, 5),
	(22, '2014-03-24 23:45:35', 7, 5),
	(24, '2014-03-24 23:45:51', 8, 5),
	(26, '2014-03-24 23:51:20', 5, 4),
	(27, '2014-03-24 23:49:15', 2, 4),
	(28, '2014-03-24 23:50:05', 8, 4),
	(30, '2014-03-24 23:52:00', 7, 4),
	(31, '2014-03-24 23:54:25', 6, 6),
	(32, '2014-03-24 23:54:47', 2, 6),
	(33, '2014-03-24 23:55:18', 5, 6),
	(34, '2014-03-24 23:55:32', 7, 6),
	(35, '2014-03-24 23:55:44', 8, 6),
	(36, '2014-03-24 23:57:20', 5, 7),
	(37, '2014-03-24 23:57:43', 2, 7),
	(38, '2014-03-24 23:58:51', 7, 7),
	(39, '2014-03-24 23:59:11', 8, 7);
/*!40000 ALTER TABLE `valid_order_bid_state_combo` ENABLE KEYS */;

# Dumping data for table cb_medibid.valid_order_transition: ~7 rows (approximately)
/*!40000 ALTER TABLE `valid_order_transition` DISABLE KEYS */;
INSERT INTO `valid_order_transition` (`id`, `date_modified`, `state1_id`, `state2_id`) VALUES
	(1, '2014-03-14 14:35:27', 1, 2),
	(2, '2014-03-14 14:35:42', 1, 4),
	(3, '2014-03-14 14:36:00', 2, 3),
	(4, '2014-03-14 14:36:23', 3, 5),
	(5, '2014-03-14 14:36:32', 3, 4),
	(6, '2014-03-14 14:37:37', 5, 6),
	(7, '2014-03-14 14:37:49', 5, 7);
/*!40000 ALTER TABLE `valid_order_transition` ENABLE KEYS */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
